# Stand-alone EMEC Geomtery Simulation using std G4 Solids

This project is trying to re-develop the ATLAS EMEC geomtery from standard G4 solids for simulation. Currenlty, "custom solid" geometry is used for it's simulation.  

This is very basic G4 code to visualize the EMEC geometry created using trapezoids. To build and run:
```shell
git clone https://:@gitlab.cern.ch:8443/avishwak/atlas_emec_g4.git
cd atlas_emec_g4
mkdir build
cd build
cmake ..
make -j8
./yourMainApplication
```
Make sure you have already Geant4 installed on your machine.
