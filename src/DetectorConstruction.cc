#include "DetectorConstruction.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

G4VPhysicalVolume* DetectorConstruction::Construct()
{
  //Set sensitive detector manager
  //  sensitive_detector_manager = G4SDManager::GetSDMpointer();

  //Define sensitive detectors
  //  absorbers_sensitive = new AbsorbersSensitive("/AbsorbersSensitive");

  //Add these sensitive detectors
  //  sensitive_detector_manager->AddNewDetector(absorbers_sensitive);

  //Writing to log
  where_to_write=1;
  // Get the materials -------------------------------------------------------------
  WriteToLog("Starting construction of the materials", 1);
  G4NistManager* materialmanager = G4NistManager::Instance();
  Air = materialmanager->FindOrBuildMaterial("G4_AIR");
  //  FibreGlass = materialmanager->FindOrBuildMaterial("G4_AIR");
  Al = materialmanager->FindOrBuildMaterial("G4_Al");
  lAr = materialmanager->FindOrBuildMaterial("G4_lAr");
  Fe = materialmanager->FindOrBuildMaterial("G4_Fe");
  Pb = materialmanager->FindOrBuildMaterial("G4_Pb");
  Cu = materialmanager->FindOrBuildMaterial("G4_Cu");
  Kapton = materialmanager->FindOrBuildMaterial("G4_KAPTON");

  //Need to define FibreGlass manually
  SiO2 = materialmanager->FindOrBuildMaterial("G4_SILICON_DIOXIDE");
  CaO = materialmanager->FindOrBuildMaterial("G4_CALCIUM_OXIDE");
  Al2O3 = materialmanager->FindOrBuildMaterial("G4_ALUMINUM_OXIDE");
  MgO = materialmanager->FindOrBuildMaterial("G4_MAGNESIUM_OXIDE");
  Na2O = materialmanager->FindOrBuildMaterial("G4_SODIUM_MONOXIDE");

  G4double density = 0.200*g/cm3;
  G4double fractionmass;
  G4String name;
  G4int ncomponents;
  FibreGlass = new G4Material(name="FibreGlass",density,ncomponents=5);
  FibreGlass->AddMaterial(SiO2, fractionmass=72.7*perCent);
  FibreGlass->AddMaterial(CaO, fractionmass=10*perCent);
  FibreGlass->AddMaterial(Al2O3, fractionmass=0.6*perCent);
  FibreGlass->AddMaterial(MgO, fractionmass=2.5*perCent);
  FibreGlass->AddMaterial(Na2O, fractionmass=14.2*perCent);
  //Should add sulphur. 0.7% and Si02->72%

  //Check the materials table.
  //  G4cout << *(G4Material::GetMaterialTable()) << G4endl;

  // End get the materials ---------------------------------------------------------

  WriteToLog("Starting construction of the detector", 1);
  //Build the solids
  CreateMother(); //Mother solid for the overall tube shape. 
  CreateEMEndcap("mother_logical"); //EMEndcap solids for the overall cone endcap shape.
  CreateEMECEndcap("endcaps_logical"); //EMEC Endcap
  CreateSupportingDisc("emec_endcap_logical"); //supporting_disc solid for the overall tube shape.
  CreateExternalSupportingDisc("emec_endcap_logical"); //external_supporting_disc solid for the overall tube shape.
  CreateMediumSupportingDisc("emec_endcap_logical");//medium_supporting_disc solid for the overall tube shape.
  CreateInternalSupportingDisc("emec_endcap_logical"); //internal_supporting_disc solid for the overall tube shape.
  CreateWheels("emec_endcap_logical"); //wheel solids representing the inner and outer wheels of the EMEC.
  CreateWheelCrack("emec_endcap_logical"); //solid representing the crack between wheels of the EMEC.
  CreateReflectedVolumes(); //Create the reflected volumes
  //DumpSolidAndVolumeDetails();

   return GetPhysicalVolume("mother_physical");

}



void DetectorConstruction::CreateZDivision(G4String parent_logical,G4Material* pMaterial,G4int wheel_number,G4double zdisplacement,G4RotationMatrix* zdivision_rotation,G4double ifwd,G4double ifold,G4double ifront,G4double zr1,G4double zr1p,G4double zr2,G4double zr2m,G4double zr,G4double kr,G4double kc,G4double zcr1,G4double zcr2,G4double zcr,G4double drrmax,G4double drrmin)
{


  if (zr==0)
    {
      // Temp
    };

  elements_rotation[element_counter]=zdivision_rotation;

  if (ifold==0)
    {
      rrint1=(wheel_internal_radius_1*(lar_totalthickness-zr1)+(wheel_internal_radius_2*zr1))/lar_totalthickness;
      rrint1p=(wheel_internal_radius_1*(lar_totalthickness-zr1p)+(wheel_internal_radius_2*zr1p))/lar_totalthickness;
      rrint2=(wheel_internal_radius_1*(lar_totalthickness-zr2)+(wheel_internal_radius_2*zr2))/lar_totalthickness;
      rrint2m=(wheel_internal_radius_1*(lar_totalthickness-zr2m)+(wheel_internal_radius_2*zr2m))/lar_totalthickness;
      rrext1=(wheel_external_radius_1*(lar_totalthickness-zr1)+(wheel_external_radius_2*zr1))/lar_totalthickness;
      rrext1p=(wheel_external_radius_1*(lar_totalthickness-zr1p)+(wheel_external_radius_2*zr1p))/lar_totalthickness;
      rrext2=(wheel_external_radius_1*(lar_totalthickness-zr2)+(wheel_external_radius_2*zr2))/lar_totalthickness;
      rrext2m=(wheel_external_radius_1*(lar_totalthickness-zr2m)+(wheel_external_radius_2*zr2m))/lar_totalthickness;

      rti1=std::max(rrint1,rrint1p);
      rti2=std::max(rrint2,rrint2m);
      rte1=std::min(rrext1,rrext1p);
      rte2=std::min(rrext2,rrext2m);
      
      rrmax=std::min(rrext1,rrext2);
      rrmin=std::max(rrint1,rrint2);

    };

  //  if(ifwd#1)then  
  if (ifwd!=1) //A Hash goes here!
    {
      //Set sensitive medium something here
    };
  
  if (ifold==1)
    {
      rrmin=(wheel_internal_radius_1*(lar_totalthickness-zcr)+wheel_internal_radius_2*zcr)/lar_totalthickness;
      rrmax=(wheel_external_radius_1*(lar_totalthickness-zcr)+wheel_external_radius_2*zcr)/lar_totalthickness;
      rrmax1=rrmax-drrmax*(wheel_external_radius_2-wheel_external_radius_1)/lar_totalthickness;
      rrmin1=rrmin+drrmin*(wheel_internal_radius_2-wheel_internal_radius_1)/lar_totalthickness;

      //if (iwh==1 & kc==2)
      if (wheel_number==0 & kc==2)
	{
	  rrmax=rrmax-(supportingbar_container_zsize/2)*(wheel_external_radius_2-wheel_external_radius_1)/lar_totalthickness;
	};
      //      if (iwh==2 & kc==1)
      if (wheel_number==1 & kc==1)
	{
	  rrmin=rrmin+(supportingbar_container_zsize/2)*(wheel_internal_radius_2-wheel_internal_radius_1)/lar_totalthickness;
	};
      rrint1=(wheel_internal_radius_1*(lar_totalthickness-zcr1)+(wheel_internal_radius_2*zcr1))/lar_totalthickness;
      rrint2=(wheel_internal_radius_1*(lar_totalthickness-zcr2)+(wheel_internal_radius_2*zcr2))/lar_totalthickness;
      rrext1=(wheel_external_radius_1*(lar_totalthickness-zcr1)+(wheel_external_radius_2*zcr1))/lar_totalthickness;
      rrext2=(wheel_external_radius_1*(lar_totalthickness-zcr2)+(wheel_external_radius_2*zcr2))/lar_totalthickness;
      
      // if (iwh==1)
      if (wheel_number==0)
	{

	  if (kc==1)
	    {

	      //Parameters
	      std::stringstream number;
	      number << element_counter;
	      G4String zdivision1_namestub="zdivision1-"+number.str();
	      G4double zdivision1_pRmin1=rrint1;
	      G4double zdivision1_pRmax1=rrext1;
	      G4double zdivision1_pRmin2=rrint2;
	      G4double zdivision1_pRmax2=rrext2;
	      G4double zdivision1_pDz=double(fabs(zcr1-zcr2))/2;
	      G4double zdivision1_pSPhi=-(180/wheel_parameters_absorbers_number[wheel_number])*deg; //Start Phi
	      G4double zdivision1_pDPhi=2*(180/wheel_parameters_absorbers_number[wheel_number])*deg; //Delta Phi
	      
	      elements_translation[element_counter].set(0*cm,0*cm,zdisplacement);
	      
	      //Solid
	      CreateSolid(zdivision1_namestub,zdivision1_pRmin1,zdivision1_pRmax1,zdivision1_pRmin2,zdivision1_pRmax2,zdivision1_pDz,zdivision1_pSPhi,zdivision1_pDPhi);
	      
	      //Logical volume
	      CreateLogicalVolume(elements_cons[element_counter],lAr,zdivision1_namestub);
	      
	      //Physical volume
	      CreatePhysicalVolume(elements_rotation[element_counter],elements_translation[element_counter],elements_logical[element_counter],zdivision1_namestub,GetLogicalVolume(parent_logical));
	      current_logical=elements_logical_names[element_counter];
	      //G4cout<<"ZD1: "<<CheckAxes(elements_rotation[element_counter])<<G4endl;
	      element_counter=element_counter+1;
	      
	    }
	  else
	    {

	      //Parameters
	      std::stringstream number;
	      number << element_counter;
	      G4String zdivision2_namestub="zdivision2-"+number.str();
	      G4double zdivision2_pRmin=std::max(rrint1,rrint2);
	      G4double zdivision2_pRmax=std::min(rrext1,rrext2);
	      G4double zdivision2_pDz=double(fabs(zcr1-zcr2))/2;
	      G4double zdivision2_pSPhi=-(180/wheel_parameters_absorbers_number[wheel_number])*deg; //Start phi
	      G4double zdivision2_pDPhi=2*(180/wheel_parameters_absorbers_number[wheel_number])*deg; //Delta phi

	      elements_translation[element_counter].set(0*cm,0*cm,zdisplacement);

	      //Solid
	      CreateSolid(zdivision2_namestub,zdivision2_pRmin,zdivision2_pRmax,zdivision2_pDz,zdivision2_pSPhi,zdivision2_pDPhi);

	      //Logical volume
	      CreateLogicalVolume(elements_tubs[element_counter],lAr,zdivision2_namestub);
	      
	      //Physical volume
	      CreatePhysicalVolume(elements_rotation[element_counter],elements_translation[element_counter],elements_logical[element_counter],zdivision2_namestub,GetLogicalVolume(parent_logical));
	      current_logical=elements_logical_names[element_counter];
	      element_counter=element_counter+1;
	    };
	}
      else
	{

	  if (kc==2)
	    {

	      //Parameters
	      std::stringstream number;
	      number << element_counter;
	      G4String zdivision3_namestub="zdivision3-"+number.str();
	      G4double zdivision3_pRmin1=rrint1-0.001*cm;
	      G4double zdivision3_pRmax1=rrext1;
	      G4double zdivision3_pRmin2=rrint2-0.001*cm;
	      G4double zdivision3_pRmax2=rrext2;
	      G4double zdivision3_pDz=double(fabs(zcr1-zcr2))/2;
	      G4double zdivision3_pSPhi=-(180/wheel_parameters_absorbers_number[wheel_number])*deg; //Start phi
	      G4double zdivision3_pDPhi=2*(180/wheel_parameters_absorbers_number[wheel_number])*deg; //Delta phi
	      
	      elements_translation[element_counter].set(0*cm,0*cm,zdisplacement);

	      //Solid
	      CreateSolid(zdivision3_namestub,zdivision3_pRmin1,zdivision3_pRmax1,zdivision3_pRmin2,zdivision3_pRmax2,zdivision3_pDz,zdivision3_pSPhi,zdivision3_pDPhi);

	      //Logical volume
	      CreateLogicalVolume(elements_cons[element_counter],lAr,zdivision3_namestub);
	      
	      //Physical volume
	      CreatePhysicalVolume(elements_rotation[element_counter],elements_translation[element_counter],elements_logical[element_counter],zdivision3_namestub,GetLogicalVolume(parent_logical));
	      current_logical=elements_logical_names[element_counter];
	      element_counter=element_counter+1;
	      
	    }
	  else
	    {


	      //Parameters
	      std::stringstream number;
	      number << element_counter;
	      G4String zdivision4_namestub="zdivision4-"+number.str();
	      G4double zdivision4_pRmin=std::max(rrint1,rrint2);
	      G4double zdivision4_pRmax=std::min(rrext1,rrext2);
	      G4double zdivision4_pDz=double(fabs(zcr1-zcr2))/2;
	      G4double zdivision4_pSPhi=-(180/wheel_parameters_absorbers_number[wheel_number])*deg; //Start phi
	      G4double zdivision4_pDPhi=2*(180/wheel_parameters_absorbers_number[wheel_number])*deg; //Delta phi

	      elements_translation[element_counter].set(0*cm,0*cm,zdisplacement);	      

	      //Solid
	      CreateSolid(zdivision4_namestub,zdivision4_pRmin,zdivision4_pRmax,zdivision4_pDz,zdivision4_pSPhi,zdivision4_pDPhi);

	      //Logical volume
	      CreateLogicalVolume(elements_tubs[element_counter],lAr,zdivision4_namestub);
	      
	      //Physical volume
	      CreatePhysicalVolume(elements_rotation[element_counter],elements_translation[element_counter],elements_logical[element_counter],zdivision4_namestub,GetLogicalVolume(parent_logical));
	      current_logical=elements_logical_names[element_counter];
	      element_counter=element_counter+1;
	      
	    };

	};

      //Removed from here
      for (int il=0;il<wheel_radiallayers_number;il++)
      	{
	  radi=cylindrical_layer_parameters_radil[il+(wheel_number*8)];
	  rado=cylindrical_layer_parameters_radol[il+(wheel_number*8)];
	  alfi=cylindrical_layer_parameters_alfil[il+(wheel_number*8)];
	  alfo=cylindrical_layer_parameters_alfol[il+(wheel_number*8)];
	  tcki=cylindrical_layer_parameters_tckil[il+(wheel_number*8)];
	  tcko=cylindrical_layer_parameters_tckol[il+(wheel_number*8)];
	  ciltet=cylindrical_layer_parameters_ciltetl[il+(wheel_number*8)];
	  alfcil=cylindrical_layer_parameters_alfcill[il+(wheel_number*8)];
	  xcnc=cylindrical_layer_parameters_xcncl[il+(wheel_number*8)];
	  ciltek=cylindrical_layer_parameters_ciltekl[il+(wheel_number*8)];
	  xcnck=cylindrical_layer_parameters_xcnckl[il+(wheel_number*8)];

	  G4bool li=false;
	  G4bool lo=false;

	  if (radi<rrmin & rado>rrmin | radi<rrmax & rado>rrmax) //This doesn't seem to be getting run.
	    {
	      if (radi<rrmin)
		{
		  drdi=rrmin-radi;
		  drdo=0;
		  li=true;
		};
	      if (rado>rrmax)
		{
		  drdo=rado-rrmax;
		  drdi=0;
		  if (wheel_number==0) //if (iwh==1)
		    {
		      lo=true;
		    };
		};
	      
	      alf1=alfi;
	      alf2=alfo;
	      talf1=1/(tan(alf1/2));
	      talf2=1/(tan(alf2/2));
	      esalf1=tcki/sin(alf1/2);
	      esalf2=tcki/sin(alf2/2);
	      talfi=(talf1+((talf2-talf1)/(rado-radi))*drdi);
	      talfo=(talf2-((talf2-talf1)/(rado-radi))*drdo);
	      alfi=2*atan(1/talfi);
	      alfo=2*atan(1/talfo);
	      esalfi=(esalf1+((esalf2-esalf1)/(rado-radi))*drdi);
	      esalfo=(esalf2-((esalf2-esalf1)/(rado-radi))*drdo);
	      tcki=esalfi*sin(alfi/2);
	      tcko=esalfo*sin(alfo/2);
	      radi=radi+drdi;
	      rado=rado-drdo;

	    };

	  //alfidg=alfi*(convert_radians_degrees);
	  //alfodg=alfo*(convert_radians_degrees);
	  
	  tckix=tcki/sin(alfi/2);
	  tckox=tcko/sin(alfo/2);
	  ciltet=0*deg;
	  
	  for (int m=0;m<2;m++)
	    {
	      cilox=((stac/4/tan(alfo/2))-(tckox/2)-(rcrb/sin(alfo/2)/cos(ciltet)));
	      cilix=((stac/4/tan(alfi/2))-(tckix/2)-(rcrb/sin(alfi/2)/cos(ciltet)));
	      cilx=(cilox+cilix)/2;
	      ciltet=atan((cilox-cilix)/(rado-radi));
	    };
	  
	  alfclo=piby2-asin(sin(alfo/2)*cos(ciltet));
	  alfcli=piby2-asin(sin(alfi/2)*cos(ciltet));
	  alfcil=alfclo;
	  cilalf=piby2-(alfclo+alfcli)/2;
	  clcphi=atan(2*cilx/(radi+rado));
	  rcil=sqrt(pow(cilx,2)+pow((radi+rado),2)/4);
	  
	  ciloxk=((stac/4/tan(alfo/2))-(kapton_curvature_radius/sin(alfo/2)));
	  cilixk=((stac/4/tan(alfi/2))-(kapton_curvature_radius/sin(alfi/2)));
	  cilxk=(ciloxk+cilixk)/2;
	  ciltek=atan((ciloxk-cilixk)/(radi-rado));
	  xcnc=cilx;
	  xcnck=cilxk;

	  if (radi>=rrmin-0.001 & rado<=rrmax+0.001 & (ifwd==0 | ifwd==4 | ifwd==2))
	    {
	      if (ifwd==0 | ifwd==4)
		{

		  phigc=-ciltet*(convert_radians_degrees);
		  phigck=-ciltek*(convert_radians_degrees);
		  dphi=(((-360*deg)/wheel_parameters_absorbers_number[wheel_number])*(double(wheel_parameters_beta[wheel_number])/2));
		  cdphi=cos(dphi);
		  sdphi=sin(dphi);	
		  
		  //Creating volume.
		  elements_translation[element_counter].set((cdphi*((rado+radi)/2)-(sdphi*xcnc)),(sdphi*((rado+radi)/2)+(cdphi*xcnc)),(-supportingbar_container_zsize/4));

		  elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
		  rotation_values.clear();
		  rotation_values.push_back(90*deg); //ThetaX
 		  rotation_values.push_back(-90*deg+ciltet+dphi); //PhiX
 		  rotation_values.push_back(0*deg); //ThetaY
 		  rotation_values.push_back(0*deg); //PhiY
 		  rotation_values.push_back(90*deg); //ThetaZ
		  rotation_values.push_back(ciltet+dphi); //PhiZ
		  //G4cout<<"BRENDAN1"<<G4endl;
		  elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
		  //elements_rotation[element_counter]->rotateY(90*deg);
		  //ThetaX=90,ThetaY=0,ThetaZ=90,PhiX=-90+CILTET*RADDEG+dphi,PhiY=0,PhiZ=CILTET*RADDEG+dphi

		  //-1-CreateAbsorberFold(elements_cons[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],pMaterial,GetLogicalVolume(current_logical)); //ERAN
		  
		  dphi=(((-360*deg)/wheel_parameters_absorbers_number[wheel_number])*(double(wheel_parameters_beta[wheel_number]-1)/2));
		  cdphi=cos(dphi);
		  sdphi=sin(dphi);

		  //Creating volume.
		  elements_translation[element_counter].set(cdphi*(rado+radi)/2-sdphi*xcnck,sdphi*(rado+radi)/2+cdphi*xcnck,-supportingbar_container_zsize/4);
		  elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
		  rotation_values.clear();
		  rotation_values.push_back(90*deg); //ThetaX
		  rotation_values.push_back(-90*deg+ciltek+dphi); //PhiX
		  rotation_values.push_back(0*deg); //ThetaY
		  rotation_values.push_back(0*deg); //PhiY
		  rotation_values.push_back(90*deg); //ThetaZ
		  rotation_values.push_back(ciltek+dphi); //PhiZ
		  
		  elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
		  //ThetaX=90 ThetaY=0 ThetaZ=90, PhiX=-90+CILTEK*RADDEG+dphi PhiY=0 PhiZ=CILTEK*RADDEG+dphi
		  //G4cout<<"BRENDNA1: "<<G4endl;
		  //-2-CreateKaptonFold(elements_cons[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical)); //EPKR
		}
	      else if (ifwd==2)
		{
		  
		  xcnc=rcrb+(tcki+tcko)/4;
		  ciltet=atan(xcnc*(tan((CLHEP::pi-alfo)/4)-tan((CLHEP::pi-alfi)/4))/(rado-radi));
		  ciltek=atan((kapton_curvature_radius*(tan((CLHEP::pi-alfo)/4)-tan((CLHEP::pi-alfi)/4)))/(rado-radi));

		  phigc=-ciltet*(convert_radians_degrees);
		  phigck=-ciltek*(convert_radians_degrees);
		  
		  for (int idphi=1;idphi<=(wheel_parameters_beta[wheel_number]%2)+1;idphi++)
		    {
		      
		      dphi=((-360*deg)/wheel_parameters_absorbers_number[wheel_number])*(idphi-1-double((wheel_parameters_beta[wheel_number]%2))/2);
		      cdphi=cos(dphi);
		      sdphi=sin(dphi);
		      
		      
		      
		      //Creating volume.
		      elements_translation[element_counter].set((cdphi*((rado+radi)/2)+(sdphi*xcnc)),(sdphi*((rado+radi)/2)-(cdphi*xcnc)),(-xcnc*(tan((pi-alfo)/4)+tan((pi-alfi)/4))/2));

		      elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
		      rotation_values.clear();
		      rotation_values.push_back(90*deg); //ThetaX
		      rotation_values.push_back(-90*deg+dphi); //PhiX
		      rotation_values.push_back(ciltet); //ThetaY
		      rotation_values.push_back(dphi); //PhiY
		      rotation_values.push_back(90*deg+ciltet); //ThetaZ
		      rotation_values.push_back(dphi); //PhiZ
		      //G4cout<<"BRENDAN2"<<G4endl;
		      elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
		      //ThetaX=90,ThetaY=ciltet*raddeg,ThetaZ=90+ciltet*raddeg,PhiX=-90+dphi,PhiY=dphi,PhiZ=dphi

		      //-3-CreateAbsorberFold(elements_cons[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],pMaterial,GetLogicalVolume(current_logical)); //ERAN


		      
		      //Creating volume.
		      elements_translation[element_counter].set(cdphi*(rado+radi)/2,sdphi*(rado+radi)/2,-(supportingbar_container_zsize/2+xcnc*(tan((pi-alfo)/4)+tan((pi-alfi)/4)))/4);

		      elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
		      rotation_values.clear();
		      rotation_values.push_back(90*deg); //ThetaX
		      rotation_values.push_back(90*deg+dphi); //PhiX
		      rotation_values.push_back(0*deg); //ThetaY
		      rotation_values.push_back(dphi); //PhiY
		      rotation_values.push_back(90*deg); //ThetaZ
		      rotation_values.push_back(dphi); //PhiZ
		      
		      elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);

		      //ThetaX=90 ThetaY=0 ThetaZ=90, PhiX=90+dphi PhiY=dphi PhiZ=dphi
		      
		      CreateAbsorberBeginning(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical)); // EWij
		    };

		  for (int idphi=1;idphi<=((wheel_parameters_beta[wheel_number]+1)%2)+1;idphi++)
		    {
		      
		      dphi=((-360*deg)/wheel_parameters_absorbers_number[wheel_number])*(idphi-1-double((wheel_parameters_beta[wheel_number]+1)%2)/2);

		      cdphi=cos(dphi);
		      sdphi=sin(dphi);

		      //Creating volume.
		      elements_translation[element_counter].set(cdphi*(rado+radi)/2+sdphi*kapton_curvature_radius,sdphi*(rado+radi)/2-cdphi*kapton_curvature_radius,-kapton_curvature_radius*(tan((pi-alfi)/4)+tan((pi-alfo)/4))/2);

		      elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
		      rotation_values.clear();
		      rotation_values.push_back(90*deg); //ThetaX
		      rotation_values.push_back(-90*deg+dphi); //PhiX
		      rotation_values.push_back(ciltek); //ThetaY
		      rotation_values.push_back(dphi); //PhiY
		      rotation_values.push_back(90*deg+ciltek); //ThetaZ
		      rotation_values.push_back(dphi); //PhiZ
		     
		      elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
		      //ThetaX=90 ThetaY=ciltek*raddeg ThetaZ=90+ciltek*raddeg, PhiX=-90+dphi PhiY=dphi PhiZ=dphi
		      //G4cout<<"BRENDNA2: "<<G4endl;
		      //-4-CreateKaptonFold(elements_cons[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical)); //EPKR

		      
		      //Creating volume.
		      elements_translation[element_counter].set(cdphi*(rado+radi)/2,sdphi*(rado+radi)/2,-(supportingbar_container_zsize/2+kapton_curvature_radius*(tan((pi-alfo)/4)+tan((pi-alfi)/4)))/4);
		      elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
		      rotation_values.clear();
		      rotation_values.push_back(90*deg); //ThetaX
		      rotation_values.push_back(90*deg+dphi); //PhiX
		      rotation_values.push_back(0*deg); //ThetaY
		      rotation_values.push_back(dphi); //PhiY
		      rotation_values.push_back(90*deg); //ThetaZ
		      rotation_values.push_back(dphi); //PhiZ

		      //ThetaX=90 ThetaY=0 ThetaZ=90, PhiX=90+dphi PhiY=dphi PhiZ=dphi		      
		      elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
		      
		      CreateKaptonBeginning(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical)); // EUij
		    };
		};
	    }


	  drdo=0;
	  drdi=0;
	  li=false;
	  lo=false;
	  
	  if (kc==2 & rado>rrmax1 & radi<rrmax1)
	    {
	      drdo=rado-rrmax1;
	      if (wheel_number==0) //if (iwh==1)
		{
		  lo=true;
		};
	    };
	  
	  if (kc==1 & radi<rrmin1 & rado>rrmin1)
	    {
	      drdi=rrmin1-radi;
	      li=true;
	    };
	  
	  if (drdo!=0 | drdi!=0)
	    {
	      alf1=alfi;
	      alf2=alfo;
	      talf1=1/tan(alf1/2);
	      talf2=1/tan(alf2/2);
	      esalf1=tcki/sin(alf1/2);
	      esalf2=tcki/sin(alf2/2);
	      talfi=talf1+((talf2-talf1)/(rado-radi))*drdi;
	      talfo=talf2-((talf2-talf1)/(rado-radi))*drdo;
	      alfi=2*atan(1/talfi);
	      alfo=2*atan(1/talfo);
	      esalfi=esalf1+((esalf2-esalf1)/(rado-radi))*drdi;
	      esalfo=esalf2-((esalf2-esalf1)/(rado-radi))*drdo;
	      tcki=esalf1*sin(alfi/2);
	      tcko=esalf2*sin(alfo/2);
	      radi=radi+drdi;
	      rado=rado-drdo;
	    };
	  
	  //alfidg=alfi*(convert_radians_degrees);
	  //alfodg=alfo*(convert_radians_degrees);
	  
	  // 	  if (wheel_number==1 & il>7)
	  // 	    {
	  
	  // 	    };

	  if (radi>rrmin-0.001 & rado<=rrmax+0.001 & (ifwd==0 | ifwd==4 | ifwd==2))
	    {
	      //	      if(ifwd#0)then
	      if (ifwd!=0) //This should be a # hash here!
		{
		  rho=kapton_curvature_radius;
		  
		  xtrfwi=((stac/4)+(rho*tan((CLHEP::pi-alfi)/4)*sin(alfi/2))-(rho*cos(alfi/2)))/2;
		  xtrfwo=((stac/4)+(rho*tan((CLHEP::pi-alfo)/4)*sin(alfo/2))-(rho*cos(alfo/2)))/2;
		  
		  ytrfwi=((stac/4/tan(alfi/2))+(rho*tan((CLHEP::pi-alfi)/4)*cos(alfi/2))-(rho*cos(alfi/2)/tan(alfi/2)))/2;
		  ytrfwo=((stac/4/tan(alfo/2))+(rho*tan((CLHEP::pi-alfo)/4)*cos(alfo/2))-(rho*cos(alfo/2)/tan(alfo/2)))/2;
		  
		  xtrfwd=(xtrfwi+xtrfwo)/2;
		  ytrfwd=(ytrfwi+ytrfwo)/2;

		  dxyfwd=sqrt(pow((xtrfwo-xtrfwi),2)+pow((ytrfwo-ytrfwi),2));
		  thetfw=atan(dxyfwd/(rado-radi))*(convert_radians_degrees);
		  thetfw=0;
		  phifwd=(atan2((ytrfwo-ytrfwi),(xtrfwo-xtrfwi))+(alfi+alfo)/4)+180*deg;
		  
		  hfwdi=((stac/4/sin(alfi/2))-(rho/tan(alfi/2))-(rho*tan((CLHEP::pi-alfi)/4)))/2;
		  hfwdo=((stac/4/sin(alfo/2))-(rho/tan(alfo/2))-(rho*tan((CLHEP::pi-alfo)/4)))/2;

		};
	      
	      
	      //Create a shape here.
	      if ((li==true | lo==true) & ifwd==0)
		{
		  //Create a shape here.
		};
	      
	      if (li==true)
		{
		  alf=alfi;
		  tck=tcki;
		  rt1=rti1;
		  rt2=rti2;
		  phiio=-90*deg;
		  sgn=-1;
		  thetx=0*deg;
		  phx=90*deg;
		  phy=270*deg;
		};
	      
	      if (lo==true)
		{
		  alf=alfo;
		  tck=tcko;
		  rt1=rte1;
		  rt2=rte2;
		  phiio=90*deg;
		  sgn=1;
		  thetx=180*deg;
		  phx=270*deg;
		  phy=90*deg;
		};
	      
	      //	      aa=alfodg+alfidg;
	      aa=alfo+alfi;
	      
	      dphi=((-360*deg)/wheel_parameters_absorbers_number[wheel_number])*(double((wheel_parameters_beta[wheel_number]-1))/2);
	      cdphi=cos(dphi);
	      sdphi=sin(dphi);

	      if (ifwd==0)
		{

		  elements_translation[element_counter].set(cdphi*(rado+radi)/2,sdphi*(rado+radi)/2,sroue1/2+scrb1/4);

		  elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
		  rotation_values.clear();
		  rotation_values.push_back(aa/4); //ThetaX
		  rotation_values.push_back(90*deg+dphi); //PhiX
		  rotation_values.push_back(90*deg-aa/4); //ThetaY
		  rotation_values.push_back(270*deg+dphi); //PhiY
		  rotation_values.push_back(90*deg); //ThetaZ
		  rotation_values.push_back(dphi); //PhiZ
		  
		  elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
		  //ThetaX=aa/4 ThetaY=90-aa/4 ThetaZ=90, PhiX=90+dphi PhiY=270+dphi phiZ=dphi

		  CreateKaptonPlane(elements_twistedtraps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical)); // EKAP
		}
	      else if (ifwd==4)
		{		

		  elements_translation[element_counter].set(cdphi*(rado+radi)/2-sdphi*ytrfwd,sdphi*(rado+radi)/2+cdphi*ytrfwd,-xtrfwd+stac/4-supportingbar_container_zsize/4);

		  elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
		  rotation_values.clear();
		  rotation_values.push_back(aa/4); //ThetaX
		  rotation_values.push_back(90*deg+dphi); //PhiX
		  rotation_values.push_back(90*deg-aa/4); //ThetaY
		  rotation_values.push_back(270*deg+dphi); //PhiY
		  rotation_values.push_back(90*deg); //ThetaZ
		  rotation_values.push_back(dphi); //PhiZ

		  elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
		  //Fix this rotation with ThetaX=aa/4 ThetaY=90-aa/4 ThetaZ=90, PhiX=90+dphi PhiY=270+dphi phiZ=dphi

		  CreateKaptonPlane(elements_twistedtraps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical)); // EKAP
		}
	      else if (ifwd==2)
		{
		  for (int idphi=1;idphi<=((wheel_parameters_beta[wheel_number]+1)%2)+1;idphi++)
		    {
		      dphi=((-360*deg)/wheel_parameters_absorbers_number[wheel_number])*((idphi-1-double(((wheel_parameters_beta[wheel_number]+1)%2))/2));
		      cdphi=cos(dphi);
		      sdphi=sin(dphi);
		      
		      elements_translation[element_counter].set(cdphi*(rado+radi)/2+sdphi*ytrfwd,sdphi*(rado+radi)/2-cdphi*ytrfwd,xtrfwd);

		      elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
		      rotation_values.clear();
		      rotation_values.push_back(180*deg-aa/4); //ThetaX
		      rotation_values.push_back(270*deg+dphi); //PhiX
		      rotation_values.push_back(90*deg+aa/4); //ThetaY
		      rotation_values.push_back(90*deg+dphi); //PhiY
		      rotation_values.push_back(90*deg); //ThetaZ
		      rotation_values.push_back(dphi); //PhiZ

		      elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
		      
		      //Fix this rotation with ThetaX=180-aa/4 ThetaY=90+aa/4 ThetaZ=90, PhiX=270+dphi PhiY=90+dphi phiZ=dphi

		      CreateKaptonPlane(elements_twistedtraps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical)); // EKAP
		    };
		};
	      
	      if ((li==true | lo==true) & ifwd==0)
		{
		  ht=(stac/4/sin(alf/2))-(kapton_curvature_radius/tan(alf/2));

		  elements_translation[element_counter].set(cdphi*(rt1+rt2)/2-sdphi*ht/2*cos(alf/2),sdphi*(rt1+rt2)/2+ht/2*cos(alf/2)*cdphi,-ht/2*sin(alf/2)+sroue1/2+scrb1/4);
		  elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
		  rotation_values.clear();
		  rotation_values.push_back((thetx-alf/2)*sgn); //ThetaX
		  rotation_values.push_back(phx+dphi); //PhiX
		  rotation_values.push_back(90*deg+sgn*alf/2); //ThetaY
		  rotation_values.push_back(phy+dphi); //PhiY
		  rotation_values.push_back(sgn*(90*deg)); //ThetaZ
		  rotation_values.push_back(dphi); //PhiZ
		  
		  elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
		  //ThetaX=(thetx-alf*raddeg/2)*sgn ThetaY=90+sgn*alf*raddeg/2 ThetaZ=sgn*90, PhiX=phx+dphi PhiY=phy+dphi phiZ=dphi;
		  CreateKaptonTriangle(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical),li,lo); // ETij
		};
	      //if(ifwd#0)then
	      if (ifwd!=0) //This should be a # hash!
		{
		  rho=rcrb+(tcki/2);
		  xtrfwi=((stac/4)+(rho*tan((CLHEP::pi-alfi)/4))*(sin(alfi/2))-(rho*cos(alfi/2)))/2;
		  xtrfwo=((stac/4)+(rho*tan((CLHEP::pi-alfo)/4))*(sin(alfo/2))-(rho*cos(alfo/2)))/2;

		  ytrfwi=((stac/4/tan(alfi/2))+(rho*tan((CLHEP::pi-alfi)/4)*cos(alfi/2))-(rho*cos(alfi/2)/tan(alfi/2)))/2;
		  ytrfwo=((stac/4/tan(alfo/2))+(rho*tan((CLHEP::pi-alfo)/4)*cos(alfo/2))-(rho*cos(alfo/2)/tan(alfo/2)))/2;
		  
		  xtrfwd=(xtrfwi+xtrfwo)/2;
		  ytrfwd=(ytrfwi+ytrfwo)/2;
		  dxyfwd=sqrt(pow((xtrfwo-xtrfwi),2)+pow((ytrfwo-ytrfwi),2));

		  thetfw=atan(dxyfwd/(rado-radi));

		  phifwd=(atan2((ytrfwo-ytrfwi),(xtrfwo-xtrfwi))+((alfi+alfo)/4))+180*deg;

		  hfwdi=((stac/4/sin(alfi/2))-(rho/tan(alfi/2))-(rho*tan((CLHEP::pi-alfi)/4)))/2;
		  hfwdo=((stac/4/sin(alfo/2))-(rho/tan(alfo/2))-(rho*tan((CLHEP::pi-alfo)/4)))/2;
		};
	      //Create a shape here.
	      if ((li==true | lo==true) & ifwd==0)
		{
		  //Create a shape here
		};
	  
	      for (int idphi=1;idphi<=2;idphi++)
		{
		  dphi=((360*deg)/wheel_parameters_absorbers_number[wheel_number])*(idphi-1-wheel_parameters_beta[wheel_number]/2);
		  cdphi=cos(dphi);
		  sdphi=sin(dphi);
		  
		  if (ifwd==0)
		    {
		      elements_translation[element_counter].set(cdphi*(rado+radi)/2,sdphi*(rado+radi)/2,sroue1/2+scrb1/4);
		      elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
		      rotation_values.clear();
 		      rotation_values.push_back(aa/4); //ThetaX
 		      rotation_values.push_back(90*deg+dphi); //PhiX
 		      rotation_values.push_back(90*deg-aa/4); //ThetaY
 		      rotation_values.push_back(270*deg+dphi); //PhiY
 		      rotation_values.push_back(90*deg); //ThetaZ
 		      rotation_values.push_back(dphi); //PhiZ

		      
		      elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
		      //G4cout<<"fred1: :"<<G4endl;
		      //Fix this rotation with ThetaX=aa/4 ThetaY=90-aa/4 ThetaZ=90, PhiX=90+dphi PhiY=270+dphi phiZ=dphi
		      whichflatpart=1;
		      CreateAbsorberFlatPart(elements_twistedtraps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical),wheel_number);

		    }
		  else if (ifwd==4)
		    {
		      dphi=((360*deg)/wheel_parameters_absorbers_number[wheel_number])*(idphi-1-wheel_parameters_beta[wheel_number]/2);
		      cdphi=cos(dphi);
		      sdphi=sin(dphi);
		      
		      elements_translation[element_counter].set(cdphi*(rado+radi)/2-sdphi*ytrfwd,sdphi*(rado+radi)/2+cdphi*ytrfwd,-xtrfwd+stac/4-supportingbar_container_zsize/4);

		      elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
		      rotation_values.clear();
		      rotation_values.push_back(aa/4); //ThetaX
		      rotation_values.push_back(90*deg+dphi); //PhiX
		      rotation_values.push_back(90*deg-aa/4); //ThetaY
		      rotation_values.push_back(270*deg+dphi); //PhiY
		      rotation_values.push_back(90*deg); //ThetaZ
		      rotation_values.push_back(dphi); //PhiZ
		      
		      elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
		      //G4cout<<"fred2: :"<<G4endl;
		      //Fix this rotation with ThetaX=aa/4 ThetaY=90-aa/4 ThetaZ=90, PhiX=90+dphi PhiY=270+dphi phiZ=dphi
		      whichflatpart=2;
		      CreateAbsorberFlatPart(elements_twistedtraps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical),wheel_number);

		    };
	      
		  if ((li==true | lo==true) & ifwd==0)
		    {
		      ht=(stac/4/sin(alf/2))-(rcrb+(tck/2))/tan(alf/2);
		      elements_translation[element_counter].set(cdphi*(rt1+rt2)/2-sdphi*ht/2*cos(alf/2),sdphi*(rt1+rt2)/2+ht/2*cos(alf/2)*cdphi,-ht/2*sin(alf/2)+sroue1/2+scrb1/4);							      

		      elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
		      rotation_values.clear();
		      rotation_values.push_back((thetx-alf/2)*sgn); //ThetaX
		      rotation_values.push_back(phx+dphi); //PhiX
		      rotation_values.push_back(90*deg+sgn*alf/2);  //ThetaY
		      rotation_values.push_back(phy+dphi); //PhiY
		      rotation_values.push_back(sgn*(90*deg)); //ThetaZ //This is -1* that of fortran code?
		      rotation_values.push_back(dphi); //PhiZ

		      elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
		      //ThetaX=(thetx-alf*raddeg/2)*sgn ThetaY=90+sgn*alf*raddeg/2 ThetaZ=sgn*90, PhiX=phx+dphi PhiY=phy+dphi phiZ=dphi
		      //G4cout<<"BRENDAN1: "<<G4endl;
		      CreateIronTriangle(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical),li,lo); // EZij
		    };
		};
	  
	      if (ifwd==2)
		{

		  for (int idphi=1;idphi<=((wheel_parameters_beta[wheel_number]%2)+1);idphi++)
		    {
		      dphi=((-360*deg)/(wheel_parameters_absorbers_number[wheel_number]))*(idphi-1-double((wheel_parameters_beta[wheel_number]%2))/2);

		      cdphi=cos(dphi);
		      sdphi=sin(dphi);
		      
		      elements_translation[element_counter].set(cdphi*(rado+radi)/2+sdphi*ytrfwd,sdphi*(rado+radi)/2-cdphi*ytrfwd,xtrfwd);
		      elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
		      rotation_values.clear();
		      rotation_values.push_back(180*deg-aa/4); //ThetaX
		      rotation_values.push_back(270*deg+dphi); //PhiX
		      rotation_values.push_back(90*deg+aa/4); //ThetaY
		      rotation_values.push_back(90*deg+dphi); //PhiY
		      rotation_values.push_back(90*deg); //ThetaZ
		      rotation_values.push_back(dphi); //PhiZ
		      
		      elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
		      //G4cout<<"fred3: :"<<G4endl;
		      //Fix this rotation with ThetaX=180-aa/4 ThetaY=90+aa/4 ThetaZ=90, PhiX=270+dphi PhiY=90+dphi phiZ=dphi
		      whichflatpart=3;
		      CreateAbsorberFlatPart(elements_twistedtraps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical),wheel_number);

		    };
		};
	    };
	};
    }
  else
    {
      //Parameters
      std::stringstream number;
      number << element_counter;
      G4String zdivision5_namestub="zdivision5-"+number.str();
      G4double zdivision5_pRmin1=rrint1p;
      G4double zdivision5_pRmax1=rrext1p;
      G4double zdivision5_pRmin2=rrint2m;
      G4double zdivision5_pRmax2=rrext2m;
      G4double zdivision5_pDz=(zr2m-zr1p)/2;
      G4double zdivision5_pSPhi=-(180/wheel_parameters_absorbers_number[wheel_number])*deg; //Start Phi
      G4double zdivision5_pDPhi=2*(180/wheel_parameters_absorbers_number[wheel_number])*deg; //Delta phi
      elements_translation[element_counter].set(0*cm,0*cm,zdisplacement);

      //Solid
      CreateSolid(zdivision5_namestub,zdivision5_pRmin1,zdivision5_pRmax1,zdivision5_pRmin2,zdivision5_pRmax2,zdivision5_pDz,zdivision5_pSPhi,zdivision5_pDPhi);

      //Logical volume
      CreateLogicalVolume(elements_cons[element_counter],lAr,zdivision5_namestub);

      //Physical volume
      CreatePhysicalVolume(elements_rotation[element_counter],elements_translation[element_counter],elements_logical[element_counter],zdivision5_namestub,GetLogicalVolume(parent_logical));
      current_logical=elements_logical_names[element_counter];
      element_counter=element_counter+1;

      if (ifwd==1)
	{
	  

	  for (int idphi=1;idphi<=(((wheel_parameters_beta[wheel_number]+1)%2)+1);idphi++)
	    {
	      dphi=((-360*deg)/(wheel_parameters_absorbers_number[wheel_number]))*(idphi-1-double(((wheel_parameters_beta[wheel_number]+1)%2))/2);
	      cdphi=cos(dphi);
	      sdphi=sin(dphi);

	      elements_translation[element_counter].set(cdphi*(rrint1p+rrext1p+rrint2m+rrext2m)/4,sdphi*(rrint1p+rrext1p+rrint2m+rrext2m)/4,0);
	      //HEREHERE
	      elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
	      rotation_values.clear();
	      rotation_values.push_back(90*deg); //ThetaX
	      rotation_values.push_back(270*deg+dphi); //PhiX
	      rotation_values.push_back(90*deg); //ThetaY
	      rotation_values.push_back(dphi); //PhiY
	      rotation_values.push_back(0*deg); //ThetaZ
	      rotation_values.push_back(0*deg); //PhiZ
	      
	      elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
	      //Fix this rotation with ThetaX=90 ThetaY=90 ThetaZ=0, PhiX=270+dphi PhiY=dphi PhiZ=0

	      //Creating volume.
	      CreateNonsensitiveKapton(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical)); //ENKP
	    };
	  
	  for (int idphi=1;idphi<=(((wheel_parameters_beta[wheel_number])%2)+1);idphi++)
	    {
	      dphi=((-360*deg)/(wheel_parameters_absorbers_number[wheel_number]))*(idphi-1-double((wheel_parameters_beta[wheel_number]%2))/2);
	      cdphi=cos(dphi);
	      sdphi=sin(dphi);
	      
	      elements_translation[element_counter].set(cdphi*(rrint1p+rrext1p+rrint2m+rrext2m)/4,sdphi*(rrint1p+rrext1p+rrint2m+rrext2m)/4,0);

	      elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
	      rotation_values.clear();
	      rotation_values.push_back(90*deg); //ThetaX
	      rotation_values.push_back(270*deg+dphi); //PhiX
	      rotation_values.push_back(90*deg); //ThetaY
	      rotation_values.push_back(dphi); //PhiY
	      rotation_values.push_back(0*deg); //ThetaZ
	      rotation_values.push_back(0*deg); //PhiZ
	      
	      elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);

	      //Fix this rotation with ThetaX=90 ThetaY=90 ThetaZ=0, PhiX=270+dphi PhiY=dphi PhiZ=0
	      
	      //Creating Volume
	      CreateNonsensitiveAbsorberWithoutLead(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical),wheel_number); //ENAB
	      
	    };
	};
      
      for (int il=0;il<wheel_radiallayers_number;il++)
	    {
	      
	      radi=cylindrical_layer_parameters_radil[il+(wheel_number*8)];
	      rado=cylindrical_layer_parameters_radol[il+(wheel_number*8)];
	      alfi=cylindrical_layer_parameters_alfil[il+(wheel_number*8)];
	      alfo=cylindrical_layer_parameters_alfol[il+(wheel_number*8)];
	      tcki=cylindrical_layer_parameters_tckil[il+(wheel_number*8)];
	      tcko=cylindrical_layer_parameters_tckol[il+(wheel_number*8)];
	      ciltet=cylindrical_layer_parameters_ciltetl[il+(wheel_number*8)];
	      alfcil=cylindrical_layer_parameters_alfcill[il+(wheel_number*8)];
	      xcnc=cylindrical_layer_parameters_xcncl[il+(wheel_number*8)];
	      ciltek=cylindrical_layer_parameters_ciltekl[il+(wheel_number*8)];
	      xcnck=cylindrical_layer_parameters_xcnckl[il+(wheel_number*8)];
	      
	      G4bool li=false;
	      G4bool lo=false;

	      if ((radi<rrmin & rado>rrmin) | (radi<rrmax & rado>rrmax)) //Check this
		{
		  if (radi<rrmin)
		    {
		      drdi=rrmin-radi;
		      drdo=0;
		      li=true;
		    };
		  if (rado>rrmax)
		    {
		      drdo=rado-rrmax;
		      drdi=0;
		      if (wheel_number==0) //if (iwh==1)
			{
			  lo=true;
			};
		    };

		  alf1=alfi;
		  alf2=alfo;
		  talf1=1/tan(alf1/2);
		  talf2=1/tan(alf2/2);
		  esalf1=tcki/sin(alf1/2);
		  esalf2=tcko/sin(alf2/2);
		  talfi=talf1+((talf2-talf1)/(rado-radi))*drdi;
		  talfo=talf2-((talf2-talf1)/(rado-radi))*drdo;
		  alfi=2*atan(1/talfi);
		  alfo=2*atan(1/talfo);
		  esalfi=esalf1+((esalf2-esalf1)/(rado-radi))*drdi;
		  esalfo=esalf2-((esalf2-esalf1)/(rado-radi))*drdo;
		  tcki=esalfi*sin(alfi/2);
		  tcko=esalfo*sin(alfo/2);
		  radi=radi+drdi;
		  rado=rado-drdo;
		};

	      //	      alfidg=alfi*(convert_radians_degrees);
	      //	      alfodg=alfo*(convert_radians_degrees);
	      
	      tckix=tcki/sin(alfi/2);
	      tckox=tcko/sin(alfo/2);
	      ciltet=0;
	      for (int it=1;it<=2;it++)
		{
		  cilox=(stac*tan(alfo/2)/4)-(tckox/2)-(rcrb*cos(ciltet)/sin(alfo/2));
		  cilix=(stac*tan(alfi/2)/4)-(tckix/2)-(rcrb*cos(ciltet)/sin(alfi/2));
		  cilx=(cilox+cilix)/2;
		  ciltet=atan((cilox-cilix)/(rado-radi));
		};
	      
	      alfclo=piby2-asin(sin(alfo/2)*cos(ciltet));
	      alfcli=piby2-asin(sin(alfi/2)*cos(ciltet));
	      alfcil=alfclo;
	      cilalf=piby2-(alfclo+alfcli)/2;
	      clcphi=atan(2*cilx/(radi+rado));
	      rcil=sqrt((pow(cilx,2)+pow((radi+rado),2))/4);
	      
	      ciloxk=(stac*tan(alfo/2)/4)-(kapton_curvature_radius/sin(alfo/2));
	      cilixk=(stac*tan(alfi/2)/4)-(kapton_curvature_radius/sin(alfi/2));
	      cilxk=(ciloxk+cilixk)/2;
	      ciltek=atan((ciloxk-cilixk)/(rado-radi));
	      xcnc=cilx;
	      xcnck=cilxk;

	      if (radi>=rrmin-0.001 & rado<=rrmax+0.001 & (ifwd==0 | ifwd==3))
		{
		  if ((li==true | lo==true) & ifwd==0)
		    {
		      //Create a shape here.
		      //Create a shape here.
		    };
		  
		  if (li==true)
		    {
		      alf=alfi;
		      tck=tcki;
		      rt1=rti1;
		      rt2=rti2;
		      phiio=-90*deg;
		      sgnz=-1;
		    };
		  
		  if (lo==true)
		    {
		      alf=alfo;
		      tck=tcko;
		      rt1=rte1;
		      rt2=rte2;
		      phiio=90*deg;
		      sgnz=1;
		    };
		  
		  if (kr==1)
		    {
		      thety=0*deg;
		      thetx=90*deg;
		      phx=90*deg;
		      phy=90*deg;
		      sgn=-1*sgnz;
		    }
		  else
		    {
		      thety=180*deg;
		      thetx=-90*deg;
		      phx=90*deg;
		      phy=270*deg;
		      sgn=1*sgnz;
		    };
		  
		  if (ifwd==0)
		    {

		      for (int idphi=1;idphi<=(wheel_parameters_beta[wheel_number]+1);idphi++)
			{
			  dphi=((360*deg)/(wheel_parameters_absorbers_number[wheel_number]))*(idphi-1-double(wheel_parameters_beta[wheel_number])/2);
			  cdphi=cos(dphi);
			  sdphi=sin(dphi);

			  elements_translation[element_counter].set(cos(dphi)*(rado+radi)/2,sin(dphi)*(rado+radi)/2,0*cm);

			  elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
			  rotation_values.clear();
 			  rotation_values.push_back(90*deg); //ThetaX
 			  rotation_values.push_back(90*deg+dphi); //PhiX
 			  rotation_values.push_back(thety); //ThetaY
 			  rotation_values.push_back(270*deg+dphi); //PhiY
 			  rotation_values.push_back(90*deg); //ThetaZ
 			  rotation_values.push_back(dphi); //PhiZ
	      
 			  elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
			  //			  G4cout<<"dphi is: "<<dphi/deg<<G4endl;
			  //G4cout<<"thety is: "<<thety/deg<<G4endl;
			  //elements_rotation[element_counter]->rotateX(-45*deg+thety/2);
			  //elements_rotation[element_counter]->rotateX(45*deg);
			  //			  real(90),real(thety),real(90),real(90+dphi),real(270+dphi),real(dphi)) 
			  //ThetaX=90 ThetaY=thety ThetaZ=90, PhiX=90+dphi PhiY=270+dphi PhiZ=dphi
			  //This solid is subject to rotation problems, ignore it for now.
			  //CreateAbsorberFlatPart2(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical)); // EBFS

			  if (li==true | lo==true)
			    {
			      ht=(stac/4/sin(alf/2))-(rcrb+(tck/2))/tan(alf/2);
			      elements_translation[element_counter].set(cdphi*(rt1+rt2)/2+sgn*sdphi*ht/2*cos(alf/2),sdphi*(rt1+rt2)/2-sgn*ht/2*cos(alf/2)*cdphi,sgnz*ht/2*sin(alf/2));
			      
			      elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
			      rotation_values.clear();
			      rotation_values.push_back(alf/2); //ThetaX
			      rotation_values.push_back(phx+dphi); //PhiX
			      rotation_values.push_back(90*deg-alf/2); //ThetaY
			      rotation_values.push_back(phy+dphi); //PhiY
			      rotation_values.push_back(sgnz*(90*deg)); //ThetaZ
			      rotation_values.push_back(dphi); //PhiZ

			      elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
			      //ThetaX=alf*raddeg/2 ThetaY=90-alf*raddeg/2 ThetaZ=sgnz*90., PhiX=phx+dphi PhiY=phy+dphi phiZ=dphi,
			      //G4cout<<"BRENDAN2: "<<G4endl;
			      CreateIronTriangle(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical),li,lo); // EZij
			    };
			};
		    }
		  else if (ifwd==3)
		    {
		      rho=rcrb+(tcki/2);
		      xtrfwi=((stac/4)+(rho*tan((CLHEP::pi-alfi)/4)*sin(alfi/2))-(rho*cos(alfi/2)))/2;
		      xtrfwo=((stac/4)+(rho*tan((CLHEP::pi-alfo)/4)*sin(alfo/2))-(rho*cos(alfo/2)))/2;
		      
		      ytrfwi=((stac/4/tan(alfi/2))+(rho*tan((CLHEP::pi-alfi)/4)*cos(alfi/2))-(rho*cos(alfi/2)/tan(alfi/2)))/2;
		      ytrfwo=((stac/4/tan(alfo/2))+(rho*tan((CLHEP::pi-alfo)/4)*cos(alfo/2))-(rho*cos(alfo/2)/tan(alfo/2)))/2;
		      
		      xtrfwd=(xtrfwi+xtrfwo)/2;
		      ytrfwd=(ytrfwi+ytrfwo)/2;
		      dxyfwd=sqrt(pow((xtrfwo-xtrfwi),2)+pow((ytrfwo-ytrfwi),2));
		      thetfw=atan((dxyfwd)/(rado-radi)); //*(convert_radians_degrees);
		      phifwd=(atan2((ytrfwo-ytrfwi),(xtrfwo-xtrfwi))+((alfi+alfo)/4))+180*deg; //*(convert_radians_degrees)+180*deg;
		      hfwdi=((stac/4/sin(alfi/2))-(rho/tan(alfi/2))-(rho*tan((CLHEP::pi-alfi)/4)))/2;
		      hfwdo=((stac/4/sin(alfo/2))-(rho/tan(alfo/2))-(rho*tan((CLHEP::pi-alfo)/4)))/2;
		      //Create a shape here.
		      
		      aa=(alfo+alfi);
		      for (int idphi=1;idphi<=((wheel_parameters_beta[wheel_number])-1);idphi++)
			{
			  dphi=((360*deg)/wheel_parameters_absorbers_number[wheel_number])*(idphi-1-(wheel_parameters_beta[wheel_number]/2))*ifront;
			  cdphi=cos(dphi);
			  sdphi=sin(dphi);
			  
			  elements_translation[element_counter].set(cdphi*(rado+radi)/2-sdphi*ytrfwd*ifront,sdphi*(rado+radi)/2+cdphi*ytrfwd*ifront,(xtrfwd-stac/8+supportingbar_container_zsize/8)*ifront);

			  elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
			  rotation_values.clear();
			  
			  //These are the proper ones
			  rotation_values.push_back(90*deg+ifront*(90*deg-aa/4)); //ThetaX
			  rotation_values.push_back(180*deg-ifront*(90*deg)+dphi); //PhiX
			  rotation_values.push_back(90*deg+ifront*(aa/4)); //ThetaY
			  rotation_values.push_back(180*deg+ifront*(90*deg)+dphi); //PhiY
			  rotation_values.push_back(90*deg); //ThetaZ
			  rotation_values.push_back(dphi); //PhiZ
			  elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
			  //Fix this rotation with ThetaX=90+ifront*(90-aa/4) ThetaY=90+ifront*aa/4 ThetaZ=90,PhiX=180-ifront*90+dphi PhiY=180+ifront*90+dphi phiZ=dphi

			  //elements_rotation[element_counter]->rotateX(90*deg+(90*deg-aa/4)); //Imposed
			  //			  elements_rotation[element_counter]->rotateZ(aa/2); //Imposed

			  //elements_rotation[element_counter]->rotateY(270*deg); //Imposed
			  //			  elements_rotation[element_counter]->rotateX(dphi); //Imposed
			  //			  elements_rotation[element_counter]->rotateZ(-aa/4); //Imposed
			  //G4cout<<"fred4: :"<<G4endl;
			  whichflatpart=4;
			  CreateAbsorberFlatPart(elements_twistedtraps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical),wheel_number); //EABS
			  //		  where_to_write=where_to_write-1;
			};
		    };
		  
		  if (ifwd==0)
		    {
		      //Create a shape here
		      
		      
		      for (int idphi=1;idphi<=(wheel_parameters_beta[wheel_number]);idphi++)
			{
			  dphi=((360*deg)/(wheel_parameters_absorbers_number[wheel_number]))*(idphi-double(wheel_parameters_beta[wheel_number]+1)/2);
			  cdphi=cos(dphi);
			  sdphi=sin(dphi);

			  elements_translation[element_counter].set(cos(dphi)*(rado+radi)/2,sin(dphi)*(rado+radi)/2,0*cm);
			  elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
			  rotation_values.clear();
 			  rotation_values.push_back(90*deg); //ThetaX
 			  rotation_values.push_back(90*deg+dphi); //PhiX
 			  rotation_values.push_back(thety); //ThetaY
 			  rotation_values.push_back(270*deg+dphi); //PhiY
 			  rotation_values.push_back(90*deg); //ThetaZ
 			  rotation_values.push_back(dphi); //PhiZ

 			  elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
			  //ThetaX=90 ThetaY=thety ThetaZ=90, PhiX=90+dphi PhiY=270+dphi PhiZ=dphi
			  
			  CreateKaptonAnotherPart(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical)); // EFKP
			  
			  if (li==true | lo==true)
			    {
			      ht=(stac/4/sin(alf/2))-(kapton_curvature_radius/tan(alf/2));
			      
			      elements_translation[element_counter].set(cdphi*(rt1+rt2)/2+sgn*sdphi*ht/2*cos(alf/2),sdphi*(rt1+rt2)/2-sgn*ht/2*cos(alf/2)*cdphi,sgnz*ht/2*sin(alf/2));

			      elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
			      rotation_values.clear();
			      rotation_values.push_back(alf/2); //ThetaX
			      rotation_values.push_back(phx+dphi); //PhiX
			      rotation_values.push_back(90*deg-alf/2); //ThetaY
			      rotation_values.push_back(phy+dphi); //PhiY
			      rotation_values.push_back(sgnz*(90*deg)); //ThetaZ
			      rotation_values.push_back(dphi); //PhiZ
			      
			      elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
			      //ThetaX=alf*raddeg/2 ThetaY=90-alf*raddeg/2 ThetaZ=sgnz*90, PhiX=phx+dphi PhiY=phy+dphi phiZ=dphi

			      CreateKaptonTriangle(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical),li,lo); // ETij
			    };
			};
		    }
		  else if (ifwd==3)
		    {

		      rho=kapton_curvature_radius;
		      xtrfwi=((stac/4)+(rho*tan((CLHEP::pi-alfi)/4)*sin(alfi/2))-(rho*cos(alfi/2)))/2;
		      xtrfwo=((stac/4)+(rho*tan((CLHEP::pi-alfo)/4)*sin(alfo/2))-(rho*cos(alfo/2)))/2;

		      
		      ytrfwi=((stac/4/tan(alfi/2))+(rho*tan((CLHEP::pi-alfi)/4)*cos(alfi/2))-(rho*cos(alfi/2)/tan(alfi/2)))/2;
		      ytrfwo=((stac/4/tan(alfo/2))+(rho*tan((CLHEP::pi-alfo)/4)*cos(alfo/2))-(rho*cos(alfo/2)/tan(alfo/2)))/2;

		      xtrfwd=(xtrfwi+xtrfwo)/2;
		      ytrfwd=(ytrfwi+ytrfwo)/2;
		      dxyfwd=sqrt(pow((xtrfwo-xtrfwi),2)+pow((ytrfwo-ytrfwi),2));
		      thetfw=atan(dxyfwd/(rado-radi)); //*(convert_radians_degrees);
		      phifwd=(atan2((ytrfwo-ytrfwi),(xtrfwo-xtrfwi))+((alfi+alfo)/4))+180*deg; //*(convert_radians_degrees)+180*deg;
		      hfwdi=((stac/4/sin(alfi/2))-(rho/tan(alfi/2))-(rho*tan((CLHEP::pi-alfi)/4)))/2;
		      hfwdo=((stac/4/sin(alfo/2))-(rho/tan(alfo/2))-(rho*tan((CLHEP::pi-alfo)/4)))/2;
		      //Create a shape here.
		      aa=alfo+alfi;//Just changes this
		      
		      for (int idphi=1;idphi<=2;idphi++)
			{
			  dphi=((360*deg)/(wheel_parameters_absorbers_number[wheel_number]))*(idphi-double((wheel_parameters_beta[wheel_number]+1))/2)*ifront;

			  cdphi=cos(dphi);
			  sdphi=sin(dphi);
			  
			  elements_translation[element_counter].set(cdphi*(rado+radi)/2-sdphi*ytrfwd*ifront,sdphi*(rado+radi)/2+cdphi*ytrfwd*ifront,(xtrfwd-stac/8+supportingbar_container_zsize/8)*ifront);


			  elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
			  rotation_values.clear();
			  rotation_values.push_back(90*deg+ifront*(90*deg-aa/4)); //ThetaX
			  rotation_values.push_back(180*deg-ifront*(90*deg)+dphi); //PhiX
			  rotation_values.push_back(90*deg+ifront*(aa/4)); //ThetaY
			  rotation_values.push_back(180*deg+ifront*(90*deg)+dphi); //PhiY
			  rotation_values.push_back(90*deg); //ThetaZ
			  rotation_values.push_back(dphi); //PhiZ
			  
			  elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
			  //Fix this rotation with ThetaX=90+ifront*(90-aa/4) ThetaY=90+ifront*aa/4 ThetaZ=90, PhiX=180-ifront*90+dphi, PhiY=180+ifront*90+dphi phiZ=dphi


			  //ThetaX=aa/4 ThetaY=90-aa/4 ThetaZ=90, PhiX=90+dphi PhiY=270+dphi phiZ=dphi
			  
			  CreateKaptonPlane(elements_twistedtraps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(current_logical)); // EKAP
			  //Create a shape here.
			};
		    };
		};
	    };
	};
}

void DetectorConstruction::CreateAbsorberFold(G4Cons*& absorber_fold_solid,G4LogicalVolume*& absorber_fold_logical,G4ThreeVector absorber_fold_translation,G4RotationMatrix* absorber_fold_rotation,G4Material* pMaterial,G4LogicalVolume* parent_logical)
{
  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String absorber_fold_namestub="absorber_fold-"+number.str();
  G4double absorber_fold_pRmin1=absorber_innercurvature_radius;
  G4double absorber_fold_pRmax1=absorber_innercurvature_radius+tcki;
  G4double absorber_fold_pRmin2=absorber_innercurvature_radius;
  G4double absorber_fold_pRmax2=absorber_innercurvature_radius+tcko;
  G4double absorber_fold_pDz=(rado-radi)/cos(ciltet)/2;
  G4double absorber_fold_pSPhi=180*deg;
  G4double absorber_fold_pDPhi=-alfcil;
//   G4double absorber_fold_pSPhi=180*deg-alfcil;
//   G4double absorber_fold_pDPhi=180*deg;



  //Solid
  CreateSolid(absorber_fold_namestub,absorber_fold_pRmin1,absorber_fold_pRmax1,absorber_fold_pRmin2,absorber_fold_pRmax2,absorber_fold_pDz,absorber_fold_pSPhi,absorber_fold_pDPhi);
  

  //Logical Volume
  CreateLogicalVolume(absorber_fold_solid,pMaterial,absorber_fold_namestub);

  //Physical volume
  CreatePhysicalVolume(absorber_fold_rotation,absorber_fold_translation,absorber_fold_logical,absorber_fold_namestub,parent_logical);
  element_counter=element_counter+1;

  for (G4int cone_number=0;cone_number<2;cone_number++)
    {
      //-5-CreateIronFold(elements_cons[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],absorber_fold_logical,cone_number);
      //-6-CreateGlueFold(elements_cons[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],absorber_fold_logical,cone_number);
    };

  
}

void DetectorConstruction::CreateIronFold(G4Cons*& iron_fold_solid,G4LogicalVolume*& iron_fold_logical,G4ThreeVector iron_fold_translation,G4RotationMatrix* iron_fold_rotation,G4LogicalVolume* parent_logical,G4int cone_number)
{
  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String iron_fold_namestub="iron_fold-"+number.str();
  G4double iron_fold_pRmin1=absorber_innercurvature_radius;
  G4double iron_fold_pRmax1=absorber_innercurvature_radius+tcki;
  G4double iron_fold_pRmin2=absorber_innercurvature_radius;
  G4double iron_fold_pRmax2=absorber_innercurvature_radius+tcko;
  G4double iron_fold_pDz=(rado-radi)/cos(ciltet)/2;
  G4double iron_fold_pSPhi=180*deg;
  G4double iron_fold_pDPhi=-alfcil;

  //  G4double iron_fold_pSPhi=180*deg-alfcil;
  //  G4double iron_fold_pDPhi;


  if (cone_number==0)
    {

      iron_fold_pRmin1=(absorber_innercurvature_radius+tcki-(stainlesssteel_thickness/2));
      iron_fold_pRmin2=(absorber_innercurvature_radius+tcko-(stainlesssteel_thickness/2));
    }
  else
    {

      iron_fold_pRmax1=(absorber_innercurvature_radius+(stainlesssteel_thickness/2));
      iron_fold_pRmax2=(absorber_innercurvature_radius+(stainlesssteel_thickness/2));
    };

  //Solid
  CreateSolid(iron_fold_namestub,iron_fold_pRmin1,iron_fold_pRmax1,iron_fold_pRmin2,iron_fold_pRmax2,iron_fold_pDz,iron_fold_pSPhi,iron_fold_pDPhi);

  //Logical Volume
  CreateLogicalVolume(iron_fold_solid,Fe,iron_fold_namestub);

  //Physical volume
  CreatePhysicalVolume(iron_fold_rotation,iron_fold_translation,iron_fold_logical,iron_fold_namestub,parent_logical);
  element_counter=element_counter+1;

}

void DetectorConstruction::CreateGlueFold(G4Cons*& glue_fold_solid,G4LogicalVolume*& glue_fold_logical,G4ThreeVector glue_fold_translation,G4RotationMatrix* glue_fold_rotation,G4LogicalVolume* parent_logical,G4int cone_number)
{
  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String glue_fold_namestub="glue_fold-"+number.str();
  G4double glue_fold_pRmin1;
  G4double glue_fold_pRmax1;
  G4double glue_fold_pRmin2;
  G4double glue_fold_pRmax2;
  G4double glue_fold_pDz=(rado-radi)/cos(ciltet)/2;
  G4double glue_fold_pSPhi=180*deg;
  G4double glue_fold_pDPhi=-alfcil;
  //  G4double glue_fold_pSPhi=180*deg-alfcil;
  //  G4double glue_fold_pDPhi;

  if (cone_number==0)
    {
      glue_fold_pRmin1=(absorber_innercurvature_radius+tcki-(stainlesssteel_thickness/2)-(glue_thickness/2));
      glue_fold_pRmax1=absorber_innercurvature_radius+tcki-(stainlesssteel_thickness/2);
      glue_fold_pRmin2=(absorber_innercurvature_radius+tcko-(stainlesssteel_thickness/2)-(glue_thickness/2));
      glue_fold_pRmax2=absorber_innercurvature_radius+tcko-(stainlesssteel_thickness/2);
    }
  else
    {
      glue_fold_pRmin1=absorber_innercurvature_radius+(stainlesssteel_thickness/2);
      glue_fold_pRmax1=(absorber_innercurvature_radius+(stainlesssteel_thickness/2)+(glue_thickness/2));
      glue_fold_pRmin2=absorber_innercurvature_radius+(stainlesssteel_thickness/2);
      glue_fold_pRmax2=(absorber_innercurvature_radius+(stainlesssteel_thickness/2)+(glue_thickness/2));
    };
      
  //Solid
  CreateSolid(glue_fold_namestub,glue_fold_pRmin1,glue_fold_pRmax1,glue_fold_pRmin2,glue_fold_pRmax2,glue_fold_pDz,glue_fold_pSPhi,glue_fold_pDPhi);
  
  //Logical Volume
  CreateLogicalVolume(glue_fold_solid,FibreGlass,glue_fold_namestub);

  //Physical volume
  CreatePhysicalVolume(glue_fold_rotation,glue_fold_translation,glue_fold_logical,glue_fold_namestub,parent_logical);
  element_counter=element_counter+1;

}









  













DetectorConstruction::DetectorConstruction()
{
  //Some parameters
  for (int i=0;i<300;i++)
    {

    };
  element_counter=0;


  for (int kk=0;kk<20000;kk++)
    {
      elements_cons.push_back(0);
      elements_tubs.push_back(0);
      elements_twistedtraps.push_back(0);
      elements_traps.push_back(0);
      elements_logical.push_back(0);
      elements_physical.push_back(0);
      elements_translation.push_back(G4ThreeVector());
      elements_rotation.push_back(0);
      elements_tgbrotation.push_back(0);
      elements_solid_names.push_back("None");
      elements_logical_names.push_back("None");
      elements_physical_names.push_back("None");
    };


  convert_degrees_radians=CLHEP::pi/180;
  convert_radians_degrees=180/CLHEP::pi;

 //Set the parameters ------------------------------------------------------------
  granularity_packet_eta=0.025; //Eta granularity packet
  granularity_packet_phi=(CLHEP::twopi/768)*3; //minimum phi granulatiry (why *3?)

  //Geometry parameters
  calorimeter_internal_radius=29*cm; //Internal radius of the calorimeter
  calorimeter_external_radius=210*cm; //External radius of the calorimeter
  emec_z_origin=364.1*cm; //EMEC z origin
  calorimeter_length=63.2*cm; //Length of calorimeter
  lar_nominal_origin=369.1*cm; //Nominal origin of lAr, used to calculate the folds
  wheel_reference_point=368.95*cm; //New position of wheel reference point in Atlas system
  cell_focalpoint_distance=368.9*cm; //Distance of cell focal point from the wheel reference point.
  charge_collection_onoff=0; //Switch for charge collection: 0=off, 1=on THIS COULD BE A BOOL
  massless_gap_onoff=0; //Switch for massless gap: 0=off, 1=on THIS COULD BE A BOOL
  absorber_startingpart_length=1.3*cm; //The length of the starting part of the absorber
  supportingbar_thickness=1.5*cm; //The thickness of the supporting bar
  supportingbar_groove_depth=0.5*cm; //The depth of the groove in the supporting bar
  supportingbar_container_zsize=1*cm; //The z size of the cylinder containg THE SUPPORTING BAR?
  lar_totalthickness=53.6*cm; //The total thickness of the lar part.
  active_maximumradius=203.4*cm; //The maximum radius of the active part.
  precision_solving_nonlinear=0.000001; //The presicion for solving the nonlinear EQUATION?
  absorber_innercurvature_radius=0.18*cm; //The inner curvature radius of the absorber
  kapton_curvature_radius=0.3*cm; //Curvature radius of the kapton
  kapton_thickness=0.024*cm; //Kapton thickness
  copper_thickness=0.006*cm; //Copper thickness
  glue_thickness=0.03*cm; //Glue thickness
  stainlesssteel_thickness=0.04*cm; //Stainless steel thickness
  wheel_radiallayers_number=8; //Number of radial layers in each wheel
  wheel_betweenwheels_semigap=0.15*cm; //Semi gap between wheels
  supportingdisc_thickness=2.0*cm; //Thickness of disc supporting zig-zag structure;
  externalsupportingdisc_thickness=2.1*cm; //Thickness of external disc supporting zig-zag structure;
  mediumsupportingdisc_thickness=2.7*cm; //Thickness of medium disc supporting zig-zag structure;
  internalsupportingdisc_thickness=2.7*cm; //Thickness of internal disc supporting zig-zag structure;
  uniformregions_number=11; //Number of uniform regions
  granulatiry_regions_eta[0]=granularity_packet_eta*4;
  granulatiry_regions_eta[1]=granularity_packet_eta*4;
  granulatiry_regions_eta[2]=granularity_packet_eta;
  granulatiry_regions_eta[3]=granularity_packet_eta/4;
  granulatiry_regions_eta[4]=granularity_packet_eta/6;
  granulatiry_regions_eta[5]=granularity_packet_eta/8;
  granulatiry_regions_eta[6]=granularity_packet_eta;
  granulatiry_regions_eta[7]=granularity_packet_eta;
  granulatiry_regions_eta[8]=granularity_packet_eta*2;
  granulatiry_regions_eta[9]=granularity_packet_eta*2;
  granulatiry_regions_eta[10]=granularity_packet_eta*2; //Eta granulatiry in regions
  granularity_regions_phi[0]=granularity_packet_phi*4;
  granularity_regions_phi[1]=granularity_packet_phi*4;
  granularity_regions_phi[2]=granularity_packet_phi*4;
  granularity_regions_phi[3]=granularity_packet_phi*4;
  granularity_regions_phi[4]=granularity_packet_phi*4;
  granularity_regions_phi[5]=granularity_packet_phi*4;
  granularity_regions_phi[6]=granularity_packet_phi*4;
  granularity_regions_phi[7]=granularity_packet_phi;
  granularity_regions_phi[8]=granularity_packet_phi;
  granularity_regions_phi[9]=granularity_packet_phi*4;
  granularity_regions_phi[10]=granularity_packet_phi; //Phi granularity in regions.
  zones_differentstripwidth_eta[0]=2.5;
  zones_differentstripwidth_eta[1]=2.4;
  zones_differentstripwidth_eta[2]=2.0;
  zones_differentstripwidth_eta[3]=1.8;
  zones_differentstripwidth_eta[4]=1.5;
  zones_differentstripwidth_eta[5]=1.425;
  zones_differentstripwidth_eta[6]=1.375; //Eta zones with different strip widths
  hit_maximumenergy=40*eV; //Maximum energy of a hit
  digit_maximumenergy=500*eV; //Maximum energy of a digit
  signalnormalzation_gapsize=0.13*cm; //Average gap size for the signal normalization
  barrel_endcap_gap=4*cm; //Opening of the gap between the barral and endcap

  //inner_wheel parameters
  inner_wheel_absorbers_number=256;//256; //Number of absorbers in inner wheel
  inner_wheel_absorbers_numwaves=6;//3; //Number of waves on the absorber
  inner_wheel_eta_int=3.2; //Maximum eta of the inner wheel
  inner_wheel_eta_ext=2.5; //Minimum eta of the inner wheel
  inner_wheel_firstsampling_numwaves=3; //Number of waves in the first sampling
  inner_wheel_beta=4; //Beta parameter of the inner wheel
  inner_wheel_alpha_inner=107*deg; //Inner opening angle
  inner_wheel_alpha_outer=64.5*deg; //Outer opening angle
  inner_wheel_lead_thickness=0.22*cm; //Thickness of the lead layer.

  //outer_wheel parameters
  outer_wheel_absorbers_number=768; //Number of absorbers in outer wheel
  outer_wheel_absorbers_numwaves=9; //Number of waves on the absorber
  outer_wheel_eta_int=2.5; //Maximum eta of the outer wheel
  outer_wheel_eta_ext=1.375; //Minimum eta of the outer wheel
  outer_wheel_firstsampling_numwaves=3; //Number of waves in the first sampling
  outer_wheel_beta=3; //Beta parameter of the outer wheel
  outer_wheel_alpha_inner=122.5*deg; //Inner opening angle
  outer_wheel_alpha_outer=59*deg; //Outer opening angle
  outer_wheel_lead_thickness=0.17*cm; //Thickness of the lead layer.

  //  Arrays combining both inner and outer with the 0 and 1 index - wheel_parameters_* array.
  wheel_parameters_absorbers_number[0]=inner_wheel_absorbers_number;
  wheel_parameters_absorbers_number[1]=outer_wheel_absorbers_number; //Number of absorbers in wheels
  wheel_parameters_absorbers_numwaves[0]=inner_wheel_absorbers_numwaves;
  wheel_parameters_absorbers_numwaves[1]=outer_wheel_absorbers_numwaves; //Number of waves on the absorbers
  wheel_parameters_eta_int[0]=inner_wheel_eta_int;
  wheel_parameters_eta_int[1]=outer_wheel_eta_int; //Maximum eta of the wheels
  wheel_parameters_eta_ext[0]=inner_wheel_eta_ext;
  wheel_parameters_eta_ext[1]=outer_wheel_eta_ext; //Minimum eta of the wheels
  wheel_parameters_firstsampling_numwaves[0]=inner_wheel_firstsampling_numwaves;
  wheel_parameters_firstsampling_numwaves[1]=outer_wheel_firstsampling_numwaves; //Number of waves in the first sampling
  wheel_parameters_beta[0]=inner_wheel_beta;
  wheel_parameters_beta[1]=outer_wheel_beta; //Beta parameter of the wheels
  wheel_parameters_alpha_inner[0]=inner_wheel_alpha_inner;
  wheel_parameters_alpha_inner[1]=outer_wheel_alpha_inner; //Inner opening angle
  wheel_parameters_alpha_outer[0]=inner_wheel_alpha_outer;
  wheel_parameters_alpha_outer[1]=outer_wheel_alpha_outer; //Outer opening angle
  wheel_parameters_lead_thickness[0]=inner_wheel_lead_thickness;
  wheel_parameters_lead_thickness[1]=outer_wheel_lead_thickness; //Thickness of the lead layer.
  //  wheel_parameters_theta_int[0]=(2*atan(exp(-inner_wheel_eta_int)));
  //wheel_parameters_theta_int[1]=(2*atan(exp(-outer_wheel_eta_int))); //Theta corresponding to maximum eta of the wheels.
  //wheel_parameters_theta_ext[0]=(2*atan(exp(-inner_wheel_eta_ext)));
  //wheel_parameters_theta_ext[1]=(2*atan(exp(-outer_wheel_eta_ext))); //Theta corresponding to minimum eta of the wheels.
  ////////////////////////////temp
  wheel_parameters_absorbers_number_temp[0]=256;//256;
  wheel_parameters_absorbers_number_temp[1]=768;//768; //Number of absorbers in wheels_temp
  
  ///////////////////////////temp2

  //calibration constants
  samplingfraction_fit_innerwheel[0]=1.1753;
  samplingfraction_fit_innerwheel[1]=-0.44802;
  samplingfraction_fit_innerwheel[2]=0.16489;
  samplingfraction_fit_innerwheel[3]=-0.00994; //p3 fit for sampfrac/gap^1.3 vs eta for inner wheel.
  samplingfraction_fit_outerwheel[0]=1.1205;
  samplingfraction_fit_outerwheel[1]=-0.32666;
  samplingfraction_fit_outerwheel[2]=0.26374;
  samplingfraction_fit_outerwheel[3]=-0.02668; //p3 fit for sampfrac/gap^1.3 vs eta for outer wheel.
  scale_correcttion=1.055; //Ratio calculation/simulation

  //sampling separation
  padcounting_eta_extimum=1.4; //Low eta for pad counting
  padcounting_width=0.025*cm;
  separation_inner_wheel_compartment[0]=413.9337*cm;
  separation_inner_wheel_compartment[1]=412.5181*cm;
  separation_inner_wheel_compartment[2]=411.7915*cm;
  separation_inner_wheel_compartment[3]=409.5447*cm;
  separation_inner_wheel_compartment[4]=407.9870*cm;
  separation_inner_wheel_compartment[5]=407.5103*cm;
  separation_inner_wheel_compartment[6]=404.7296*cm; //Inner wheel compartment separation
  separation_z12[20]=378.39800*cm;
  separation_z12[1]=378.13725*cm;
  separation_z12[2]=378.27650*cm;
  separation_z12[3]=378.16652*cm;
  separation_z12[4]=378.15500*cm;
  separation_z12[5]=378.18146*cm;
  separation_z12[6]=378.18883*cm;
  separation_z12[7]=378.19607*cm;
  separation_z12[8]=378.20319*cm;
  separation_z12[9]=378.21017*cm;
  separation_z12[10]=378.21702*cm;
  separation_z12[11]=378.22372*cm;
  separation_z12[12]=378.23027*cm;
  separation_z12[13]=378.23668*cm;
  separation_z12[14]=378.24293*cm;
  separation_z12[15]=378.24902*cm;
  separation_z12[16]=378.25495*cm;
  separation_z12[17]=378.26070*cm;
  separation_z12[18]=378.26629*cm;
  separation_z12[19]=378.27169*cm;
  separation_z12[20]=378.27690*cm;
  separation_z12[21]=378.28193*cm;
  separation_z12[22]=378.28677*cm;
  separation_z12[23]=378.29140*cm;
  separation_z12[24]=378.29583*cm;
  separation_z12[25]=378.30005*cm;
  separation_z12[26]=378.30406*cm;
  separation_z12[27]=378.30784*cm;
  separation_z12[28]=378.34395*cm;
  separation_z12[29]=378.35307*cm;
  separation_z12[30]=378.36199*cm;
  separation_z12[31]=378.37074*cm;
  separation_z12[32]=378.37928*cm;
  separation_z12[33]=378.38761*cm;
  separation_z12[34]=378.39573*cm;
  separation_z12[35]=378.39573*cm;
  separation_z12[36]=376.15025*cm;
  separation_z12[37]=376.15025*cm;
  separation_z12[38]=376.08950*cm;
  separation_z12[39]=376.08950*cm;
  separation_z12[40]=375.76800*cm;
  separation_z12[41]=375.90725*cm;
  separation_z12[42]=375.64650*cm;
  separation_z12[43]=375.78575*cm; 

  separation_z23[0]=999.999*cm;
  separation_z23[1]=999.999*cm;
  separation_z23[2]=413.20542*cm;
  separation_z23[3]=413.10763*cm;
  separation_z23[4]=412.53569*cm;
  separation_z23[5]=412.24576*cm;
  separation_z23[6]=412.26122*cm;
  separation_z23[7]=410.29246*cm;
  separation_z23[8]=410.29246*cm;
  separation_z23[9]=410.26562*cm;
  separation_z23[10]=410.25252*cm;
  separation_z23[11]=409.69822*cm;
  separation_z23[12]=409.50664*cm;
  separation_z23[13]=407.38440*cm;
  separation_z23[14]=407.15652*cm;
  separation_z23[15]=406.70284*cm;
  separation_z23[16]=404.44630*cm;
  separation_z23[17]=404.36430*cm;
  separation_z23[18]=403.97155*cm;
  separation_z23[19]=401.59031*cm;
  separation_z23[20]=401.32156*cm;
  separation_z23[21]=401.15312*cm;

  //More parameters

  //  scrb1=supportingbar_container_zsize+0.001*cm; //I don't know what this does yet!

  for (int i=0;i<2;i++)
    {
      rcrb=absorber_innercurvature_radius;
      z2=lar_nominal_origin+lar_totalthickness;
      zpk=lar_nominal_origin+(0.5*lar_totalthickness);
      wheel_parameters_theta_int[i]=(2*atan(exp(-wheel_parameters_eta_int[i])));
      wheel_parameters_theta_ext[i]=(2*atan(exp(-wheel_parameters_eta_ext[i])));
      wheel_parameters_internal_radius[i]=zpk*tan(wheel_parameters_theta_int[i]);
      wheel_parameters_external_radius[i]=zpk*tan(wheel_parameters_theta_ext[i]);
      wheel_parameters_external_radius[i]=std::min(wheel_parameters_external_radius[i],active_maximumradius);
      wheel_parameters_d=(lar_totalthickness-(2*absorber_startingpart_length))/2/wheel_parameters_absorbers_numwaves[i];
      wheel_parameters_eabs[i]=wheel_parameters_lead_thickness[i]+stainlesssteel_thickness+glue_thickness;
      wheel_parameters_rho[i]=absorber_innercurvature_radius+(wheel_parameters_eabs[i]/2);

      wheel_parameters_eopt[0]=wheel_parameters_lead_thickness[i];
      wheel_parameters_eopt[1]=wheel_parameters_lead_thickness[i]; //Force both of these to the same value, but the values will be different for both wheels.

      wheel_parameters_ropt[0]=wheel_parameters_internal_radius[i];
      wheel_parameters_ropt[1]=wheel_parameters_external_radius[i]; //Same here hard coding, why?

      wheel_parameters_zlopt[0]=(wheel_parameters_d/sin(wheel_parameters_alpha_inner[i]/2))-((2*wheel_parameters_rho[i])/tan(wheel_parameters_alpha_inner[i]/2))+((CLHEP::pi-wheel_parameters_alpha_inner[i])*wheel_parameters_rho[i]);
      wheel_parameters_zlopt[1]=(wheel_parameters_d/sin(wheel_parameters_alpha_outer[i]/2))-((2*wheel_parameters_rho[i])/tan(wheel_parameters_alpha_outer[i]/2))+((CLHEP::pi-wheel_parameters_alpha_outer[i])*wheel_parameters_rho[i]);

      wheel_parameters_internal_radius[i]=(lar_nominal_origin*tan(wheel_parameters_theta_int[i]));
      wheel_parameters_external_radius[i]=(z2*tan(wheel_parameters_theta_ext[i]));
      wheel_parameters_external_radius[i]=std::min(wheel_parameters_external_radius[i],active_maximumradius);
      stepr=pow((wheel_parameters_external_radius[i]/wheel_parameters_internal_radius[i]),(1/wheel_radiallayers_number));

      for (int j=0;j<=wheel_radiallayers_number;j++)
	{

	  r=wheel_parameters_internal_radius[i]*pow(stepr,j);
	  rr[j]=r;
	  e=((wheel_parameters_eopt[0]*(wheel_parameters_ropt[1]-r))+(wheel_parameters_eopt[1]*(r-wheel_parameters_ropt[0])))/(wheel_parameters_ropt[1]-wheel_parameters_ropt[0]);
	  zigl=(wheel_parameters_zlopt[0]*(wheel_parameters_ropt[1]-r)+(wheel_parameters_zlopt[1]*(r-wheel_parameters_ropt[0])))/(wheel_parameters_ropt[1]-wheel_parameters_ropt[0]);
	  wheel_parameters_salf2n=wheel_parameters_d/zigl;
	  salf2=-1;
	  //Start looking here for the error!

	  while (fabs(wheel_parameters_salf2n-salf2)>precision_solving_nonlinear)
	    {
	      salf2=wheel_parameters_salf2n;

	      alf2=asin(salf2);
	      alf=2*alf2;
	      calf2=cos(alf2);
	      talf2=salf2/calf2;
	      alfdeg=alf*(convert_radians_degrees);
	      epb=e;
	      wheel_parameters_eabs[i]=epb+glue_thickness+stainlesssteel_thickness;
	      wheel_parameters_rho[i]=absorber_innercurvature_radius+(wheel_parameters_eabs[i]/2);
	      wheel_parameters_salf2n=(wheel_parameters_d/(zigl+(2*wheel_parameters_rho[i]/talf2)-(CLHEP::pi-alf)*wheel_parameters_rho[i]));


	    };
	  alphar[j]=alfdeg;
	  eabsr[j]=epb+stainlesssteel_thickness+glue_thickness;
	};

      thfc=kapton_thickness+copper_thickness;
      stac=(lar_totalthickness-2*(absorber_startingpart_length))/wheel_parameters_absorbers_numwaves[i];

      for (int k=0;k<wheel_radiallayers_number;k++)
	{
	  radi=rr[k];
	  rado=rr[k+1];
	  alfidg=alphar[k];
	  alfodg=alphar[k+1];
	  tcki=eabsr[k];
	  tcko=eabsr[k+1];

	  alfo=(convert_degrees_radians)*(alfodg);
	  alfi=(convert_degrees_radians)*(alfidg);

	  scrb1=supportingbar_container_zsize+0.001*cm;
	  sroue1=(stac/2)-scrb1;
	  sroue=sroue1-0.002;

	  tckix=tcki/sin(alfi/2);
	  tckox=tcko/sin(alfo/2);
	  zig=sroue/tan(alfo/2)+tcko;

	  ciltet=0;

	  for (int l=1;l<=2;l++)
	    { //I don't know why this is a loop
	      cilox=((stac/4/tan(alfo/2))-(tckox/2)-(rcrb/sin(alfo/2)/cos(ciltet)));
	      cilix=((stac/4/tan(alfi/2))-(tckix/2)-(rcrb/sin(alfi/2)/cos(ciltet)));
	      cilx=(cilox+cilix)/2;
	      ciltet=atan((cilox-cilix)/(rado-radi));
	    };

	  alfclo=piby2-asin(sin(alfo/2)*cos(ciltet));
	  alfcli=piby2-asin(sin(alfi/2)*cos(ciltet));
	  alfcil=alfclo;
	  cilalf=piby2-(alfclo+alfcli)/2;
	  clcphi=atan(2*(cilx/(radi+rado)));
	  rcil=sqrt(pow(cilx,2)+pow((radi+rado),2)/4);
	  ciloxk=((stac/4/tan(alfo/2))-(kapton_curvature_radius/sin(alfo/2)));
	  cilixk=((stac/4/tan(alfi/2))-(kapton_curvature_radius/sin(alfi/2)));
	  cilxk=(ciloxk+cilixk)/2;
	  ciltek=atan((ciloxk-cilixk)/(rado-radi));

	  cylindrical_layer_parameters_radil[k+(i*8)]=radi;
	  cylindrical_layer_parameters_radol[k+(i*8)]=rado;
	  cylindrical_layer_parameters_alfil[k+(i*8)]=alfi;
	  cylindrical_layer_parameters_alfol[k+(i*8)]=alfo;
	  cylindrical_layer_parameters_tckil[k+(i*8)]=tcki;
	  cylindrical_layer_parameters_tckol[k+(i*8)]=tcko;
	  cylindrical_layer_parameters_ciltetl[k+(i*8)]=ciltet;
	  cylindrical_layer_parameters_alfcill[k+(i*8)]=alfcil;
	  cylindrical_layer_parameters_xcncl[k+(i*8)]=cilx;
	  cylindrical_layer_parameters_ciltekl[k+(i*8)]=ciltek;
	  cylindrical_layer_parameters_xcnckl[k+(i*8)]=cilxk;

	};


    };

  dxiro=0;
  dyiro=0;
  driro=0;
  dxglu=0;
  dyglu=0;
  drglu=0;


  //End set the parameters -------------------------------------------------------

}

void DetectorConstruction::WriteToLog(G4String message,G4int writing_code)
{
  //  writing_code=2;
  //Writes messages to screen, file, both, none.
  if (writing_code==1) //Write to screen only
    {
      G4cout<<message<<G4endl;
    }
  else if (writing_code==0) //Suppress all writing
    {
      //Do nothing
    }
  else if (writing_code==2) //Write to file and screen
    {
      G4cout<<message<<G4endl;
      std::ofstream logfile;
      logfile.open("logfile.txt",std::ios::app);
      logfile<<message<<"\n";
      logfile.close();
    };
}
    
// G4Cons* DetectorConstruction::GetSolidPointerCons(G4int counter)
// {
//   return elements_cons[counter];
// }

// void DetectorConstruction::SetSolidPointerCons(G4Cons* pointer)
// {
// elements_cons[

G4LogicalVolume* DetectorConstruction::GetLogicalVolume(G4String name)
{
  G4LogicalVolume* pointer=0;
  
  for (unsigned int i=0;i<elements_logical_names.size();i++)
    {
      if (elements_logical_names[i]==name)
	{
	  pointer=elements_logical[i];
	  break;
	};
    };

  return pointer;
}

G4String DetectorConstruction::GetLogicalVolumeName(G4LogicalVolume* logical_volume)
{

  G4String logical_volume_name;

  for (unsigned int i=0;i<elements_logical.size();i++)
    {
      if (elements_logical[i]==logical_volume)
	{
	  logical_volume_name=elements_logical_names[i];
	  break;
	};
    };

  return logical_volume_name;
}

G4VPhysicalVolume* DetectorConstruction::GetPhysicalVolume(G4String name)
{
  G4VPhysicalVolume* pointer=0;
  
  for (unsigned int i=0;i<elements_physical_names.size();i++)
    {
      if (elements_physical_names[i]==name)
	{
	  pointer=elements_physical[i];
	  break;
	};
    };

  return pointer;
}


DetectorConstruction::~DetectorConstruction()
{
}

G4Tubs* DetectorConstruction::CreateSolid(G4String solid_name,G4double solid_pRmin,G4double solid_pRmax,G4double solid_pDz,G4double solid_pSPhi,G4double solid_pDPhi)
{
  std::stringstream pointer;
  std::stringstream dimensions;
  std::stringstream elemcount;
  WriteToLog("++++++++++++++++++++Started creating solid++++++++++++++++++++", where_to_write);
  elemcount<< "Element counter: "<<element_counter;
  WriteToLog(elemcount.str(),where_to_write);
  WriteToLog("Type = G4Tubs", where_to_write);
  elements_solid_names[element_counter]=solid_name+"_solid";
  WriteToLog("Name = "+elements_solid_names[element_counter],where_to_write);
  pointer << elements_tubs[element_counter];
  WriteToLog("Pointer before creation of solid = "+pointer.str(), where_to_write);
  dimensions << "Dimensions:     pDz: "<<(solid_pDz/10);
  WriteToLog(dimensions.str(),where_to_write);
  dimensions.str("");
  dimensions << "                pRmin: "<<(solid_pRmin/10)<<" pRmax: "<<(solid_pRmax/10);
  WriteToLog(dimensions.str(),where_to_write);
  dimensions.str("");
  dimensions << "                pSPhi: "<<(solid_pSPhi)*convert_radians_degrees<<" pDPhi: "<<(solid_pDPhi)*convert_radians_degrees<<" pEPhi: "<<(solid_pSPhi+solid_pDPhi)*convert_radians_degrees;
  WriteToLog(dimensions.str(),where_to_write);
  dimensions.str("");


  elements_tubs[element_counter] = new G4Tubs(solid_name+"_solid",solid_pRmin,solid_pRmax,solid_pDz,solid_pSPhi,solid_pDPhi);

  pointer.str("");
  pointer << elements_tubs[element_counter];
  WriteToLog("Pointer after creation of solid = "+pointer.str(), where_to_write);
  WriteToLog("++++++++++++++++++++Finished creating solid++++++++++++++++++++\n", where_to_write);
  return elements_tubs[element_counter];
}

G4Cons* DetectorConstruction::CreateSolid(G4String solid_name,G4double solid_pRmin1,G4double solid_pRmax1,G4double solid_pRmin2,G4double solid_pRmax2,G4double solid_pDz,G4double solid_pSPhi,G4double solid_pDPhi)
{
  std::stringstream pointer;
  std::stringstream dimensions;
  std::stringstream elemcount;
  WriteToLog("++++++++++++++++++++Started creating solid++++++++++++++++++++",where_to_write);
  elemcount<< "Element counter: "<<element_counter;
  WriteToLog(elemcount.str(),where_to_write);
  WriteToLog("Type = G4Cons",where_to_write);
  elements_solid_names[element_counter]=solid_name+"_solid";
  WriteToLog("Name = "+elements_solid_names[element_counter],where_to_write);
  pointer << elements_cons[element_counter];
  WriteToLog("Pointer before creation of solid = "+pointer.str(),where_to_write);
  dimensions << "Dimensions:    pDz: "<<(solid_pDz/10);
  WriteToLog(dimensions.str(),where_to_write);
  dimensions.str("");
  dimensions << "               pRmin1: "<<(solid_pRmin1/10)<<" pRmax1: "<<(solid_pRmax1/10);
  WriteToLog(dimensions.str(),where_to_write);
  dimensions.str("");
  dimensions << "               pRmin2: "<<(solid_pRmin2/10)<<" pRmax2: "<<(solid_pRmax2/10);
  WriteToLog(dimensions.str(),where_to_write);
  dimensions.str("");
  dimensions << "               pSPhi: "<<(solid_pSPhi)*convert_radians_degrees<<" pDPhi: "<<(solid_pDPhi)*convert_radians_degrees<<" pEPhi: "<<(solid_pSPhi+solid_pDPhi)*convert_radians_degrees;
  WriteToLog(dimensions.str(),where_to_write);
  dimensions.str("");

  elements_cons[element_counter] = new G4Cons(solid_name+"_solid",solid_pRmin1,solid_pRmax1,solid_pRmin2,solid_pRmax2,solid_pDz,solid_pSPhi,solid_pDPhi);

  pointer.str("");
  pointer << elements_cons[element_counter];
  WriteToLog("Pointer after creation of solid = "+pointer.str(),where_to_write);
  WriteToLog("++++++++++++++++++++Finished creating solid++++++++++++++++++++\n",where_to_write);
  return elements_cons[element_counter];
}

G4LogicalVolume* DetectorConstruction::CreateLogicalVolume(G4Tubs* pSolid,G4Material* pMaterial,G4String logicalvolume_name)
{
  std::stringstream pointer;
  WriteToLog("\t++++++++++++++++++++Started creating logical volume++++++++++++++++++++",where_to_write);
  WriteToLog("\tType = G4LogicalVolume for G4Tubs",where_to_write);
  elements_logical_names[element_counter]=logicalvolume_name+"_logical";
  WriteToLog("\tName = "+elements_logical_names[element_counter],where_to_write);
  WriteToLog("\tMaterial = "+pMaterial->GetName(),where_to_write);
  pointer << elements_logical[element_counter];
  WriteToLog("\tPointer before creation of logical volume = "+pointer.str(),where_to_write);
  elements_logical[element_counter] = new G4LogicalVolume(pSolid,pMaterial,elements_logical_names[element_counter],0,0,0);
  pointer.str("");
  pointer << elements_logical[element_counter];

 
   if (element_counter!=-5 & element_counter!=-5)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes();
       logical_vis_attributes->SetVisibility(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     }

   if (elements_logical_names[element_counter].find("zdivision2")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(0.0,1.0,0.0)); //Green
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };


   if (elements_logical_names[element_counter].find("absorber")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(0.0,1.0,0.0)); //Green
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("kapton")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,0.0,1.0)); //Purple?
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("glue")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,0.0,0.0)); //Red
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("iron")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,1.0,1.0)); //White
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("copper")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(0.0,1.0,1.0)); //Azur
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

  WriteToLog("\tPointer after creation of logical volume = "+pointer.str(),where_to_write);
  WriteToLog("\t++++++++++++++++++++Finished creating logical volume++++++++++++++++++++\n",where_to_write);
  return elements_logical[element_counter];
}

G4LogicalVolume* DetectorConstruction::CreateLogicalVolume(G4Cons* pSolid,G4Material* pMaterial,G4String logicalvolume_name)
{
  std::stringstream pointer;
  WriteToLog("\t++++++++++++++++++++Started creating logical volume++++++++++++++++++++",where_to_write);
  WriteToLog("\tType = G4LogicalVolume for G4Cons",where_to_write);
  elements_logical_names[element_counter]=logicalvolume_name+"_logical";
  WriteToLog("\tName = "+elements_logical_names[element_counter],where_to_write);
  WriteToLog("\tMaterial = "+pMaterial->GetName(),where_to_write);
  pointer << elements_logical[element_counter];
  WriteToLog("\tPointer before creation of logical volume = "+pointer.str(),where_to_write);
  elements_logical[element_counter] = new G4LogicalVolume(pSolid,pMaterial,elements_logical_names[element_counter],0,0,0);
  pointer.str("");
  pointer << elements_logical[element_counter];

   if (element_counter!=-5 & element_counter!=-5)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes();
       logical_vis_attributes->SetVisibility(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("wheel")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,1.0,1.0)); //White
       logical_vis_attributes->SetVisibility(true);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("zdivision")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,1.0,1.0)); //White
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("absorber")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(0.0,1.0,0.0)); //Green
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("iron_fold")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,0.0,0.0)); //Red
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (element_counter==219)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,0.0,1.0)); //Purple?
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (element_counter==207)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(0.0,1.0,0.0)); //Green
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("glue")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,0.0,0.0)); //Red
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("iron")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,1.0,1.0)); //White
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("copper")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(0.0,1.0,1.0)); //Azur
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

  WriteToLog("\tPointer after creation of logical volume = "+pointer.str(),where_to_write);
  WriteToLog("\t++++++++++++++++++++Finished creating logical volume++++++++++++++++++++\n",where_to_write);
  return elements_logical[element_counter];
}

G4LogicalVolume* DetectorConstruction::CreateLogicalVolume(G4TwistedTrap* pSolid,G4Material* pMaterial,G4String logicalvolume_name)
{
  std::stringstream pointer;
  WriteToLog("\t++++++++++++++++++++Started creating logical volume++++++++++++++++++++",where_to_write);
  WriteToLog("\tType = G4LogicalVolume for G4TwistedTrap",where_to_write);
  elements_logical_names[element_counter]=logicalvolume_name+"_logical";
  WriteToLog("\tName = "+elements_logical_names[element_counter],where_to_write);
  WriteToLog("\tMaterial = "+pMaterial->GetName(),where_to_write);
  pointer << elements_logical[element_counter];
  WriteToLog("\tPointer before creation of logical volume = "+pointer.str(),where_to_write);
  elements_logical[element_counter] = new G4LogicalVolume(pSolid,pMaterial,elements_logical_names[element_counter],0,0,0);
  pointer.str("");
  pointer << elements_logical[element_counter];
   if (element_counter!=-5 & element_counter!=-5)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes();
       logical_vis_attributes->SetVisibility(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("absorber")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(0.0,0.0,1.0)); //Blue
       logical_vis_attributes->SetVisibility(true);
       logical_vis_attributes->SetForceSolid(true);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("kapton")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,0.0,1.0)); //Purple?
       logical_vis_attributes->SetVisibility(true);
       logical_vis_attributes->SetForceSolid(true);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };


   if (element_counter==46)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,0.0,1.0)); //Purple?
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("glue")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,0.0,0.0)); //Red
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("iron")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,1.0,1.0)); //White
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("copper")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(0.0,1.0,1.0)); //Azur
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

  //  G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(0.0,0.0,1.0)); //Blue
  //  elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
  //  logical_vis_attributes->SetForceSolid(true);

  WriteToLog("\tPointer after creation of logical volume = "+pointer.str(),where_to_write);
  WriteToLog("\t++++++++++++++++++++Finished creating logical volume++++++++++++++++++++\n",where_to_write);
  return elements_logical[element_counter];
}

G4LogicalVolume* DetectorConstruction::CreateLogicalVolume(G4Trap* pSolid,G4Material* pMaterial,G4String logicalvolume_name)
{
  std::stringstream pointer;
  WriteToLog("\t++++++++++++++++++++Started creating logical volume++++++++++++++++++++",where_to_write);
  WriteToLog("\tType = G4LogicalVolume for G4Trap",where_to_write);
  elements_logical_names[element_counter]=logicalvolume_name+"_logical";
  WriteToLog("\tName = "+elements_logical_names[element_counter],where_to_write);
  WriteToLog("\tMaterial = "+pMaterial->GetName(),where_to_write);
  pointer << elements_logical[element_counter];
  WriteToLog("\tPointer before creation of logical volume = "+pointer.str(),where_to_write);
  elements_logical[element_counter] = new G4LogicalVolume(pSolid,pMaterial,elements_logical_names[element_counter],0,0,0);
  pointer.str("");
  pointer << elements_logical[element_counter];

  if (element_counter!=-5 & element_counter!=-5)
      {
        G4VisAttributes * logical_vis_attributes = new G4VisAttributes();
	logical_vis_attributes->SetVisibility(false);
        elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
      };

   if (elements_logical_names[element_counter].find("absorber_flatpart_2")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(0.0,1.0,0.0)); //Green
       logical_vis_attributes->SetVisibility(true);
       logical_vis_attributes->SetForceSolid(true);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("kapton")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,0.0,1.0)); //Purple?
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("glue")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,0.0,0.0)); //Red
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("iron")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,1.0,0.0)); //Yellow?
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

   if (elements_logical_names[element_counter].find("copper")!=G4String::npos)
     {
       G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(0.0,1.0,1.0)); //Azur
       logical_vis_attributes->SetVisibility(false);
       logical_vis_attributes->SetForceSolid(false);
       elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
     };

//    if (element_counter==730)
//      {
//        G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,0.0,0.0)); //Red
//        elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
//        logical_vis_attributes->SetVisibility(true);
//        logical_vis_attributes->SetForceSolid(true);
//      }
   
   G4VisAttributes * logical_vis_attributes = new G4VisAttributes();
   logical_vis_attributes->SetVisibility(false);

  WriteToLog("\tPointer after creation of logical volume = "+pointer.str(),where_to_write);
  WriteToLog("\t++++++++++++++++++++Finished creating logical volume++++++++++++++++++++\n",where_to_write);
  return elements_logical[element_counter];
}

G4VPhysicalVolume* DetectorConstruction::CreatePhysicalVolume(G4RotationMatrix* pRotationMatrix,G4ThreeVector physicalvolume_translation,G4LogicalVolume* pLogicalVolume,G4String physicalvolume_name,G4LogicalVolume* pParentLogical)
{
  G4bool reflected = false;
  G4bool valid_rotation = true;
  std::stringstream pointer;
  std::stringstream translation;
  std::stringstream rotation;
  std::stringstream matrixelements;
  WriteToLog("\t\t++++++++++++++++++++Started creating physical volume++++++++++++++++++++",where_to_write);
  WriteToLog("\t\tType = G4PVPlacement",where_to_write);
  elements_physical_names[element_counter]=physicalvolume_name+"_physical";
  WriteToLog("\t\tName = "+elements_physical_names[element_counter],where_to_write);
  translation << physicalvolume_translation;
  WriteToLog("\t\tTranslation = "+translation.str(),where_to_write);
  pointer.str("");
  pointer<<pRotationMatrix;
  WriteToLog("\t\tRotationMatrix = "+pointer.str(),where_to_write);
  WriteToLog(rotation.str(),where_to_write);
  if (pRotationMatrix==0) 
    {
      WriteToLog("\t\tRotation = False",where_to_write);
    }
  else
    {
      pRotationMatrix->invert();
    };

  WriteToLog("\t\tName = "+elements_physical_names[element_counter],where_to_write);
  WriteToLog("\t\tParental logical volume is: "+GetLogicalVolumeName(pParentLogical),where_to_write);
  pointer.str("");
  pointer << elements_physical[element_counter];

  if (CheckAxes(elements_rotation[element_counter])==2)
    {

      if (elements_logical_names[element_counter].find("absorber_flatpart")!=G4String::npos)
	{
	  StandardizeMatrix(elements_rotation[element_counter]);
	};

      if (elements_logical_names[element_counter].find("kapton_plane")!=G4String::npos)
	{
	  StandardizeMatrix(elements_rotation[element_counter]);
	};

      if (elements_logical_names[element_counter].find("kapton_another_part")!=G4String::npos)
	{
	  StandardizeMatrix(elements_rotation[element_counter]);
	};

      if (elements_logical_names[element_counter].find("zdivision1")!=G4String::npos)
	{
	  const G4Transform3D* transform3d = new G4Transform3D((*elements_rotation[element_counter]),elements_translation[element_counter]);
	  G4Scale3D scale;
	  G4Rotate3D rotate;
	  G4Translate3D translate;
	  transform3d->getDecomposition(scale,rotate,translate);
	  if (IsReflection(scale))
	    {
	      reflection_volumes.push_back(element_counter);
	      reflection_volumes_parents_logical.push_back(pParentLogical);
	      reflected=true;
	      //This volume needs to be reflected
	    };
	  //In the event that a reflection is intended, then use the G4Transform3D class, to construct a reflection and a translation.
	  
	};

      if (elements_logical_names[element_counter].find("zdivision2")!=G4String::npos)
	{
	  const G4Transform3D* transform3d = new G4Transform3D((*elements_rotation[element_counter]),elements_translation[element_counter]);
	  G4Scale3D scale;
	  G4Rotate3D rotate;
	  G4Translate3D translate;
	  transform3d->getDecomposition(scale,rotate,translate);
	  if (IsReflection(scale))
	    {
	      reflection_volumes.push_back(element_counter);
	      reflection_volumes_parents_logical.push_back(pParentLogical);
	      reflected=true;
	      //This volume needs to be reflected
	    };
	  //In the event that a reflection is intended, then use the G4Transform3D class, to construct a reflection and a translation.
	  
	};

      if (elements_logical_names[element_counter].find("zdivision4")!=G4String::npos)
	{
	  const G4Transform3D* transform3d = new G4Transform3D((*elements_rotation[element_counter]),elements_translation[element_counter]);
	  G4Scale3D scale;
	  G4Rotate3D rotate;
	  G4Translate3D translate;
	  transform3d->getDecomposition(scale,rotate,translate);
	  if (IsReflection(scale))
	    {
	      reflection_volumes.push_back(element_counter);
	      reflection_volumes_parents_logical.push_back(pParentLogical);
	      reflected=true;
	      //G4cout<<"Needs reflecting: "<<G4endl;
	      //This volume needs to be reflected
	    };
	  //In the event that a reflection is intended, then use the G4Transform3D class, to construct a reflection and a translation.
	};

      if (elements_logical_names[element_counter].find("zdivision3")!=G4String::npos)
	{
	  const G4Transform3D* transform3d = new G4Transform3D((*elements_rotation[element_counter]),elements_translation[element_counter]);
	  G4Scale3D scale;
	  G4Rotate3D rotate;
	  G4Translate3D translate;
	  transform3d->getDecomposition(scale,rotate,translate);
	  if (IsReflection(scale))
	    {
	      reflection_volumes.push_back(element_counter);
	      reflection_volumes_parents_logical.push_back(pParentLogical);
	      reflected=true;
	      //G4cout<<"Needs reflecting: "<<G4endl;
	      //This volume needs to be reflected
	    };
	  //In the event that a reflection is intended, then use the G4Transform3D class, to construct a reflection and a translation.
	  
	};
      
      if (elements_logical_names[element_counter].find("absorber_fold")!=G4String::npos)
	{
	  StandardizeMatrix(elements_rotation[element_counter],"x");
	};

      if (elements_logical_names[element_counter].find("kapton_fold")!=G4String::npos)
	{
	  StandardizeMatrix(elements_rotation[element_counter],"x");
	};

      if (elements_logical_names[element_counter].find("kapton_triangle")!=G4String::npos)
	{
	  StandardizeMatrix(elements_rotation[element_counter],"x");
	};

      if (elements_logical_names[element_counter].find("iron_triangle")!=G4String::npos)
	{
	  StandardizeMatrix(elements_rotation[element_counter],"x");
	};

    }
  else if (CheckAxes(elements_rotation[element_counter])==0)
    {
      valid_rotation=false;
    };

  WriteToLog("\t\tPointer before creation of physical volume = "+pointer.str(),where_to_write);
  if (valid_rotation==false)
    {
      WriteToLog("\t\tThe rotation specified is not legal, and cannot be fixed. Not building physical volume.",where_to_write);
    }
  else
    {
      if (reflected==true)
	{
	  WriteToLog("\t\tThis volume will be reflected",where_to_write);
	}
      else
	{
	  //elements_physical[element_counter] = new G4PVPlacement(pRotationMatrix,physicalvolume_translation,pLogicalVolume,physicalvolume_name+"physical",pParentLogical,false,0);
	  elements_physical[element_counter] = new G4PVPlacement(pRotationMatrix,physicalvolume_translation,pLogicalVolume,physicalvolume_name+"physical",pParentLogical,false,element_counter);
	};
    };

  if (elements_physical[element_counter]!=0)
    {      
      PrintTransformMatrices(elements_physical[element_counter]->GetTranslation(),elements_physical[element_counter]->GetRotation());
      CheckAxes2(elements_physical[element_counter]->GetRotation());
    };
  pointer.str("");
  pointer << elements_physical[element_counter];
  WriteToLog("\t\tPointer after creation of physical volume = "+pointer.str(),where_to_write);

  if (GetLogicalVolumeName(pParentLogical).find("zdivision6")!=G4String::npos)// || GetLogicalVolumeName(pParentLogical).find("zdivision4")!=G4String::npos)// || GetLogicalVolumeName(pParentLogical).find("zdivision2")!=G4String::npos || GetLogicalVolumeName(pParentLogical).find("zdivision1")!=G4String::npos)
    {
      G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(0.0,0.0,1.0)); //Blue
      logical_vis_attributes->SetVisibility(true);
      logical_vis_attributes->SetForceSolid(true);
      elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
      
      if (elements_logical_names[element_counter].find("absorber")!=G4String::npos)
	{
	  G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(0.0,1.0,0.0)); //Green
	  logical_vis_attributes->SetVisibility(true);
	  logical_vis_attributes->SetForceSolid(true);
	  elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
	};
      
      if (elements_logical_names[element_counter].find("kapton")!=G4String::npos)
	{
	  G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,0.0,1.0)); //Purple?
	  logical_vis_attributes->SetVisibility(true);
	  logical_vis_attributes->SetForceSolid(true);
	  elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
	};
      
      if (elements_logical_names[element_counter].find("glue")!=G4String::npos)
	{
	  G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,0.0,0.0)); //Red
	  logical_vis_attributes->SetVisibility(true);
	  logical_vis_attributes->SetForceSolid(true);
	  elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
	};
      
      if (elements_logical_names[element_counter].find("iron")!=G4String::npos)
	{
	  G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(1.0,1.0,1.0)); //White
	  logical_vis_attributes->SetVisibility(true);
	  logical_vis_attributes->SetForceSolid(true);
	  elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
	};
      
      if (elements_logical_names[element_counter].find("copper")!=G4String::npos)
	{
	  G4VisAttributes * logical_vis_attributes = new G4VisAttributes(G4Colour(0.0,1.0,1.0)); //Azur
	  logical_vis_attributes->SetVisibility(true);
	  logical_vis_attributes->SetForceSolid(true);
	  elements_logical[element_counter]->SetVisAttributes(logical_vis_attributes);
	};
      
    };
  

  WriteToLog("\t\t++++++++++++++++++++Finished creating physical volume++++++++++++++++++++\n",where_to_write);
  //  if (elements_logical_names[element_counter].find("absorber")!=G4String::npos)
  //    {
  //      elements_logical[element_counter]->SetSensitiveDetector(absorbers_sensitive);
  //    };
  
  elements_logical[element_counter]->SetSmartless(1.7);

//    if (GetLogicalVolumeName(elements_logical[element_counter]).find("emec_endcap")!=G4String::npos)
//      {
//        elements_logical[element_counter]->SetSmartless(1000);
  G4cout<<"Smartless in "<<element_counter<<"is "<<elements_logical[element_counter]->GetSmartless()<<G4endl;
//      };
  
   G4cout<<"Optimisation for this volume? with number: "<<element_counter<<" is: "<<elements_logical[element_counter]->IsToOptimise()<<G4endl;
   G4cout<<"Physics region defined for with number: "<<element_counter<<" is: "<<elements_logical[element_counter]->IsRegion()<<G4endl;


  return elements_physical[element_counter];
}

G4VPhysicalVolume* DetectorConstruction::CreatePhysicalVolumeDivision(G4String physicalvolume_name,G4LogicalVolume* pLogicalVolume,G4String parent_logical,EAxis physicalvolume_divisionsaxis,G4int physicalvolume_divisionsnumber)
{
  std::stringstream pointer;
  WriteToLog("\t\t++++++++++++++++++++++++Started creating physical volume++++++++++++++++++++++++",where_to_write);
  WriteToLog("\t\tType = G4PVDivision",where_to_write);
  elements_physical_names[element_counter]=physicalvolume_name+"_physical";
  WriteToLog("\t\tName = "+elements_physical_names[element_counter],where_to_write);
  pointer << physicalvolume_divisionsaxis;
  WriteToLog("\t\tAxis of division = "+pointer.str(),where_to_write);
  pointer.str("");
  pointer << physicalvolume_divisionsnumber;
  WriteToLog("\t\tNumber of divisions = "+pointer.str(),where_to_write);
  pointer.str("");
  pointer << elements_physical[element_counter];
  WriteToLog("\t\tPointer before creation of physical volume = "+pointer.str(),where_to_write);
  //  elements_physical[element_counter] = new G4PVDivision(elements_physical_names[element_counter],pLogicalVolume,GetLogicalVolume(parent_logical),physicalvolume_divisionsaxis,physicalvolume_divisionsnumber,0);
  elements_physical[element_counter] = new G4PVReplica(elements_physical_names[element_counter],pLogicalVolume,GetLogicalVolume(parent_logical),physicalvolume_divisionsaxis,physicalvolume_divisionsnumber,(360*deg)/physicalvolume_divisionsnumber);

  pointer.str("");
  pointer << elements_physical[element_counter];
  WriteToLog("\t\tPointer after creation of physical volume = "+pointer.str(),where_to_write);
  WriteToLog("\t\t++++++++++++++++++++++++Finished creating physical volume++++++++++++++++++++++++\n",where_to_write);
  //  elements_logical[element_counter]->SetSensitiveDetector(absorbers_sensitive);
  return elements_physical[element_counter];
}

// G4PhysicalVolumesPair* DetectorConstruction::CreatePhysicalVolumesPair(G4RotationMatrix* pRotationMatrix,G4ThreeVector physicalvolume_translation,G4LogicalVolume* pLogicalVolume,G4String physicalvolume_name,G4LogicalVolume* pParentLogical)
// {
//   In the event that a reflection is intended, then use the G4Transform3D class, to construct a reflect\ion and a translation.
//   const G4RotationMatrix* rotation=pRotationMatrix;
//   const G4ThreeVector translation=physicalcolume_translation;
//   const G4String name=physicalvolume_name+"physical_pair";
//   const G4Transform3D* transform3d = new G4Transform3D((*rotation),translation);
//   G4PhysicalVolumesPair PVPair=G4ReflectionFactory::Instance()->Place((*transform3d),name,pLogicalVolume,pParentLogical,false,1,false);

//   std::stringstream pointer;
//   std::stringstream translation;
//   std::stringstream rotation;
//   std::stringstream matrixelements;
//   WriteToLog("\t\t++++++++++++++++++++Started creating physical volume++++++++++++++++++++",where_to_write);
//   WriteToLog("\t\tType = G4PVPlacement",where_to_write);
//   elements_physical_names[element_counter]=physicalvolume_name+"_physical";
//   WriteToLog("\t\tName = "+elements_physical_names[element_counter],where_to_write);
//   translation << physicalvolume_translation;
//   WriteToLog("\t\tTranslation = "+translation.str(),where_to_write);
//   pointer.str("");
//   pointer<<pRotationMatrix;
//   WriteToLog("\t\tRotationMatrix = "+pointer.str(),where_to_write);
//   WriteToLog(rotation.str(),1);
 
//   WriteToLog("\t\tName = "+elements_physical_names[element_counter],where_to_write);
//   WriteToLog("\t\tParental logical volume is: "+GetLogicalVolumeName(pParentLogical),where_to_write);
//   pointer.str("");
//   pointer << elements_physical[element_counter];
//   WriteToLog("\t\tPointer before creation of physical volume = "+pointer.str(),where_to_write);
//   elements_physical[element_counter] = new G4PVPlacement(pRotationMatrix,physicalvolume_translation,pLogicalVolume,physicalvolume_name+"physical",pParentLogical,false,0);
//   PrintTransformMatrices(elements_physical[element_counter]->GetTranslation(),elements_physical[element_counter]->GetRotation());
//   CheckAxes2(elements_physical[element_counter]->GetRotation());
//   pointer.str("");
//   pointer << elements_physical[element_counter];
//   WriteToLog("\t\tPointer after creation of physical volume = "+pointer.str(),where_to_write);



//   WriteToLog("\t\t++++++++++++++++++++Finished creating physical volume++++++++++++++++++++\n",where_to_write);
//   if (elements_logical_names[element_counter].find("absorber")!=G4String::npos)
//     {
//       elements_logical[element_counter]->SetSensitiveDetector(absorbers_sensitive);
//     };
//   return elements_physical[element_counter];
// }

void DetectorConstruction::CreateEMEndcap(G4String parent_logical)
{
  //Parameters
  G4String endcaps_namestub="endcaps";
  G4double endcaps_pRmin1=calorimeter_internal_radius;
  G4double endcaps_pRmax1=calorimeter_external_radius;
  G4double endcaps_pRmin2=(calorimeter_internal_radius*(1+(calorimeter_length/emec_z_origin)));
  G4double endcaps_pRmax2=calorimeter_external_radius;
  G4double endcaps_pDz=(calorimeter_length/2);
  G4double endcaps_pSPhi=0*deg;
  G4double endcaps_pDPhi=360*deg;
  
  //Solid
 CreateSolid(endcaps_namestub,endcaps_pRmin1,endcaps_pRmax1,endcaps_pRmin2,endcaps_pRmax2,endcaps_pDz,endcaps_pSPhi,endcaps_pDPhi);

 elements_cons[element_counter+1]=elements_cons[element_counter]; //In view of the two physical volumes to be created
 elements_solid_names[element_counter+1]=elements_solid_names[element_counter]; //In view of the two physical volumes to be created

  //Logical Volume
  CreateLogicalVolume(elements_cons[element_counter],Air,endcaps_namestub);  

  elements_logical[element_counter+1]=elements_logical[element_counter]; //In view of the two physical volumes to be created
  elements_logical_names[element_counter+1]=elements_logical_names[element_counter]; //In view of the two physical volumes to be created
  //Physical volume

  elements_rotation[element_counter+1] = new G4RotationMatrix; //For one end of the detector only.

  for (int i=0;i<2;i++) //Create two copies of the endcaps volume at each end of the detector, rotated by 180*deg in X.
    { 
      if (i==0)
	{
	  elements_translation[element_counter].set(0*cm,0*cm,(emec_z_origin+calorimeter_length/2+barrel_endcap_gap));
	  CreatePhysicalVolume(elements_rotation[element_counter],elements_translation[element_counter],elements_logical[element_counter],endcaps_namestub,GetLogicalVolume(parent_logical));
	  element_counter=element_counter+1;
       	}
      else if (i==1)
      	{
	  elements_translation[element_counter].set(0*cm,0*cm,(-emec_z_origin-calorimeter_length/2-barrel_endcap_gap));
	  elements_rotation[element_counter]->rotateX(180*deg);
	  CreatePhysicalVolume(elements_rotation[element_counter],elements_translation[element_counter],elements_logical[element_counter],endcaps_namestub,GetLogicalVolume(parent_logical));
	  element_counter=element_counter+1;
      	};
    };
}

void DetectorConstruction::CreateSupportingDisc(G4String parent_logical)
{
  //Parameters
  G4String supporting_disc_namestub="supporting_disc";
  G4double supporting_disc_pRmin=(calorimeter_internal_radius*(lar_nominal_origin+absorber_startingpart_length)/emec_z_origin);
  G4double supporting_disc_pRmax=active_maximumradius;
  G4double supporting_disc_pDz=(supportingdisc_thickness/2);
  G4double supporting_disc_pSPhi=0*deg;
  G4double supporting_disc_pDPhi=360*deg;
  elements_translation[element_counter].set(0*cm,0*cm,(wheel_reference_point-(emec_z_origin+(calorimeter_length/2))-supportingdisc_thickness+(supportingdisc_thickness/2)));  

  //Solid
  CreateSolid(supporting_disc_namestub,supporting_disc_pRmin,supporting_disc_pRmax,supporting_disc_pDz,supporting_disc_pSPhi,supporting_disc_pDPhi);

  //Logical Volume
  CreateLogicalVolume(elements_tubs[element_counter],FibreGlass,supporting_disc_namestub);

  //Physical volume
  CreatePhysicalVolume(elements_rotation[element_counter],elements_translation[element_counter],elements_logical[element_counter],supporting_disc_namestub,GetLogicalVolume(parent_logical));

  element_counter=element_counter+1;
}

void DetectorConstruction::CreateExternalSupportingDisc(G4String parent_logical)
{
  //Parameters
  G4String external_supporting_disc_namestub="external_supporting_disc";
  G4double external_supporting_disc_pRmin=196.1*cm;
  G4double external_supporting_disc_pRmax=active_maximumradius;
  G4double external_supporting_disc_pDz=(externalsupportingdisc_thickness/2);
  G4double external_supporting_disc_pSPhi=0*deg;
  G4double external_supporting_disc_pDPhi=360*deg;

  elements_translation[element_counter].set(0*cm,0*cm,(wheel_reference_point-(emec_z_origin+(calorimeter_length/2))-supportingdisc_thickness-(externalsupportingdisc_thickness/2)));

  //Solid
  CreateSolid(external_supporting_disc_namestub,external_supporting_disc_pRmin,external_supporting_disc_pRmax,external_supporting_disc_pDz,external_supporting_disc_pSPhi,external_supporting_disc_pDPhi);

  //Logical Volume
  CreateLogicalVolume(elements_tubs[element_counter],Al,external_supporting_disc_namestub); 

  //Physical volume
  CreatePhysicalVolume(elements_rotation[element_counter],elements_translation[element_counter],elements_logical[element_counter],external_supporting_disc_namestub,GetLogicalVolume(parent_logical));
  element_counter=element_counter+1;
}

void DetectorConstruction::CreateMediumSupportingDisc(G4String parent_logical)
{
  //Parameters
  G4String medium_supporting_disc_namestub="medium_supporting_disc";
  G4double medium_supporting_disc_pRmin=55.5*cm;
  G4double medium_supporting_disc_pRmax=66.9*cm;
  G4double medium_supporting_disc_pDz=(mediumsupportingdisc_thickness/2);
  G4double medium_supporting_disc_pSPhi=0*deg;
  G4double medium_supporting_disc_pDPhi=360*deg;
  elements_translation[element_counter].set(0*cm,0*cm,(wheel_reference_point-(emec_z_origin+(calorimeter_length/2))-supportingdisc_thickness-(mediumsupportingdisc_thickness/2)));

  //Solid
  CreateSolid(medium_supporting_disc_namestub,medium_supporting_disc_pRmin,medium_supporting_disc_pRmax,medium_supporting_disc_pDz,medium_supporting_disc_pSPhi,medium_supporting_disc_pDPhi);

  //Logical Volume
 CreateLogicalVolume(elements_tubs[element_counter],FibreGlass,medium_supporting_disc_namestub);

  //Physical volume
 CreatePhysicalVolume(elements_rotation[element_counter],elements_translation[element_counter],elements_logical[element_counter],medium_supporting_disc_namestub,GetLogicalVolume(parent_logical));
 element_counter=element_counter+1;
}

void DetectorConstruction::CreateInternalSupportingDisc(G4String parent_logical)
{
  //Parameters
  G4String internal_supporting_disc_namestub="internal_supporting_disc";
  G4double internal_supporting_disc_pRmin=(calorimeter_internal_radius*(lar_nominal_origin/emec_z_origin));
  G4double internal_supporting_disc_pRmax=(calorimeter_internal_radius+10.1*cm);
  G4double internal_supporting_disc_pDz=(internalsupportingdisc_thickness/2);
  G4double internal_supporting_disc_pSPhi=0*deg;
  G4double internal_supporting_disc_pDPhi=360*deg;
  elements_translation[element_counter].set(0*cm,0*cm,(wheel_reference_point-(emec_z_origin+(calorimeter_length/2))-supportingdisc_thickness-(internalsupportingdisc_thickness/2)));

  //Solid
  CreateSolid(internal_supporting_disc_namestub,internal_supporting_disc_pRmin,internal_supporting_disc_pRmax,internal_supporting_disc_pDz,internal_supporting_disc_pSPhi,internal_supporting_disc_pDPhi);

  //Logical Volume
  CreateLogicalVolume(elements_tubs[element_counter],Al,internal_supporting_disc_namestub);
  
  //Physical volume
  CreatePhysicalVolume(elements_rotation[element_counter],elements_translation[element_counter],elements_logical[element_counter],internal_supporting_disc_namestub,GetLogicalVolume(parent_logical));
  element_counter=element_counter+1;
}


void DetectorConstruction::CreateWheels(G4String parent_logical)
{
  for (int j=0;j<2;j++) //inner and outer wheels
    {
      stac=(lar_totalthickness-2*absorber_startingpart_length)/wheel_parameters_absorbers_numwaves[j];    
      wheel_internal_radius_1=lar_nominal_origin*tan(wheel_parameters_theta_int[j]);
      wheel_internal_radius_2=z2*tan(wheel_parameters_theta_int[j]);
      wheel_external_radius_1=lar_nominal_origin*tan(wheel_parameters_theta_ext[j]);
      wheel_external_radius_2=z2*tan(wheel_parameters_theta_ext[j]);
      sroue1=stac/2-scrb1;
      sroue=sroue1-0.002*cm;

      if (j==0) //Inner wheel
	{
	  wheel_external_radius_1=wheel_external_radius_1-wheel_betweenwheels_semigap;
	  wheel_external_radius_2=wheel_external_radius_2-wheel_betweenwheels_semigap;

	  //Parameters
	  G4String inner_wheel_namestub="inner_wheel";
	  G4double inner_wheel_pRmin1=wheel_internal_radius_1;
	  G4double inner_wheel_pRmax1=wheel_external_radius_1;
	  G4double inner_wheel_pRmin2=wheel_internal_radius_2;
	  G4double inner_wheel_pRmax2=wheel_external_radius_2;
	  G4double inner_wheel_pDz=(lar_totalthickness/2);
	  G4double inner_wheel_pSPhi=0*deg;
	  G4double inner_wheel_pDPhi=360*deg;
	  elements_translation[element_counter].set(0*cm,0*cm,(wheel_reference_point+(lar_totalthickness/2)-emec_z_origin-(calorimeter_length/2)));
	  elements_rotation[element_counter] = new G4RotationMatrix();
	  elements_rotation[element_counter]->rotateZ((((180*deg)/wheel_parameters_absorbers_number[j])*(wheel_parameters_beta[j]-3)));

	  //Solid
	  CreateSolid(inner_wheel_namestub,inner_wheel_pRmin1,inner_wheel_pRmax1,inner_wheel_pRmin2,inner_wheel_pRmax2,inner_wheel_pDz,inner_wheel_pSPhi,inner_wheel_pDPhi);

	  //Logical volume
	  CreateLogicalVolume(elements_cons[element_counter],lAr,inner_wheel_namestub);

	  //Physical Volume
	  CreatePhysicalVolume(elements_rotation[element_counter],elements_translation[element_counter],elements_logical[element_counter],inner_wheel_namestub,GetLogicalVolume(parent_logical));
	  element_counter=element_counter+1;

	  //Inner wheel divisions
	  //Parameters
	  G4String inner_wheel_division_namestub="inner_wheel_division";
	  G4double inner_wheel_division_pRmin1=wheel_internal_radius_1;
	  G4double inner_wheel_division_pRmax1=wheel_external_radius_1-wheel_betweenwheels_semigap;
	  G4double inner_wheel_division_pRmin2=wheel_internal_radius_2;
	  G4double inner_wheel_division_pRmax2=wheel_external_radius_2-wheel_betweenwheels_semigap;
	  G4double inner_wheel_division_pDz=(lar_totalthickness/2);
	  G4double inner_wheel_division_pSPhi=0*deg;
	  //G4double inner_wheel_division_pDPhi=360*deg;
	  G4double inner_wheel_division_pDPhi=(360*deg)/int(wheel_parameters_absorbers_number_temp[j]);

	  //Solid
	  CreateSolid(inner_wheel_division_namestub,inner_wheel_division_pRmin1,inner_wheel_division_pRmax1,inner_wheel_division_pRmin2,inner_wheel_division_pRmax2,inner_wheel_division_pDz,inner_wheel_division_pSPhi,inner_wheel_division_pDPhi);
	  //Logical volume
	  CreateLogicalVolume(elements_cons[element_counter],lAr,inner_wheel_division_namestub);

	  //Physical Volume
	  CreatePhysicalVolumeDivision(inner_wheel_division_namestub,elements_logical[element_counter],"inner_wheel_logical",kPhi,int(wheel_parameters_absorbers_number_temp[j]));
	  element_counter=element_counter+1;
	  G4cout<<"NEED TO REMOVE THE _temp FROM THE ARRAY NAME FOR REAL USE: "<<G4endl;
	  
	  CreateAzimuthalSector("inner_wheel_division_logical",j); //Fill azimuthal section for inner wheel

	}
      else if (j==1) //Outer wheel
	{
	  wheel_internal_radius_1=wheel_internal_radius_1+wheel_betweenwheels_semigap;
	  wheel_internal_radius_2=wheel_internal_radius_2+wheel_betweenwheels_semigap;
	  wheel_external_radius_1=active_maximumradius;
	  wheel_external_radius_2=active_maximumradius;


	  //Parameters
	  G4String outer_wheel_namestub="outer_wheel";
	  G4double outer_wheel_pRmin1=wheel_internal_radius_1;
	  G4double outer_wheel_pRmax1=active_maximumradius;
	  G4double outer_wheel_pRmin2=wheel_internal_radius_2;
	  G4double outer_wheel_pRmax2=active_maximumradius;
	  G4double outer_wheel_pDz=(lar_totalthickness/2);
	  G4double outer_wheel_pSPhi=0*deg;
	  G4double outer_wheel_pDPhi=360*deg;
	  elements_translation[element_counter].set(0*cm,0*cm,(wheel_reference_point+(lar_totalthickness/2)-emec_z_origin-(calorimeter_length/2)));
	  elements_rotation[element_counter] = new G4RotationMatrix();
	  elements_rotation[element_counter]->rotateZ(((180/wheel_parameters_absorbers_number[j])*(wheel_parameters_beta[j]-3))*deg);

	  //Solid
	  CreateSolid(outer_wheel_namestub,outer_wheel_pRmin1,outer_wheel_pRmax1,outer_wheel_pRmin2,outer_wheel_pRmax2,outer_wheel_pDz,outer_wheel_pSPhi,outer_wheel_pDPhi);

	  //Logical volume
	  CreateLogicalVolume(elements_cons[element_counter],lAr,outer_wheel_namestub);

	  //Physical volume
	  CreatePhysicalVolume(elements_rotation[element_counter],elements_translation[element_counter],elements_logical[element_counter],outer_wheel_namestub,GetLogicalVolume(parent_logical));
	  element_counter=element_counter+1;
	  //Outer wheel divisions
	  //Parameters
	  G4String outer_wheel_division_namestub="outer_wheel_division";
	  G4double outer_wheel_division_pRmin1=wheel_internal_radius_1;
	  G4double outer_wheel_division_pRmax1=wheel_external_radius_1-wheel_betweenwheels_semigap;
	  G4double outer_wheel_division_pRmin2=wheel_internal_radius_2;
	  G4double outer_wheel_division_pRmax2=wheel_external_radius_2-wheel_betweenwheels_semigap;
	  G4double outer_wheel_division_pDz=(lar_totalthickness/2);
	  G4double outer_wheel_division_pSPhi=0*deg;
	  //G4double outer_wheel_division_pDPhi=200*deg;
          G4double outer_wheel_division_pDPhi=(360*deg)/int(wheel_parameters_absorbers_number_temp[j]);



	  //Solid
	  CreateSolid(outer_wheel_division_namestub,outer_wheel_division_pRmin1,outer_wheel_division_pRmax1,outer_wheel_division_pRmin2,outer_wheel_division_pRmax2,outer_wheel_division_pDz,outer_wheel_division_pSPhi,outer_wheel_division_pDPhi);

	  //Logical volume
	  CreateLogicalVolume(elements_cons[element_counter],lAr,outer_wheel_division_namestub);

	  //Physical volume
	  CreatePhysicalVolumeDivision(outer_wheel_division_namestub,elements_logical[element_counter],"outer_wheel_logical",kPhi,int(wheel_parameters_absorbers_number_temp[j]));
	  element_counter=element_counter+1;
  	  G4cout<<"NEED TO REMOVE THE _temp FROM THE ARRAY NAME FOR REAL USE: "<<G4endl; 

	  CreateAzimuthalSector("outer_wheel_division_logical",j); //Fill azimuthal section for outer wheel
	};
    };
}


void DetectorConstruction::CreateWheelCrack(G4String parent_logical)
{
  for (int k=0;k<1;k++) //Only use inner wheel parameters, use loop for flexibility, i.e. can change to outer if required.
    {
      wheel_external_radius_1=lar_nominal_origin*tan(wheel_parameters_theta_ext[k]);
      wheel_external_radius_2=z2*tan(wheel_parameters_theta_ext[k]);
      wheel_internal_radius_1=wheel_external_radius_1-wheel_betweenwheels_semigap;
      wheel_internal_radius_2=wheel_external_radius_2-wheel_betweenwheels_semigap;
      wheel_external_radius_1=wheel_external_radius_1+wheel_betweenwheels_semigap;
      wheel_external_radius_2=wheel_external_radius_2+wheel_betweenwheels_semigap;

      //Parameters
      G4String wheel_crack_namestub="wheel_crack";
      G4double wheel_crack_pRmin1=wheel_internal_radius_1;
      G4double wheel_crack_pRmax1=wheel_external_radius_1;
      G4double wheel_crack_pRmin2=wheel_internal_radius_2;
      G4double wheel_crack_pRmax2=wheel_external_radius_2;
      G4double wheel_crack_pDz=(lar_totalthickness/2);
      G4double wheel_crack_pSPhi=0*deg;
      G4double wheel_crack_pDPhi=360*deg;
  
      elements_translation[element_counter].set(0*cm,0*cm,(wheel_reference_point+(lar_totalthickness/2)-emec_z_origin-(calorimeter_length/2)));

      //Solid
      CreateSolid(wheel_crack_namestub,wheel_crack_pRmin1,wheel_crack_pRmax1,wheel_crack_pRmin2,wheel_crack_pRmax2,wheel_crack_pDz,wheel_crack_pSPhi,wheel_crack_pDPhi);      

      //Logical Volume
      CreateLogicalVolume(elements_cons[element_counter],FibreGlass,wheel_crack_namestub);
      
      //Physical Volume
      CreatePhysicalVolume(elements_rotation[element_counter],elements_translation[element_counter],elements_logical[element_counter],wheel_crack_namestub,GetLogicalVolume(parent_logical));
      element_counter=element_counter+1;

    };
}




void DetectorConstruction::CreateEMECEndcap(G4String parent_logical)
{
  //Parameters
  G4String emec_endcap_namestub="emec_endcap";
  G4double emec_endcap_pRmin1=calorimeter_internal_radius;
  G4double emec_endcap_pRmax1=calorimeter_external_radius;
  G4double emec_endcap_pRmin2=(calorimeter_internal_radius*(1+(calorimeter_length/emec_z_origin)));
  G4double emec_endcap_pRmax2=calorimeter_external_radius;
  G4double emec_endcap_pDz=(calorimeter_length/2);
  G4double emec_endcap_pSPhi=0*deg;
  G4double emec_endcap_pDPhi=360*deg;

  //Solid
  CreateSolid(emec_endcap_namestub,emec_endcap_pRmin1,emec_endcap_pRmax1,emec_endcap_pRmin2,emec_endcap_pRmax2,emec_endcap_pDz,emec_endcap_pSPhi,emec_endcap_pDPhi);

  //Logical Volume
  CreateLogicalVolume(elements_cons[element_counter],lAr,emec_endcap_namestub);
  //Physical volume
  CreatePhysicalVolume(elements_rotation[element_counter],elements_translation[element_counter],elements_logical[element_counter],emec_endcap_namestub,GetLogicalVolume(parent_logical));
  element_counter=element_counter+1;
}

void DetectorConstruction::DumpSolidAndVolumeDetails()
{
  //Dump the details of the solids and volumes.
  std::stringstream pointer;
  for (unsigned int mycounter=0;mycounter<elements_physical.size();mycounter++)
    {
      pointer.str("");
      if (elements_cons[mycounter]!=0)
	{
	  pointer << elements_cons[mycounter];
	}
      else if (elements_tubs[mycounter]!=0)
	{
	  pointer << elements_tubs[mycounter];
	}
      
      WriteToLog("\nSolid: "+elements_solid_names[mycounter]+" \tpointer: "+pointer.str(),1);
      
      pointer.str("");
      pointer << elements_logical[mycounter];
      
      WriteToLog("\tLogical: "+elements_logical_names[mycounter]+" \tpointer: "+pointer.str(),1);
      
      pointer.str("");
      pointer << elements_physical[mycounter];
      
      WriteToLog("\t\tPhysical: "+elements_physical_names[mycounter]+" \tpointer: "+pointer.str(),1);
      pointer.str("");
      pointer << GetLogicalVolume("mother_logical");
      WriteToLog("Extracting postion of mother, well, rather the pointer: "+pointer.str(),1);
    };
  
}


void DetectorConstruction::CreateMother()
{
  //Parameters
  G4String mother_namestub="mother";
  G4double mother_pRmin=0*cm;
  G4double mother_pRmax=calorimeter_external_radius;
  G4double mother_pDz=(emec_z_origin+calorimeter_length+barrel_endcap_gap);//+100*cm;
  G4double mother_pSPhi=0*deg;
  G4double mother_pDPhi=360*deg;

  //Solid
  CreateSolid(mother_namestub,mother_pRmin,mother_pRmax,mother_pDz,mother_pSPhi,mother_pDPhi);

  //Logical Volume
  CreateLogicalVolume(elements_tubs[element_counter],Air,mother_namestub);

  //Physical volume
  CreatePhysicalVolume(elements_rotation[element_counter],elements_translation[element_counter],elements_logical[element_counter],mother_namestub,0);

  element_counter=element_counter+1;
}



void DetectorConstruction::CreateAzimuthalSector(G4String parent_logical,G4int wheel_number)
{

  //Create eight volumes with calls to create z divisions of accorion structure
  //Translation will be literal for now.

  G4Material* pMaterial=lAr;
  //Element1
  ifwd=1;
  ifold=0;
  zr1=0*cm;
  zr2=absorber_startingpart_length-(supportingbar_container_zsize/4);
  zr1p=zr1;
  zr2m=zr2;
  zr=(zr1+zr2)/2;
  //  G4cout<<"zdivisioncall 1: "<<G4endl;
  //CreateZDivision(parent_logical,pMaterial,wheel_number,(zr-(lar_totalthickness/2)),0,ifwd,ifold,ifront,zr1,zr1p,zr2,zr2m,zr,kr,kc,zcr1,zcr2,zcr,drrmax,drrmin);

  //Element2
  ifwd=1;
  ifold=0;
  zr1=(lar_totalthickness-absorber_startingpart_length+(supportingbar_container_zsize/4));
  zr2=lar_totalthickness;
  zr1p=zr1;
  zr2m=zr2;
  zr=(zr1+zr2)/2;
  //G4cout<<"zdivisioncall 2: "<<G4endl;
  //CreateZDivision(parent_logical,pMaterial,wheel_number,(zr-(lar_totalthickness/2)),0,ifwd,ifold,ifront,zr1,zr1p,zr2,zr2m,zr,kr,kc,zcr1,zcr2,zcr,drrmax,drrmin);

  //Element3
  ifwd=3;
  ifold=0;
  ifront=1;
  zr1p=(absorber_startingpart_length+(supportingbar_container_zsize/4));
  zr2m=(absorber_startingpart_length+(stac/4)-(supportingbar_container_zsize/2));
  zr1=zr1p-(supportingbar_container_zsize/2);
  zr2=zr2m+(supportingbar_container_zsize/2);
  zr=(zr1p+zr2m)/2;
  //G4cout<<"zdivisioncall 3: "<<G4endl;
  //CreateZDivision(parent_logical,pMaterial,wheel_number,(zr-(lar_totalthickness/2)),0,ifwd,ifold,ifront,zr1,zr1p,zr2,zr2m,zr,kr,kc,zcr1,zcr2,zcr,drrmax,drrmin);

  //Element4
  ifwd=3;
  ifold=0;
  ifront=-1;
  zr2m=lar_totalthickness-(absorber_startingpart_length+(supportingbar_container_zsize/4));
  zr1p=lar_totalthickness-(absorber_startingpart_length+(stac/4)-(supportingbar_container_zsize/2));
  zr1=zr1p-(supportingbar_container_zsize/2);
  zr2=zr2m+(supportingbar_container_zsize/2);
  zr=(zr1p+zr2m)/2;
  //G4cout<<"zdivisioncall 4: "<<G4endl;
  //CreateZDivision(parent_logical,pMaterial,wheel_number,(zr-(lar_totalthickness/2)),0,ifwd,ifold,ifront,zr1,zr1p,zr2,zr2m,zr,kr,kc,zcr1,zcr2,zcr,drrmax,drrmin);

  //Element5
  ifwd=2;
  ifold=1;
  ifront=1;
  zcr1=absorber_startingpart_length-(supportingbar_container_zsize/4);
  zcr2=absorber_startingpart_length+(supportingbar_container_zsize/4);
  zcr=zcr1;
  kr=1;
  kc=1;
  drrmax=absorber_startingpart_length-(supportingbar_container_zsize/4);
  drrmin=stac/4+(supportingbar_container_zsize/4);
  
  G4RotationMatrix* element5_rotation=new G4RotationMatrix;

  elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
  rotation_values.clear();
  rotation_values.push_back(element5_rotation->thetaX()); //ThetaX
  rotation_values.push_back(element5_rotation->phiX()); //PhiX
  rotation_values.push_back(element5_rotation->thetaY()); //ThetaY
  rotation_values.push_back((-90*deg)*(2*kc-3)*(2*kr-3)); //PhiY
  rotation_values.push_back((180*deg)*(kc-1)); //ThetaZ
  rotation_values.push_back(element5_rotation->phiZ()); //PhiZ
  // ThetaZ=180*(kc-1)  PhiY=-90*(2*kc-3)*(2*kr-3)
  element5_rotation=0;
  element5_rotation=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
  elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
  //  CreateZDivision(parent_logical,pMaterial,wheel_number,((zcr2+zcr1-lar_totalthickness)/2),element5_rotation,ifwd,ifold,ifront,zr1,zr1p,zr2,zr2m,zr,kr,kc,zcr1,zcr2,zcr,drrmax,drrmin);
  //G4cout<<"zdivisioncall 5: "<<G4endl;
  //CreateZDivision(parent_logical,pMaterial,wheel_number,((zcr2+zcr1-lar_totalthickness)/2),elements_rotation[element_counter],ifwd,ifold,ifront,zr1,zr1p,zr2,zr2m,zr,kr,kc,zcr1,zcr2,zcr,drrmax,drrmin);

  //Element6
  ifwd=2;
  ifold=1;
  ifront=-1;
  zr2m=lar_totalthickness-(absorber_startingpart_length+(supportingbar_container_zsize/4));
  zr1p=lar_totalthickness-(absorber_startingpart_length+(stac/4)-(supportingbar_container_zsize/2));
  zr1=zr1p-(supportingbar_container_zsize/2);
  zr2=zr2m+(supportingbar_container_zsize/2);
  zr=(zr1p+zr2m)/2;
  zcr1=lar_totalthickness-(absorber_startingpart_length-(supportingbar_container_zsize/4));
  zcr2=lar_totalthickness-(absorber_startingpart_length+(supportingbar_container_zsize/4));
  zcr=zcr1;
  kr=1;
  kc=2;
  drrmin=absorber_startingpart_length-(supportingbar_container_zsize/4);
  drrmax=(stac/4)+(supportingbar_container_zsize/4);
  G4RotationMatrix* element6_rotation=new G4RotationMatrix;

  elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
  rotation_values.clear();
  rotation_values.push_back(element6_rotation->thetaX()); //ThetaX
  rotation_values.push_back(element6_rotation->phiX()); //PhiX
  rotation_values.push_back(element6_rotation->thetaY()); //ThetaY
  rotation_values.push_back((-90*deg)*(2*kc-3)*(2*kr-3)); //PhiY
  rotation_values.push_back((180*deg)*(kc-1)); //ThetaZ
  rotation_values.push_back(element6_rotation->phiZ()); //PhiZ
  //ThetaZ=180*(kc-1)  PhiY=-90*(2*kc-3)*(2*kr-3)
  element6_rotation=0;
  element6_rotation=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
  elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
  //  CreateZDivision(parent_logical,pMaterial,wheel_number,((zcr2+zcr1-lar_totalthickness)/2),element6_rotation,ifwd,ifold,ifront,zr1,zr1p,zr2,zr2m,zr,kr,kc,zcr1,zcr2,zcr,drrmax,drrmin);
  //G4cout<<"zdivisioncall 6: "<<G4endl;
  //CreateZDivision(parent_logical,pMaterial,wheel_number,((zcr2+zcr1-lar_totalthickness)/2),elements_rotation[element_counter],ifwd,ifold,ifront,zr1,zr1p,zr2,zr2m,zr,kr,kc,zcr1,zcr2,zcr,drrmax,drrmin);

  //Element7
  ifwd=4;
  ifold=1;
  ifront=1;
  zr2m=lar_totalthickness-(absorber_startingpart_length+(supportingbar_container_zsize/4));
  zr1p=lar_totalthickness-(absorber_startingpart_length+(stac/4)-(supportingbar_container_zsize/2));
  zr1=zr1p-(supportingbar_container_zsize/2);
  zr2=zr2m+(supportingbar_container_zsize/2);
  zr=(zr1p+zr2m)/2;
  zcr1=absorber_startingpart_length+(stac/4);
  zcr=zcr1;
  zcr2=zcr1-(supportingbar_container_zsize/2);
  kr=1;
  kc=2;
  drrmax=(stac/4)+(supportingbar_container_zsize/4);
  drrmin=stac/2;
  G4RotationMatrix* element7_rotation=new G4RotationMatrix;

  elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
  rotation_values.clear();
  rotation_values.push_back(element7_rotation->thetaX()); //ThetaX
  rotation_values.push_back(element7_rotation->phiX()); //PhiX
  rotation_values.push_back(element7_rotation->thetaY()); //ThetaY
  rotation_values.push_back((-90*deg)*(2*kc-3)*(2*kr-3)); //PhiY
  rotation_values.push_back((180*deg)*(kc-1)); //ThetaZ
  rotation_values.push_back(element7_rotation->phiZ()); //PhiZ
//   rotation_values.push_back(0*deg); //ThetaX
//   rotation_values.push_back(0*deg); //PhiX
//   rotation_values.push_back(0*deg); //ThetaY
//   rotation_values.push_back((-90*deg)*(2*kc-3)*(2*kr-3)); //PhiY
//   rotation_values.push_back((180*deg)*(kc-1)); //ThetaZ
//   rotation_values.push_back(0*deg); //PhiZ
  //ThetaZ=180*(kc-1)  PhiY=-90*(2*kc-3)*(2*kr-3)
  element7_rotation=0;
  element7_rotation=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
  elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
  //  CreateZDivision(parent_logical,pMaterial,wheel_number,((zcr2+zcr1-lar_totalthickness)/2),element7_rotation,ifwd,ifold,ifront,zr1,zr1p,zr2,zr2m,zr,kr,kc,zcr1,zcr2,zcr,drrmax,drrmin);
  //G4cout<<"zdivisioncall 7: "<<G4endl;
  //CreateZDivision(parent_logical,pMaterial,wheel_number,((zcr2+zcr1-lar_totalthickness)/2),elements_rotation[element_counter],ifwd,ifold,ifront,zr1,zr1p,zr2,zr2m,zr,kr,kc,zcr1,zcr2,zcr,drrmax,drrmin);
  
  //Element8
  ifwd=4;
  ifold=1;
  ifront=-1;
  zr2m=lar_totalthickness-(absorber_startingpart_length+(supportingbar_container_zsize/4));
  zr1p=lar_totalthickness-(absorber_startingpart_length+(stac/4)-(supportingbar_container_zsize/2));
  zr1=zr1p-(supportingbar_container_zsize/2);
  zr2=zr2m+(supportingbar_container_zsize/2);
  zr=(zr1p+zr2m)/2;
  zcr1=lar_totalthickness-(absorber_startingpart_length+(stac/4));
  zcr=zcr1;
  zcr2=zcr1+(supportingbar_container_zsize/2);
  kr=1;
  kc=1;
  drrmin=(stac/4)+(supportingbar_container_zsize/4);
  drrmax=stac/2;
  G4RotationMatrix* element8_rotation=new G4RotationMatrix;

  elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
  rotation_values.clear();
  rotation_values.push_back(element8_rotation->thetaX()); //ThetaX
  rotation_values.push_back(element8_rotation->phiX()); //PhiX
  rotation_values.push_back(element8_rotation->thetaY()); //ThetaY
  rotation_values.push_back((-90*deg)*(2*kc-3)*(2*kr-3)); //PhiY
  rotation_values.push_back((180*deg)*(kc-1)); //ThetaZ
  rotation_values.push_back(element8_rotation->phiZ()); //PhiZ
  //ThetaZ=180*(kc-1)  PhiY=-90*(2*kc-3)*(2*kr-3)
  element8_rotation=0;
  element8_rotation=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
  elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
  //  CreateZDivision(parent_logical,pMaterial,wheel_number,((zcr2+zcr1-lar_totalthickness)/2),element8_rotation,ifwd,ifold,ifront,zr1,zr1p,zr2,zr2m,zr,kr,kc,zcr1,zcr2,zcr,drrmax,drrmin);
  //G4cout<<"zdivisioncall 8: "<<G4endl;
  //CreateZDivision(parent_logical,pMaterial,wheel_number,((zcr2+zcr1-lar_totalthickness)/2),elements_rotation[element_counter],ifwd,ifold,ifront,zr1,zr1p,zr2,zr2m,zr,kr,kc,zcr1,zcr2,zcr,drrmax,drrmin);

  //Loop for something
  ifwd=0;
  for (int iacc=1;iacc<=wheel_parameters_absorbers_numwaves[wheel_number];iacc++)
    {
      for (kr=1;kr<=2;kr++)
	{
	  iroue=2*(iacc-1)+kr;
	  zr=(stac/2)*(iroue-1)+absorber_startingpart_length;
	  zr1=zr-stac/4;
	  zr1p=zr1+(supportingbar_container_zsize/2);
	  zr2=zr+(stac/4);
	  zr2m=zr2-(supportingbar_container_zsize/2);

	  ifold=0;

	  //          if(iacc#1|kr#1)then
	  if (iacc!=1 | kr!=1)
	    {
	      //      G4cout<<"zdivisioncall 9: "<<G4endl;
	      //CreateZDivision(parent_logical,pMaterial,wheel_number,(zr-lar_totalthickness/2),0,ifwd,ifold,ifront,zr1,zr1p,zr2,zr2m,zr,kr,kc,zcr1,zcr2,zcr,drrmax,drrmin);
	      for (kc=1;kc<=2;kc++)
		{
		  zcr=zr+(2*kc-3)*(stac/4);
		  zcr1=zcr;
		  zcr2=zcr-(2*kc-3)*(supportingbar_container_zsize/2);
		  zcrk=zcr-(2*kc-3)*(supportingbar_container_zsize/4);
		  drrmax=(stac/2);
		  drrmin=(stac/2);
		  ifold=1;
		  
		  G4RotationMatrix* element10_rotation=new G4RotationMatrix;
		  
		  elements_tgbrotation[element_counter]=new G4tgbRotationMatrix();
		  rotation_values.clear();
		  rotation_values.push_back(element10_rotation->thetaX()); //ThetaX
		  rotation_values.push_back(element10_rotation->phiX()); //PhiX
		  rotation_values.push_back(element10_rotation->thetaY()); //ThetaY
		  rotation_values.push_back((-90*deg)*(2*kc-3)*(2*kr-3)); //PhiY
		  rotation_values.push_back((180*deg)*(kc-1)); //ThetaZ
		  rotation_values.push_back(element10_rotation->phiZ()); //PhiZ
		  //ThetaZ=180*(kc-1)  PhiY=-90*(2*kc-3)*(2*kr-3)
		  element10_rotation=0;
		  element10_rotation=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
		  elements_rotation[element_counter]=elements_tgbrotation[element_counter]->BuildG4RotMatrixFrom6(rotation_values);
		  
		  //		  CreateZDivision(parent_logical,pMaterial,wheel_number,(zcrk-lar_totalthickness/2),element10_rotation,ifwd,ifold,ifront,zr1,zr1p,zr2,zr2m,zr,kr,kc,zcr1,zcr2,zcr,drrmax,drrmin);
		  //  G4cout<<"zdivisioncall 10: "<<G4endl;
		  //CreateZDivision(parent_logical,pMaterial,wheel_number,(zcrk-lar_totalthickness/2),elements_rotation[element_counter],ifwd,ifold,ifront,zr1,zr1p,zr2,zr2m,zr,kr,kc,zcr1,zcr2,zcr,drrmax,drrmin);
		};
	      
	    };
	};
    }
}











void DetectorConstruction::CreateNonsensitiveKapton(G4Trap*& nonsensitive_kapton_solid,G4LogicalVolume*& nonsensitive_kapton_logical,G4ThreeVector nonsensitive_kapton_translation,G4RotationMatrix* nonsensitive_kapton_rotation,G4LogicalVolume* parent_logical)
{
  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String nonsensitive_kapton_namestub="nonsensitive_kapton-"+number.str();
  
  G4int nonsensitive_kapton_trap_paremeter=1;
  G4double nonsensitive_kapton_pDz=(zr2m-zr1p)/2; //Dz
  G4double nonsensitive_kapton_pTheta=atan((rrext2m+rrint2m-rrint1p-rrext1p)/2/(zr2m-zr1p)); //theta
  G4double nonsensitive_kapton_pPhi=90*deg; //phi
  G4double nonsensitive_kapton_pDy1=(rrext1p-rrint1p)/2; //h1
  G4double nonsensitive_kapton_pDx1=thfc/2; //bl1
  G4double nonsensitive_kapton_pDx2=thfc/2; //tl1
  G4double nonsensitive_kapton_pAlp1=0*deg; //Alp1
  G4double nonsensitive_kapton_pDy2=(rrext2m-rrint2m)/2; //h2 
  G4double nonsensitive_kapton_pDx3=thfc/2; //bl2
  G4double nonsensitive_kapton_pDx4=thfc/2; //tl2
  G4double nonsensitive_kapton_pAlp2=0*deg; //Alp2

  //Solid
  CreateSolid(nonsensitive_kapton_namestub,nonsensitive_kapton_pDz,nonsensitive_kapton_pTheta,nonsensitive_kapton_pPhi,nonsensitive_kapton_pDy1,nonsensitive_kapton_pDx1,nonsensitive_kapton_pDx2,nonsensitive_kapton_pAlp1,nonsensitive_kapton_pDy2,nonsensitive_kapton_pDx3,nonsensitive_kapton_pDx4,nonsensitive_kapton_pAlp2,nonsensitive_kapton_trap_paremeter);

  //Logical Volume
  CreateLogicalVolume(nonsensitive_kapton_solid,FibreGlass,nonsensitive_kapton_namestub);

  //Physical volume
  CreatePhysicalVolume(nonsensitive_kapton_rotation,nonsensitive_kapton_translation,nonsensitive_kapton_logical,nonsensitive_kapton_namestub,parent_logical);
  element_counter=element_counter+1;
  
  CreateNonsensitiveCopper(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(elements_logical_names[element_counter-1]));
  
}

void DetectorConstruction::CreateNonsensitiveCopper(G4Trap*& nonsensitive_copper_solid,G4LogicalVolume*& nonsensitive_copper_logical,G4ThreeVector nonsensitive_copper_translation,G4RotationMatrix* nonsensitive_copper_rotation,G4LogicalVolume* parent_logical)
{
  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String nonsensitive_copper_namestub="nonsensitive_copper-"+number.str();

  G4int nonsensitive_copper_trap_paremeter=1;
  G4double nonsensitive_copper_pDz=(zr2m-zr1p)/2; //Dz
  G4double nonsensitive_copper_pTheta=atan((rrext2m+rrint2m-rrint1p-rrext1p)/2/(zr2m-zr1p)); //theta
  G4double nonsensitive_copper_pPhi=90*deg; //phi
  G4double nonsensitive_copper_pDy1=(rrext1p-rrint1p)/2; //h1
  G4double nonsensitive_copper_pDx1=copper_thickness/2; //bl1
  G4double nonsensitive_copper_pDx2=copper_thickness/2; //tl1
  G4double nonsensitive_copper_pAlp1=0*deg; //Alp1
  G4double nonsensitive_copper_pDy2=(rrext2m-rrint2m)/2; //h2 
  G4double nonsensitive_copper_pDx3=copper_thickness/2; //bl2
  G4double nonsensitive_copper_pDx4=copper_thickness/2; //tl2
  G4double nonsensitive_copper_pAlp2=0*deg; //Alp2

  //Solid
  CreateSolid(nonsensitive_copper_namestub,nonsensitive_copper_pDz,nonsensitive_copper_pTheta,nonsensitive_copper_pPhi,nonsensitive_copper_pDy1,nonsensitive_copper_pDx1,nonsensitive_copper_pDx2,nonsensitive_copper_pAlp1,nonsensitive_copper_pDy2,nonsensitive_copper_pDx3,nonsensitive_copper_pDx4,nonsensitive_copper_pAlp2,nonsensitive_copper_trap_paremeter);

  //  CreateSolid(nonsensitive_copper_namestub,nonsensitive_copper_twistedangle,nonsensitive_copper_pDz,nonsensitive_copper_pTheta,nonsensitive_copper_pPhi,nonsensitive_copper_pDy1,nonsensitive_copper_pDx1,nonsensitive_copper_pDx2,nonsensitive_copper_pDy2,nonsensitive_copper_pDx3,nonsensitive_copper_pDx4,nonsensitive_copper_pAlph);
  //Logical Volume
  CreateLogicalVolume(nonsensitive_copper_solid,Cu,nonsensitive_copper_namestub);

  //Physical volume
  CreatePhysicalVolume(nonsensitive_copper_rotation,nonsensitive_copper_translation,nonsensitive_copper_logical,nonsensitive_copper_namestub,parent_logical);

  element_counter=element_counter+1;
}

G4TwistedTrap* DetectorConstruction::CreateSolid(G4String solid_name,G4double solid_twistedangle,G4double solid_pDz,G4double solid_pTheta,G4double solid_pPhi,G4double solid_pDy1,G4double solid_pDx1,G4double solid_pDx2,G4double solid_pDy2,G4double solid_pDx3,G4double solid_pDx4,G4double solid_pAlph)
{
  std::stringstream pointer;
  std::stringstream dimensions;
  std::stringstream elemcount;
  WriteToLog("\n++++++++++++++++++++Started creating solid++++++++++++++++++++", where_to_write);
  elemcount<< "Element counter: "<<element_counter;
  WriteToLog(elemcount.str(),where_to_write);
  WriteToLog("Type = G4TwistedTrap", where_to_write);
  elements_solid_names[element_counter]=solid_name+"_solid";
  WriteToLog("Name = "+elements_solid_names[element_counter],where_to_write);
  pointer << elements_twistedtraps[element_counter];
  WriteToLog("Pointer before creation of solid = "+pointer.str(), where_to_write);
  dimensions << "Dimensions:     twistedangle: "<<(solid_twistedangle)*convert_radians_degrees<<" pTheta: "<<(solid_pTheta)*convert_radians_degrees;
  WriteToLog(dimensions.str(),where_to_write);
  dimensions.str("");
  dimensions << "                pPhi: "<<(solid_pPhi)*convert_radians_degrees<<" pAlph: "<<(solid_pAlph)*convert_radians_degrees;
  WriteToLog(dimensions.str(),where_to_write);
  dimensions.str("");
  dimensions << "                pDz: "<<(solid_pDz/10);
  WriteToLog(dimensions.str(),where_to_write);
  dimensions.str("");
  dimensions << "                pDx1 (bl1): "<<(solid_pDx1/10)<<" pDx2 (tl1): "<<(solid_pDx2/10)<< " pDx3 (bl2): "<<(solid_pDx3/10)<<" pDx4 (tl2): "<<(solid_pDx4/10);
  WriteToLog(dimensions.str(),where_to_write);
  dimensions.str("");
  dimensions << "                pDy1 (h1): "<<(solid_pDy1/10)<<" pDy2 (h2): "<<(solid_pDy2/10);
  WriteToLog(dimensions.str(),where_to_write);
  dimensions.str("");

  elements_twistedtraps[element_counter] = new G4TwistedTrap(solid_name+"_solid",solid_twistedangle,solid_pDz,solid_pTheta,solid_pPhi,solid_pDy1,solid_pDx1,solid_pDx2,solid_pDy2,solid_pDx3,solid_pDx4,solid_pAlph);

  pointer.str("");
  pointer << elements_twistedtraps[element_counter];
  WriteToLog("Pointer after creation of solid = "+pointer.str(), where_to_write);
  WriteToLog("++++++++++++++++++++Finished creating solid++++++++++++++++++++\n", where_to_write);

  return elements_twistedtraps[element_counter];
}

G4Trap* DetectorConstruction::CreateSolid(G4String solid_name,G4double solid_pDz,G4double solid_pTheta,G4double solid_pPhi,G4double solid_pDy1,G4double solid_pDx1,G4double solid_pDx2,G4double solid_pAlp1,G4double solid_pDy2,G4double solid_pDx3,G4double solid_pDx4,G4double solid_pAlp2,G4int trap_parameter)
{
  trap_parameter=1;
  std::stringstream pointer;
  std::stringstream dimensions;
  std::stringstream elemcount;
  WriteToLog("\n++++++++++++++++++++Started creating solid++++++++++++++++++++", where_to_write);
  elemcount<< "Element counter: "<<element_counter;
  WriteToLog(elemcount.str(),where_to_write);
  WriteToLog("Type = G4Trap", where_to_write);
  elements_solid_names[element_counter]=solid_name+"_solid";
  WriteToLog("Name = "+elements_solid_names[element_counter],where_to_write);
  pointer << elements_traps[element_counter];
  WriteToLog("Pointer before creation of solid = "+pointer.str(), where_to_write);
  dimensions << "Dimensions:     pDz: "<<(solid_pDz/10)<<" pTheta: "<<(solid_pTheta)*convert_radians_degrees;
  WriteToLog(dimensions.str(),where_to_write);
  dimensions.str("");
  dimensions << "                pPhi: "<<(solid_pPhi)*convert_radians_degrees;
  WriteToLog(dimensions.str(),where_to_write);
  dimensions.str("");
  dimensions << "                pAlp1: "<<(solid_pAlp1)*convert_radians_degrees<<" pAlp2: "<<(solid_pAlp2)*convert_radians_degrees;
  WriteToLog(dimensions.str(),where_to_write);
  dimensions.str("");
  dimensions << "                pDx1 (bl1): "<<(solid_pDx1/10)<<" pDx2 (tl1): "<<(solid_pDx2/10)<< " pDx3 (bl2): "<<(solid_pDx3/10)<<" pDx4 (tl2): "<<(solid_pDx4/10);
  WriteToLog(dimensions.str(),where_to_write);
  dimensions.str("");
  dimensions << "                pDy1 (h1): "<<(solid_pDy1/10)<<" pDy2 (h2): "<<(solid_pDy2/10);
  WriteToLog(dimensions.str(),where_to_write);
  dimensions.str("");

  elements_traps[element_counter] = new G4Trap(solid_name+"_solid",solid_pDz,solid_pTheta,solid_pPhi,solid_pDy1,solid_pDx1,solid_pDx2,solid_pAlp1,solid_pDy2,solid_pDx3,solid_pDx4,solid_pAlp2);

  pointer.str("");
  pointer << elements_traps[element_counter];
  WriteToLog("Pointer after creation of solid = "+pointer.str(), where_to_write);
  WriteToLog("++++++++++++++++++++Finished creating solid++++++++++++++++++++\n", where_to_write);

  return elements_traps[element_counter];
}

void DetectorConstruction::CreateNonsensitiveAbsorberWithoutLead(G4Trap*& nonsensitive_absorber_withoutlead_solid,G4LogicalVolume*& nonsensitive_absorber_withoutlead_logical,G4ThreeVector nonsensitive_absorber_withoutlead_translation,G4RotationMatrix* nonsensitive_absorber_withoutlead_rotation,G4LogicalVolume* parent_logical,G4int wheel_number)
{
  tck=wheel_parameters_lead_thickness[wheel_number]+stainlesssteel_thickness+glue_thickness;
  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String nonsensitive_absorber_withoutlead_namestub="nonsensitive_absorber_withoutlead-"+number.str();

  G4int nonsensitive_absorber_withoutlead_trap_paremeter=1;
  G4double nonsensitive_absorber_withoutlead_pDz=(zr2m-zr1p)/2; //Dz
  G4double nonsensitive_absorber_withoutlead_pTheta=atan((rrext2m+rrint2m-rrint1p-rrext1p)/2/(zr2m-zr1p)); //theta
  G4double nonsensitive_absorber_withoutlead_pPhi=90*deg; //phi
  G4double nonsensitive_absorber_withoutlead_pDy1=(rrext1p-rrint1p)/2; //h1
  G4double nonsensitive_absorber_withoutlead_pDx1=tck/2; //bl1
  G4double nonsensitive_absorber_withoutlead_pDx2=tck/2; //tl1
  G4double nonsensitive_absorber_withoutlead_pAlp1=0*deg; //Alp1
  G4double nonsensitive_absorber_withoutlead_pDy2=(rrext2m-rrint2m)/2; //h2 
  G4double nonsensitive_absorber_withoutlead_pDx3=tck/2; //bl2
  G4double nonsensitive_absorber_withoutlead_pDx4=tck/2; //tl2
  G4double nonsensitive_absorber_withoutlead_pAlp2=0*deg; //Alp2

  //Solid
  CreateSolid(nonsensitive_absorber_withoutlead_namestub,nonsensitive_absorber_withoutlead_pDz,nonsensitive_absorber_withoutlead_pTheta,nonsensitive_absorber_withoutlead_pPhi,nonsensitive_absorber_withoutlead_pDy1,nonsensitive_absorber_withoutlead_pDx1,nonsensitive_absorber_withoutlead_pDx2,nonsensitive_absorber_withoutlead_pAlp1,nonsensitive_absorber_withoutlead_pDy2,nonsensitive_absorber_withoutlead_pDx3,nonsensitive_absorber_withoutlead_pDx4,nonsensitive_absorber_withoutlead_pAlp2,nonsensitive_absorber_withoutlead_trap_paremeter);  

  //Logical Volume
  CreateLogicalVolume(nonsensitive_absorber_withoutlead_solid,Fe,nonsensitive_absorber_withoutlead_namestub);

  //Physical volume
  CreatePhysicalVolume(nonsensitive_absorber_withoutlead_rotation,nonsensitive_absorber_withoutlead_translation,nonsensitive_absorber_withoutlead_logical,nonsensitive_absorber_withoutlead_namestub,parent_logical);
  element_counter=element_counter+1;

  CreateNonsensitiveGlue(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],GetLogicalVolume(elements_logical_names[element_counter-1]),wheel_number);
}

void DetectorConstruction::CreateNonsensitiveGlue(G4Trap*& nonsensitive_glue_solid,G4LogicalVolume*& nonsensitive_glue_logical,G4ThreeVector nonsensitive_glue_translation,G4RotationMatrix* nonsensitive_glue_rotation,G4LogicalVolume* parent_logical,G4int wheel_number)
{
  tck=wheel_parameters_lead_thickness[wheel_number]+glue_thickness;
  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String nonsensitive_glue_namestub="nonsensitive_glue-"+number.str();

  G4int nonsensitive_glue_trap_paremeter=1;
  G4double nonsensitive_glue_pDz=(zr2m-zr1p)/2; //Dz
  G4double nonsensitive_glue_pTheta=atan((rrext2m+rrint2m-rrint1p-rrext1p)/2/(zr2m-zr1p)); //theta
  G4double nonsensitive_glue_pPhi=90*deg; //phi
  G4double nonsensitive_glue_pDy1=(rrext1p-rrint1p)/2; //h1
  G4double nonsensitive_glue_pDx1=tck/2; //bl1
  G4double nonsensitive_glue_pDx2=tck/2; //tl1
  G4double nonsensitive_glue_pAlp1=0*deg; //Alp1
  G4double nonsensitive_glue_pDy2=(rrext2m-rrint2m)/2; //h2 
  G4double nonsensitive_glue_pDx3=tck/2; //bl2
  G4double nonsensitive_glue_pDx4=tck/2; //tl2
  G4double nonsensitive_glue_pAlp2=0*deg; //Alp2

  //Solid
  CreateSolid(nonsensitive_glue_namestub,nonsensitive_glue_pDz,nonsensitive_glue_pTheta,nonsensitive_glue_pPhi,nonsensitive_glue_pDy1,nonsensitive_glue_pDx1,nonsensitive_glue_pDx2,nonsensitive_glue_pAlp1,nonsensitive_glue_pDy2,nonsensitive_glue_pDx3,nonsensitive_glue_pDx4,nonsensitive_glue_pAlp2,nonsensitive_glue_trap_paremeter);  

  //Logical Volume
  CreateLogicalVolume(nonsensitive_glue_solid,FibreGlass,nonsensitive_glue_namestub);

  //Physical volume
  CreatePhysicalVolume(nonsensitive_glue_rotation,nonsensitive_glue_translation,nonsensitive_glue_logical,nonsensitive_glue_namestub,parent_logical);
  element_counter=element_counter+1;
  
}

void DetectorConstruction::CreateAbsorberFlatPart(G4TwistedTrap*& absorber_flatpart_solid,G4LogicalVolume*& absorber_flatpart_logical,G4ThreeVector absorber_flatpart_translation,G4RotationMatrix* absorber_flatpart_rotation,G4LogicalVolume* parent_logical,G4int wheel_number)
{
  G4double hfwdi1;
  G4double hfwdo1;

  //   if(nint(endg_mlgap)#1 | ifront#1 | iwh#2)then
  //  if(int(massless_gap_onoff)!=1 | ifront!=1 | iwh!=2) //# here
  if(int(massless_gap_onoff)!=1 | ifront!=1 | wheel_number!=1) //# here
    {
      hfwdi1=hfwdi;
      hfwdo1=hfwdo;
    }
  else
    {
      hfwdi1=hfwdi+0.001;
      hfwdo1=hfwdo+0.001;
    };

      if(ifwd==4 | ifwd==2 | ifwd==3)
	{

	  //Parameters
	  std::stringstream number;
	  number << element_counter;
	  G4String absorber_flatpart_namestub="absorber_flatpart11-"+number.str();
	  G4double absorber_flatpart_twistedangle=-(alfo-alfi)/4; //twis
	  G4double absorber_flatpart_pDz=(rado-radi)/2; //Dz
	  G4double absorber_flatpart_pTheta=thetfw; //theta
	  G4double absorber_flatpart_pPhi=phifwd; //phi
	  G4double absorber_flatpart_pDy1=hfwdi1; //h1
	  G4double absorber_flatpart_pDx1=tcki/2; //bl1
	  G4double absorber_flatpart_pDx2=tcki/2; //tl1
	  G4double absorber_flatpart_pDy2=hfwdo1; //h2 
	  G4double absorber_flatpart_pDx3=tcko/2; //bl2
	  G4double absorber_flatpart_pDx4=tcko/2; //tl2
	  G4double absorber_flatpart_pAlph=0*deg; //One alpha
	  
	  //Solid
	  CreateSolid(absorber_flatpart_namestub,absorber_flatpart_twistedangle,absorber_flatpart_pDz,absorber_flatpart_pTheta,absorber_flatpart_pPhi,absorber_flatpart_pDy1,absorber_flatpart_pDx1,absorber_flatpart_pDx2,absorber_flatpart_pDy2,absorber_flatpart_pDx3,absorber_flatpart_pDx4,absorber_flatpart_pAlph);
	  
	  //Logical Volume
	  CreateLogicalVolume(absorber_flatpart_solid,Fe,absorber_flatpart_namestub);
	  
	  //Physical volume
	  CreatePhysicalVolume(absorber_flatpart_rotation,absorber_flatpart_translation,absorber_flatpart_logical,absorber_flatpart_namestub,parent_logical);
	  element_counter=element_counter+1;
	  
	  CreateGlueLayer(elements_twistedtraps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],elements_logical[element_counter-1],hfwdi1,hfwdo1,wheel_number); //EXij
	}
      else if(ifwd==0)
	{
	  //Parameters
	  std::stringstream number;
	  number << element_counter;
	  G4String absorber_flatpart_namestub="absorber_flatpart22-"+number.str();
	  G4double absorber_flatpart_twistedangle=-(alfo-alfi)/4; //twis
	  G4double absorber_flatpart_pDz=(rado-radi)/2; //Dz
	  G4double absorber_flatpart_pTheta=0*deg; //theta
	  G4double absorber_flatpart_pPhi=0*deg; //phi
	  G4double absorber_flatpart_pDy1=stac/4/sin(alfi/2)-(rcrb+tcki/2)/tan(alfi/2); //h1
	  G4double absorber_flatpart_pDx1=tcki/2; //bl1
	  G4double absorber_flatpart_pDx2=tcki/2; //tl1
	  G4double absorber_flatpart_pDy2=stac/4/sin(alfo/2)-(rcrb+tcko/2)/tan(alfo/2); //h2 
	  G4double absorber_flatpart_pDx3=tcko/2; //bl2
	  G4double absorber_flatpart_pDx4=tcko/2; //tl2
	  G4double absorber_flatpart_pAlph=0*deg; //One alpha
	  
	  //Solid
	  CreateSolid(absorber_flatpart_namestub,absorber_flatpart_twistedangle,absorber_flatpart_pDz,absorber_flatpart_pTheta,absorber_flatpart_pPhi,absorber_flatpart_pDy1,absorber_flatpart_pDx1,absorber_flatpart_pDx2,absorber_flatpart_pDy2,absorber_flatpart_pDx3,absorber_flatpart_pDx4,absorber_flatpart_pAlph);
	  
	  //Logical Volume
	  CreateLogicalVolume(absorber_flatpart_solid,Pb,absorber_flatpart_namestub);
	  
	  //Physical volume
	  CreatePhysicalVolume(absorber_flatpart_rotation,absorber_flatpart_translation,absorber_flatpart_logical,absorber_flatpart_namestub,parent_logical);
	  element_counter=element_counter+1;

	  dxiro=-((tcko+tcki)/4-stainlesssteel_thickness/4)*sin((alfo-alfi)/2);
	  dyiro=(tcko-tcki)/2;
	  driro=sqrt(pow(dxiro,2)+pow(dyiro,2));

	  elements_translation[element_counter].set((tcko+tcki)/4-stainlesssteel_thickness/4,0*cm,0*cm);
	  CreateIronLayer(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],elements_logical[element_counter-1],(rado-radi)/2,atan2(dxiro,dyiro),atan(driro/(rado-radi)),-(alfodg-alfidg)/4,stac/4/sin(alfi/2)-(rcrb+tcki/2)/tan(alfi/2),stainlesssteel_thickness/4,stainlesssteel_thickness/4,stac/4/sin(alfo/2)-(rcrb+tcko/2)/tan(alfo/2),stainlesssteel_thickness/4,stainlesssteel_thickness/4,0,0); //EIRO

	  elements_translation[element_counter].set((tcko+tcki)/4-stainlesssteel_thickness/4,0*cm,0*cm);
	  CreateIronLayer(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],elements_logical[element_counter-1],(rado-radi)/2,atan2(-dxiro,-dyiro),atan(driro/(rado-radi)),-(alfodg-alfidg)/4,stac/4/sin(alfi/2)-(rcrb+tcki/2)/tan(alfi/2),stainlesssteel_thickness/4,stainlesssteel_thickness/4,stac/4/sin(alfo/2)-(rcrb+tcko/2)/tan(alfo/2),stainlesssteel_thickness/4,stainlesssteel_thickness/4,0,0); //EIRO

	  dxglu=-((tcko+tcki)/4-stainlesssteel_thickness/2-glue_thickness/4)*sin((alfo-alfi)/2);
	  dyglu=(tcko-tcki)/2;
	  drglu=sqrt(pow(dxglu,2)+pow(dyglu,2));

	  elements_translation[element_counter].set((tcko+tcki)/4-stainlesssteel_thickness/2-glue_thickness/4,0*cm,0*cm);
	  CreateGlueLayer3(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],elements_logical[element_counter-1],(rado-radi)/2,atan2(dxglu,dyglu),atan(drglu/(rado-radi)),-(alfodg-alfidg)/4,stac/4/sin(alfi/2)-(rcrb+tcki/2)/tan(alfi/2),glue_thickness/4,glue_thickness/4,stac/4/sin(alfo/2)-(rcrb+tcko/2)/tan(alfo/2),glue_thickness/4,glue_thickness/4,0,0); //EGLU

	  elements_translation[element_counter].set(-((tcko+tcki)/4-stainlesssteel_thickness/2-glue_thickness/4),0*cm,0*cm);
	  CreateGlueLayer3(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],elements_logical[element_counter-1],(rado-radi)/2,atan2(-dxglu,-dyglu),atan(drglu/(rado-radi)),-(alfodg-alfidg)/4,stac/4/sin(alfi/2)-(rcrb+tcki/2)/tan(alfi/2),glue_thickness/4,glue_thickness/4,stac/4/sin(alfo/2)-(rcrb+tcko/2)/tan(alfo/2),glue_thickness/4,glue_thickness/4,0,0); //EGLU

	};
}

void DetectorConstruction::CreateGlueLayer(G4TwistedTrap*& glue_layer_solid,G4LogicalVolume*& glue_layer_logical,G4ThreeVector glue_layer_translation,G4RotationMatrix* glue_layer_rotation,G4LogicalVolume* parent_logical,G4double hfwdi1,G4double hfwdo1,G4int wheel_number)
{
  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String glue_layer_namestub="glue_layer-"+number.str();
  G4double glue_layer_twistedangle=-(alfo-alfi)/4; //twis
  G4double glue_layer_pDz=(rado-radi)/2; //Dz
  G4double glue_layer_pTheta=thetfw; //theta
  G4double glue_layer_pPhi=phifwd; //phi
  G4double glue_layer_pDy1=hfwdi1; //h1
  G4double glue_layer_pDx1=(tcki-stainlesssteel_thickness)/2; //bl1
  G4double glue_layer_pDx2=(tcki-stainlesssteel_thickness)/2; //tl1
  G4double glue_layer_pDy2=hfwdo1; //h2 
  G4double glue_layer_pDx3=(tcko-stainlesssteel_thickness)/2; //bl2
  G4double glue_layer_pDx4=(tcko-stainlesssteel_thickness)/2; //tl2
  G4double glue_layer_pAlph=0*deg; //One alpha

  //Solid
  CreateSolid(glue_layer_namestub,glue_layer_twistedangle,glue_layer_pDz,glue_layer_pTheta,glue_layer_pPhi,glue_layer_pDy1,glue_layer_pDx1,glue_layer_pDx2,glue_layer_pDy2,glue_layer_pDx3,glue_layer_pDx4,glue_layer_pAlph);

  //Logical Volume
  CreateLogicalVolume(glue_layer_solid,FibreGlass,glue_layer_namestub);

  //Physical volume
  CreatePhysicalVolume(glue_layer_rotation,glue_layer_translation,glue_layer_logical,glue_layer_namestub,parent_logical);
  element_counter=element_counter+1;

  CreateLeadLayer(elements_twistedtraps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],elements_logical[element_counter-1],hfwdi1,hfwdo1,wheel_number); // EYij  
}

void DetectorConstruction::CreateLeadLayer(G4TwistedTrap*& lead_layer_solid,G4LogicalVolume*& lead_layer_logical,G4ThreeVector lead_layer_translation,G4RotationMatrix* lead_layer_rotation,G4LogicalVolume* parent_logical,G4double hfwdi1,G4double hfwdo1,G4int wheel_number)
{
  G4Material* pMaterial;
  //  if(nint(endg_mlgap)#1 | ifront#1 | iwh#2)then
  //  if(int(massless_gap_onoff)!=1 | ifront!=1 | iwh!=2)
  if(int(massless_gap_onoff)!=1 | ifront!=1 | wheel_number!=1)
    {
      pMaterial=Pb;
    }
  else
    {
      pMaterial=Al;
    };

 //Parameters
  std::stringstream number;
  number << element_counter;
  G4String lead_layer_namestub="lead_layer-"+number.str();
  G4double lead_layer_twistedangle=-(alfo-alfi)/4; //twis
  G4double lead_layer_pDz=(rado-radi)/2; //Dz
  G4double lead_layer_pTheta=thetfw; //theta
  G4double lead_layer_pPhi=phifwd; //phi
  G4double lead_layer_pDy1=hfwdi1; //h1
  G4double lead_layer_pDx1=(tcki-stainlesssteel_thickness-glue_thickness)/2; //bl1
  G4double lead_layer_pDx2=(tcki-stainlesssteel_thickness-glue_thickness)/2; //tl1
  G4double lead_layer_pDy2=hfwdo1; //h2 
  G4double lead_layer_pDx3=(tcko-stainlesssteel_thickness-glue_thickness)/2; //bl2
  G4double lead_layer_pDx4=(tcko-stainlesssteel_thickness-glue_thickness)/2; //tl2
  G4double lead_layer_pAlph=0*deg; //One alpha

  //Solid
  CreateSolid(lead_layer_namestub,lead_layer_twistedangle,lead_layer_pDz,lead_layer_pTheta,lead_layer_pPhi,lead_layer_pDy1,lead_layer_pDx1,lead_layer_pDx2,lead_layer_pDy2,lead_layer_pDx3,lead_layer_pDx4,lead_layer_pAlph);

  //Logical Volume
  CreateLogicalVolume(lead_layer_solid,pMaterial,lead_layer_namestub);

  //Physical volume
  CreatePhysicalVolume(lead_layer_rotation,lead_layer_translation,lead_layer_logical,lead_layer_namestub,parent_logical);
  element_counter=element_counter+1;
 
}

void DetectorConstruction::CreateKaptonPlane(G4TwistedTrap*& kapton_plane_solid,G4LogicalVolume*& kapton_plane_logical,G4ThreeVector kapton_plane_translation,G4RotationMatrix* kapton_plane_rotation,G4LogicalVolume* parent_logical)
{

  G4String kapton_plane_namestub="";
  G4double kapton_plane_twistedangle=0*deg;
  G4double kapton_plane_pDz=0*cm;
  G4double kapton_plane_pTheta=0*deg; //
  G4double kapton_plane_pPhi=0*deg; //
  G4double kapton_plane_pDy1=0*cm; //
  G4double kapton_plane_pDx1=0*cm;
  G4double kapton_plane_pDx2=0*cm;
  G4double kapton_plane_pDy2=0*cm; //
  G4double kapton_plane_pDx3=0*cm;
  G4double kapton_plane_pDx4=0*cm;
  G4double kapton_plane_pAlph=0*deg;


  if(ifwd==4 | ifwd==2 | ifwd==3)
    {

      //Parameters
      std::stringstream number;
      number << element_counter;
      kapton_plane_namestub="kapton_plane1-"+number.str();
      kapton_plane_twistedangle=-(alfo-alfi)/4; //twis
      kapton_plane_pDz=(rado-radi)/2; //Dz
      kapton_plane_pTheta=thetfw; //theta
      kapton_plane_pPhi=phifwd; //phi
      kapton_plane_pDy1=hfwdi; //h1
      kapton_plane_pDx1=thfc/2; //bl1
      kapton_plane_pDx2=thfc/2; //tl1
      kapton_plane_pDy2=hfwdo; //h2 
      kapton_plane_pDx3=thfc/2; //bl2
      kapton_plane_pDx4=thfc/2; //tl2
      kapton_plane_pAlph=0*deg; //One alpha
      
      //Solid
      CreateSolid(kapton_plane_namestub,kapton_plane_twistedangle,kapton_plane_pDz,kapton_plane_pTheta,kapton_plane_pPhi,kapton_plane_pDy1,kapton_plane_pDx1,kapton_plane_pDx2,kapton_plane_pDy2,kapton_plane_pDx3,kapton_plane_pDx4,kapton_plane_pAlph);
      
      //Logical Volume
      CreateLogicalVolume(kapton_plane_solid,FibreGlass,kapton_plane_namestub);
      
      //Physical volume
      CreatePhysicalVolume(kapton_plane_rotation,kapton_plane_translation,kapton_plane_logical,kapton_plane_namestub,parent_logical);
      element_counter=element_counter+1;
      
    }
  else if (ifwd==0)
    {
      
      //Parameters
      std::stringstream number;
      number << element_counter;
      kapton_plane_namestub="kapton_plane2-"+number.str();
      kapton_plane_twistedangle=-(alfo-alfi)/4; //twis
      kapton_plane_pDz=(rado-radi)/2; //Dz
      kapton_plane_pTheta=0*deg; //theta
      kapton_plane_pPhi=0*deg; //phi
      kapton_plane_pDy1=stac/4/sin(alfi/2)-kapton_curvature_radius/tan(alfi/2); //h1
      kapton_plane_pDx1=thfc/2; //bl1
      kapton_plane_pDx2=thfc/2; //tl1
      kapton_plane_pDy2=stac/4/sin(alfo/2)-kapton_curvature_radius/tan(alfo/2); //h2 
      kapton_plane_pDx3=thfc/2; //bl2
      kapton_plane_pDx4=thfc/2; //tl2
      kapton_plane_pAlph=0*deg; //One alpha
      
      //Solid
      CreateSolid(kapton_plane_namestub,kapton_plane_twistedangle,kapton_plane_pDz,kapton_plane_pTheta,kapton_plane_pPhi,kapton_plane_pDy1,kapton_plane_pDx1,kapton_plane_pDx2,kapton_plane_pDy2,kapton_plane_pDx3,kapton_plane_pDx4,kapton_plane_pAlph);
      
      //Logical Volume
      CreateLogicalVolume(kapton_plane_solid,FibreGlass,kapton_plane_namestub);
      
      //Physical volume
      CreatePhysicalVolume(kapton_plane_rotation,kapton_plane_translation,kapton_plane_logical,kapton_plane_namestub,parent_logical);
  element_counter=element_counter+1;
  
    };
  
  CreateCopperElectrodeInKapton(elements_twistedtraps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],elements_logical[element_counter-1],kapton_plane_pTheta,kapton_plane_pPhi,kapton_plane_pDy1,kapton_plane_pDy2); // EELC 
}


void DetectorConstruction::CreateCopperElectrodeInKapton(G4TwistedTrap*& copper_electrode_in_kapton_solid,G4LogicalVolume*& copper_electrode_in_kapton_logical,G4ThreeVector copper_electrode_in_kapton_translation,G4RotationMatrix* copper_electrode_in_kapton_rotation,G4LogicalVolume* parent_logical,G4double pTheta,G4double pPhi,G4double pDy1,G4double pDy2)
{

 //Parameters
  std::stringstream number;
  number << element_counter;
  G4String copper_electrode_in_kapton_namestub="copper_electrode_in_kapton-"+number.str();
  G4double copper_electrode_in_kapton_twistedangle=-(alfo-alfi)/4; //twis
  G4double copper_electrode_in_kapton_pDz=(rado-radi)/2; //Dz
  G4double copper_electrode_in_kapton_pTheta=pTheta; //theta
  G4double copper_electrode_in_kapton_pPhi=pPhi; //phi
  G4double copper_electrode_in_kapton_pDy1=pDy1; //h1
  G4double copper_electrode_in_kapton_pDx1=copper_thickness/2; //bl1
  G4double copper_electrode_in_kapton_pDx2=copper_thickness/2; //tl1
  G4double copper_electrode_in_kapton_pDy2=pDy2; //h2 
  G4double copper_electrode_in_kapton_pDx3=copper_thickness/2; //bl2
  G4double copper_electrode_in_kapton_pDx4=copper_thickness/2; //tl2
  G4double copper_electrode_in_kapton_pAlph=0*deg; //One alpha

  //Solid
  CreateSolid(copper_electrode_in_kapton_namestub,copper_electrode_in_kapton_twistedangle,copper_electrode_in_kapton_pDz,copper_electrode_in_kapton_pTheta,copper_electrode_in_kapton_pPhi,copper_electrode_in_kapton_pDy1,copper_electrode_in_kapton_pDx1,copper_electrode_in_kapton_pDx2,copper_electrode_in_kapton_pDy2,copper_electrode_in_kapton_pDx3,copper_electrode_in_kapton_pDx4,copper_electrode_in_kapton_pAlph);

  //Logical Volume
  CreateLogicalVolume(copper_electrode_in_kapton_solid,Cu,copper_electrode_in_kapton_namestub);

  //Physical volume
  CreatePhysicalVolume(copper_electrode_in_kapton_rotation,copper_electrode_in_kapton_translation,copper_electrode_in_kapton_logical,copper_electrode_in_kapton_namestub,parent_logical);
  element_counter=element_counter+1;
 
}

void DetectorConstruction::CreateAbsorberBeginning(G4Trap*& absorber_beginning_solid,G4LogicalVolume*& absorber_beginning_logical,G4ThreeVector absorber_beginning_translation,G4RotationMatrix* absorber_beginning_rotation,G4LogicalVolume* parent_logical)
{
 //Parameters
  std::stringstream number;
  number << element_counter;
  G4String absorber_beginning_namestub="absorber_beginning-"+number.str();

  G4int absorber_beginning_trap_paremeter=1;
  G4double absorber_beginning_pDz=(rado-radi)/2; //Dz
  G4double absorber_beginning_pTheta=ciltet/2; //theta
  G4double absorber_beginning_pPhi=-90*deg; //phi
  G4double absorber_beginning_pDy1=supportingbar_container_zsize/8-(rcrb+tcki/2)*tan((pi-alfi)/4)/2; //h1
  G4double absorber_beginning_pDx1=tcki/2; //bl1
  G4double absorber_beginning_pDx2=tcki/2; //tl1
  G4double absorber_beginning_pAlp1=0*deg; //Alp1
  G4double absorber_beginning_pDy2=supportingbar_container_zsize/8-(rcrb+tcko/2)*tan((pi-alfo)/4)/2; //h2 
  G4double absorber_beginning_pDx3=tcko/2; //bl2
  G4double absorber_beginning_pDx4=tcko/2; //tl2
  G4double absorber_beginning_pAlp2=0; //Alp2

  //Solid
  CreateSolid(absorber_beginning_namestub,absorber_beginning_pDz,absorber_beginning_pTheta,absorber_beginning_pPhi,absorber_beginning_pDy1,absorber_beginning_pDx1,absorber_beginning_pDx2,absorber_beginning_pAlp1,absorber_beginning_pDy2,absorber_beginning_pDx3,absorber_beginning_pDx4,absorber_beginning_pAlp2,absorber_beginning_trap_paremeter);  

  //Logical Volume
  CreateLogicalVolume(absorber_beginning_solid,Fe,absorber_beginning_namestub);

  //Physical volume
  CreatePhysicalVolume(absorber_beginning_rotation,absorber_beginning_translation,absorber_beginning_logical,absorber_beginning_namestub,parent_logical);
  element_counter=element_counter+1;

  CreateGlueBeginning(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],elements_logical[element_counter-1]); // EVij

}

void DetectorConstruction::CreateGlueBeginning(G4Trap*& glue_beginning_solid,G4LogicalVolume*& glue_beginning_logical,G4ThreeVector glue_beginning_translation,G4RotationMatrix* glue_beginning_rotation,G4LogicalVolume* parent_logical)
{

 //Parameters
  std::stringstream number;
  number << element_counter;
  G4String glue_beginning_namestub="glue_beginning-"+number.str();

  G4int glue_beginning_trap_paremeter=1;
  G4double glue_beginning_pDz=(rado-radi)/2; //Dz
  G4double glue_beginning_pTheta=ciltet/2; //theta
  G4double glue_beginning_pPhi=-90*deg; //phi
  G4double glue_beginning_pDy1=supportingbar_container_zsize/8-(rcrb+tcki/2)*tan((pi-alfi)/4)/2; //h1
  G4double glue_beginning_pDx1=(tcki-stainlesssteel_thickness)/2; //bl1
  G4double glue_beginning_pDx2=(tcki-stainlesssteel_thickness)/2; //tl1
  G4double glue_beginning_pAlp1=0*deg; //Alp1
  G4double glue_beginning_pDy2=supportingbar_container_zsize/8-(rcrb+tcko/2)*tan((pi-alfo)/4)/2; //h2 
  G4double glue_beginning_pDx3=(tcko-stainlesssteel_thickness)/2; //bl2
  G4double glue_beginning_pDx4=(tcko-stainlesssteel_thickness)/2; //tl2
  G4double glue_beginning_pAlp2=0; //Alp2

  //Solid
  CreateSolid(glue_beginning_namestub,glue_beginning_pDz,glue_beginning_pTheta,glue_beginning_pPhi,glue_beginning_pDy1,glue_beginning_pDx1,glue_beginning_pDx2,glue_beginning_pAlp1,glue_beginning_pDy2,glue_beginning_pDx3,glue_beginning_pDx4,glue_beginning_pAlp2,glue_beginning_trap_paremeter);  

  //Logical Volume
  CreateLogicalVolume(glue_beginning_solid,FibreGlass,glue_beginning_namestub);

  //Physical volume
  CreatePhysicalVolume(glue_beginning_rotation,glue_beginning_translation,glue_beginning_logical,glue_beginning_namestub,parent_logical);
  element_counter=element_counter+1;

}

void DetectorConstruction::CreateKaptonFold(G4Cons*& kapton_fold_solid,G4LogicalVolume*& kapton_fold_logical,G4ThreeVector kapton_fold_translation,G4RotationMatrix* kapton_fold_rotation,G4LogicalVolume* parent_logical)
{
  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String kapton_fold_namestub="kapton_fold-"+number.str();
  G4double kapton_fold_pRmin1=kapton_curvature_radius-thfc/2;
  G4double kapton_fold_pRmax1=kapton_curvature_radius+thfc/2;
  G4double kapton_fold_pRmin2=kapton_curvature_radius-thfc/2;
  G4double kapton_fold_pRmax2=kapton_curvature_radius+thfc/2;
  G4double kapton_fold_pDz=(rado-radi)/cos(ciltek)/2;
  G4double kapton_fold_pSPhi=180*deg;
  G4double kapton_fold_pDPhi=-alfcil;
//   G4double kapton_fold_pSPhi=180*deg-alfcil;
//   G4double kapton_fold_pDPhi;

//   if (180*deg-kapton_fold_pSPhi<0)
//     {
//       kapton_fold_pDPhi=360*deg+(180*deg-kapton_fold_pSPhi);
//     }
//   else
//     {
//       kapton_fold_pDPhi=(180*deg-kapton_fold_pSPhi);
//     };



  //Solid
  CreateSolid(kapton_fold_namestub,kapton_fold_pRmin1,kapton_fold_pRmax1,kapton_fold_pRmin2,kapton_fold_pRmax2,kapton_fold_pDz,kapton_fold_pSPhi,kapton_fold_pDPhi);

  //Logical Volume
  CreateLogicalVolume(kapton_fold_solid,FibreGlass,kapton_fold_namestub);

  //Physical volume
  CreatePhysicalVolume(kapton_fold_rotation,kapton_fold_translation,kapton_fold_logical,kapton_fold_namestub,parent_logical);
  element_counter=element_counter+1;
  
  //-7-CreateCopperFold(elements_cons[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],elements_logical[element_counter-1]); //ELAR  
}

void DetectorConstruction::CreateCopperFold(G4Cons*& copper_fold_solid,G4LogicalVolume*& copper_fold_logical,G4ThreeVector copper_fold_translation,G4RotationMatrix* copper_fold_rotation,G4LogicalVolume* parent_logical)
{
  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String copper_fold_namestub="copper_fold-"+number.str();
  G4double copper_fold_pRmin1=kapton_curvature_radius-copper_thickness/2;
  G4double copper_fold_pRmax1=kapton_curvature_radius+copper_thickness/2;
  G4double copper_fold_pRmin2=kapton_curvature_radius-copper_thickness/2;
  G4double copper_fold_pRmax2=kapton_curvature_radius+copper_thickness/2;
  G4double copper_fold_pDz=(rado-radi)/cos(ciltek)/2;
  G4double copper_fold_pSPhi=180*deg;
  G4double copper_fold_pDPhi=-alfcil;
  //  G4double copper_fold_pSPhi=180*deg-alfcil;
  //  G4double copper_fold_pDPhi;

  //Solid
  CreateSolid(copper_fold_namestub,copper_fold_pRmin1,copper_fold_pRmax1,copper_fold_pRmin2,copper_fold_pRmax2,copper_fold_pDz,copper_fold_pSPhi,copper_fold_pDPhi);

  //Logical Volume
  CreateLogicalVolume(copper_fold_solid,Cu,copper_fold_namestub);

  //Physical volume
  CreatePhysicalVolume(copper_fold_rotation,copper_fold_translation,copper_fold_logical,copper_fold_namestub,parent_logical);
  element_counter=element_counter+1;


}

void DetectorConstruction::CreateKaptonBeginning(G4Trap*& kapton_beginning_solid,G4LogicalVolume*& kapton_beginning_logical,G4ThreeVector kapton_beginning_translation,G4RotationMatrix* kapton_beginning_rotation,G4LogicalVolume* parent_logical)
{
 //Parameters
  std::stringstream number;
  number << element_counter;
  G4String kapton_beginning_namestub="kapton_beginning-"+number.str();

  G4int kapton_beginning_trap_paremeter=1;
  G4double kapton_beginning_pDz=(rado-radi)/2; //Dz
  G4double kapton_beginning_pTheta=ciltek/2; //theta
  G4double kapton_beginning_pPhi=-90*deg; //phi
  G4double kapton_beginning_pDy1=supportingbar_container_zsize/8-kapton_curvature_radius*tan((pi-alfi)/4)/2; //h1
  G4double kapton_beginning_pDx1=thfc/2; //bl1
  G4double kapton_beginning_pDx2=thfc/2; //tl1
  G4double kapton_beginning_pAlp1=0*deg; //Alp1
  G4double kapton_beginning_pDy2=supportingbar_container_zsize/8-kapton_curvature_radius*tan((pi-alfo)/4)/2; //h2 
  G4double kapton_beginning_pDx3=thfc/2; //bl2
  G4double kapton_beginning_pDx4=thfc/2; //tl2
  G4double kapton_beginning_pAlp2=0; //Alp2

  //Solid
  CreateSolid(kapton_beginning_namestub,kapton_beginning_pDz,kapton_beginning_pTheta,kapton_beginning_pPhi,kapton_beginning_pDy1,kapton_beginning_pDx1,kapton_beginning_pDx2,kapton_beginning_pAlp1,kapton_beginning_pDy2,kapton_beginning_pDx3,kapton_beginning_pDx4,kapton_beginning_pAlp2,kapton_beginning_trap_paremeter);  

  //Logical Volume
  CreateLogicalVolume(kapton_beginning_solid,FibreGlass,kapton_beginning_namestub);

  //Physical volume
  CreatePhysicalVolume(kapton_beginning_rotation,kapton_beginning_translation,kapton_beginning_logical,kapton_beginning_namestub,parent_logical);
  element_counter=element_counter+1;
  
  CreateCopperBeginning(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],elements_logical[element_counter-1]); // EHij
}

void DetectorConstruction::CreateCopperBeginning(G4Trap*& copper_beginning_solid,G4LogicalVolume*& copper_beginning_logical,G4ThreeVector copper_beginning_translation,G4RotationMatrix* copper_beginning_rotation,G4LogicalVolume* parent_logical)
{

  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String copper_beginning_namestub="copper_beginning-"+number.str();

  G4int copper_beginning_trap_paremeter=1;
  G4double copper_beginning_pDz=(rado-radi)/2; //Dz
  G4double copper_beginning_pTheta=ciltek/2; //theta
  G4double copper_beginning_pPhi=-90*deg; //phi
  G4double copper_beginning_pDy1=supportingbar_container_zsize/8-kapton_curvature_radius*tan((pi-alfi)/4)/2; //h1
  G4double copper_beginning_pDx1=copper_thickness/2; //bl1
  G4double copper_beginning_pDx2=copper_thickness/2; //tl1
  G4double copper_beginning_pAlp1=0*deg; //Alp1
  G4double copper_beginning_pDy2=supportingbar_container_zsize/8-kapton_curvature_radius*tan((pi-alfo)/4)/2; //h2 
  G4double copper_beginning_pDx3=copper_thickness/2; //bl2
  G4double copper_beginning_pDx4=copper_thickness/2; //tl2
  G4double copper_beginning_pAlp2=0; //Alp2

  //Solid
  CreateSolid(copper_beginning_namestub,copper_beginning_pDz,copper_beginning_pTheta,copper_beginning_pPhi,copper_beginning_pDy1,copper_beginning_pDx1,copper_beginning_pDx2,copper_beginning_pAlp1,copper_beginning_pDy2,copper_beginning_pDx3,copper_beginning_pDx4,copper_beginning_pAlp2,copper_beginning_trap_paremeter);  

  //Logical Volume
  CreateLogicalVolume(copper_beginning_solid,Cu,copper_beginning_namestub);

  //Physical volume
  CreatePhysicalVolume(copper_beginning_rotation,copper_beginning_translation,copper_beginning_logical,copper_beginning_namestub,parent_logical);
  element_counter=element_counter+1;


}

void DetectorConstruction::CreateAbsorberFlatPart2(G4Trap*& absorber_flatpart_2_solid,G4LogicalVolume*& absorber_flatpart_2_logical,G4ThreeVector absorber_flatpart_2_translation,G4RotationMatrix* absorber_flatpart_2_rotation,G4LogicalVolume* parent_logical)
{

  G4double tckix = tcki/sin(alfi/2.);
  G4double tckox = tcko/sin(alfo/2.);
  G4double thglix = glue_thickness/2./sin(alfi/2.);
  G4double thglox = glue_thickness/2./sin(alfo/2.);
  G4double thfeix = stainlesssteel_thickness/2./sin(alfi/2.);
  G4double thfeox = stainlesssteel_thickness/2./sin(alfo/2.);

  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String absorber_flatpart_2_namestub="absorber_flatpart_2-"+number.str();

  G4int absorber_flatpart_2_trap_paremeter=1;
  G4double absorber_flatpart_2_pDz=(rado-radi)/2; //Dz
  G4double absorber_flatpart_2_pTheta=0*deg; //theta
  G4double absorber_flatpart_2_pPhi=0*deg; //phi
  G4double absorber_flatpart_2_pDy1=sroue/2; //h1
  G4double absorber_flatpart_2_pDx1=tckix/2; //bl1
  G4double absorber_flatpart_2_pDx2=tckix/2; //tl1
  G4double absorber_flatpart_2_pAlp1=0*deg; //Alp1
  G4double absorber_flatpart_2_pDy2=sroue/2; //h2 
  G4double absorber_flatpart_2_pDx3=tckox/2; //bl2
  G4double absorber_flatpart_2_pDx4=tckox/2; //tl2
  G4double absorber_flatpart_2_pAlp2=0*deg; //Alp2
  //Need to fix twist and alpha alp1=90-ALFIDG/2,alp2=90-ALFODG/2

  //Solid
  CreateSolid(absorber_flatpart_2_namestub,absorber_flatpart_2_pDz,absorber_flatpart_2_pTheta,absorber_flatpart_2_pPhi,absorber_flatpart_2_pDy1,absorber_flatpart_2_pDx1,absorber_flatpart_2_pDx2,absorber_flatpart_2_pAlp1,absorber_flatpart_2_pDy2,absorber_flatpart_2_pDx3,absorber_flatpart_2_pDx4,absorber_flatpart_2_pAlp2,absorber_flatpart_2_trap_paremeter);  

  //Logical Volume
  CreateLogicalVolume(absorber_flatpart_2_solid,Pb,absorber_flatpart_2_namestub);

  //Physical volume
  CreatePhysicalVolume(absorber_flatpart_2_rotation,absorber_flatpart_2_translation,absorber_flatpart_2_logical,absorber_flatpart_2_namestub,parent_logical);
  element_counter=element_counter+1;

  elements_translation[element_counter].set((tckox+tckix-thglox-thglix)/4.-(thfeox+thfeix)/2.,0,0);

  CreateGlueLayer2(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],elements_logical[element_counter-1],0*deg,atan(((tckox-tckix-thglox+thglix)/2.-thfeox+thfeix)/(rado-radi)),thglix/2,thglix/2,thglox/2,thglox/2); //EGFU

  elements_translation[element_counter].set(-(tckox+tckix-thglox-thglix)/4.+(thfeox+thfeix)/2.,0*cm,0*cm);

  CreateGlueLayer2(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],elements_logical[element_counter-1],180*deg,atan(((tckox-tckix-thglox+thglix)/2.-thfeox+thfeix)/(rado-radi)),thglix/2,thglix/2,thglox/2,thglox/2); //EGFU

  elements_translation[element_counter].set((tckox+tckix-thfeox-thfeix)/4,0*cm,0*cm);

  CreateIronLayer2(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],elements_logical[element_counter-1],0*deg,atan((tckox-tckix-thfeox+thfeix)/2./(rado-radi)),thfeix/2,thfeix/2,thfeox/2,thfeox/2);

  elements_translation[element_counter].set(-(tckox+tckix-thfeox-thfeix)/4,0*cm,0*cm);

  CreateIronLayer2(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],elements_logical[element_counter-1],180*deg,atan((tckox-tckix-thfeox+thfeix)/2./(rado-radi)),thfeix/2,thfeix/2,thfeox/2,thfeox/2);

}

void DetectorConstruction::CreateGlueLayer2(G4Trap*& glue_layer_2_solid,G4LogicalVolume*& glue_layer_2_logical,G4ThreeVector glue_layer_2_translation,G4RotationMatrix* glue_layer_2_rotation,G4LogicalVolume* parent_logical,G4double phi,G4double theta,G4double bl1,G4double tl1,G4double bl2,G4double tl2)
{
  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String glue_layer_2_namestub="glue_layer_2-"+number.str();

  G4int glue_layer_2_trap_paremeter=1;

  //Set the values based on what is passed
  G4double glue_layer_2_pDz=0.01*cm; //Dz //Not passed
  G4double glue_layer_2_pTheta=theta; //theta
  G4double glue_layer_2_pPhi=phi; //phi
  G4double glue_layer_2_pDy1=0.01*cm; //h1 //Not passed
  G4double glue_layer_2_pDx1=bl1; //bl1
  G4double glue_layer_2_pDx2=tl1; //tl1
  G4double glue_layer_2_pAlp1=0*deg; //Alp1 //Not passed
  G4double glue_layer_2_pDy2=0.01*cm; //h2 //Not passed
  G4double glue_layer_2_pDx3=bl2; //bl2
  G4double glue_layer_2_pDx4=tl2; //tl2
  G4double glue_layer_2_pAlp2=0*deg; //Alp2 //Not passed

  //Overwrite the values based on defaults in MORTRAN
  glue_layer_2_pDz=0.01*cm; //Dz
  glue_layer_2_pTheta=0*deg; //theta
  glue_layer_2_pPhi=0*deg; //phi
  glue_layer_2_pDy1=0.01*cm; //h1
  glue_layer_2_pDx1=0.01*cm; //bl1
  glue_layer_2_pDx2=0.01*cm; //tl1
  glue_layer_2_pAlp1=0*deg; //Alp1
  glue_layer_2_pDy2=0.01*cm; //h2 
  glue_layer_2_pDx3=0.01*cm; //bl2
  glue_layer_2_pDx4=0.01*cm; //tl2
  glue_layer_2_pAlp2=0*deg; //Alp2

  //Solid
  CreateSolid(glue_layer_2_namestub,glue_layer_2_pDz,glue_layer_2_pTheta,glue_layer_2_pPhi,glue_layer_2_pDy1,glue_layer_2_pDx1,glue_layer_2_pDx2,glue_layer_2_pAlp1,glue_layer_2_pDy2,glue_layer_2_pDx3,glue_layer_2_pDx4,glue_layer_2_pAlp2,glue_layer_2_trap_paremeter);  

  //Logical Volume
  CreateLogicalVolume(glue_layer_2_solid,FibreGlass,glue_layer_2_namestub);

  //Physical volume
  CreatePhysicalVolume(glue_layer_2_rotation,glue_layer_2_translation,glue_layer_2_logical,glue_layer_2_namestub,parent_logical);
  element_counter=element_counter+1;

}

void DetectorConstruction::CreateIronLayer2(G4Trap*& iron_layer_2_solid,G4LogicalVolume*& iron_layer_2_logical,G4ThreeVector iron_layer_2_translation,G4RotationMatrix* iron_layer_2_rotation,G4LogicalVolume* parent_logical,G4double phi,G4double theta,G4double bl1,G4double tl1,G4double bl2,G4double tl2)
{

  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String iron_layer_2_namestub="iron_layer_2-"+number.str();

  G4int iron_layer_2_trap_paremeter=1;

  //Set the values based on what is passed
  G4double iron_layer_2_pDz=0.01*cm; //Dz //Not passed
  G4double iron_layer_2_pTheta=theta; //theta
  G4double iron_layer_2_pPhi=phi; //phi
  G4double iron_layer_2_pDy1=0.01*cm; //h1 //Not passed
  G4double iron_layer_2_pDx1=bl1; //bl1
  G4double iron_layer_2_pDx2=tl1; //tl1
  G4double iron_layer_2_pAlp1=0*deg; //Alp1 //Not passed
  G4double iron_layer_2_pDy2=0.01*cm; //h2 //Not passed
  G4double iron_layer_2_pDx3=bl2; //bl2
  G4double iron_layer_2_pDx4=tl2; //tl2
  G4double iron_layer_2_pAlp2=0*deg; //Alp2 //Not passed

  //Overwrite the values with default from MORTRAN
  iron_layer_2_pDz=0.01*cm; //Dz
  iron_layer_2_pTheta=0*deg; //theta
  iron_layer_2_pPhi=0*deg; //phi
  iron_layer_2_pDy1=0.01*cm; //h1
  iron_layer_2_pDx1=0.01*cm; //bl1
  iron_layer_2_pDx2=0.01*cm; //tl1
  iron_layer_2_pAlp1=0*deg; //Alp1
  iron_layer_2_pDy2=0.01*cm; //h2 
  iron_layer_2_pDx3=0.01*cm; //bl2
  iron_layer_2_pDx4=0.01*cm; //tl2
  iron_layer_2_pAlp2=0*deg; //Alp2

  //Solid
  CreateSolid(iron_layer_2_namestub,iron_layer_2_pDz,iron_layer_2_pTheta,iron_layer_2_pPhi,iron_layer_2_pDy1,iron_layer_2_pDx1,iron_layer_2_pDx2,iron_layer_2_pAlp1,iron_layer_2_pDy2,iron_layer_2_pDx3,iron_layer_2_pDx4,iron_layer_2_pAlp2,iron_layer_2_trap_paremeter);  

  //Logical Volume
  CreateLogicalVolume(iron_layer_2_solid,Fe,iron_layer_2_namestub);

  //Physical volume
  CreatePhysicalVolume(iron_layer_2_rotation,iron_layer_2_translation,iron_layer_2_logical,iron_layer_2_namestub,parent_logical);
  element_counter=element_counter+1;

}

void DetectorConstruction::CreateIronTriangle(G4Trap*& iron_triangle_solid,G4LogicalVolume*& iron_triangle_logical,G4ThreeVector iron_triangle_translation,G4RotationMatrix* iron_triangle_rotation,G4LogicalVolume* parent_logical,G4bool li,G4bool lo)
{



  if(li==true)
    {
      alf=alfi;
      tck=tcki;
      rt1=rti1;
      rt2=rti2;
      phiio=-90*deg;
    };
  if(lo==true)
    {
      alf=alfo;
      tck=tcko;
      rt1=rte1;
      rt2=rte2;
      phiio=90*deg;
    };

  ht=stac/4/sin(alf/2)-(rcrb+tck/2)/tan(alf/2);

  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String iron_triangle_namestub="iron_triangle-"+number.str();

  G4int iron_triangle_trap_paremeter=1;
  G4double iron_triangle_pDz=(rt2-rt1)/2; //Dz
  G4double iron_triangle_pTheta=atan(ht/(rt2-rt1)); //theta
  G4double iron_triangle_pPhi=phiio; //phi
  G4double iron_triangle_pDy1=ht; //h1
  G4double iron_triangle_pDx1=tck/2; //bl1
  G4double iron_triangle_pDx2=tck/2; //tl1
  G4double iron_triangle_pAlp1=0*deg; //Alp1
  G4double iron_triangle_pDy2=0.01*cm; //h2  //Should be 0*cm
  G4double iron_triangle_pDx3=tck/2; //bl2
  G4double iron_triangle_pDx4=tck/2; //tl2
  G4double iron_triangle_pAlp2=0*deg; //Alp2

  //Solid
  CreateSolid(iron_triangle_namestub,iron_triangle_pDz,iron_triangle_pTheta,iron_triangle_pPhi,iron_triangle_pDy1,iron_triangle_pDx1,iron_triangle_pDx2,iron_triangle_pAlp1,iron_triangle_pDy2,iron_triangle_pDx3,iron_triangle_pDx4,iron_triangle_pAlp2,iron_triangle_trap_paremeter);  

  //Logical Volume
  CreateLogicalVolume(iron_triangle_solid,Fe,iron_triangle_namestub);

  //Physical volume
  CreatePhysicalVolume(iron_triangle_rotation,iron_triangle_translation,iron_triangle_logical,iron_triangle_namestub,parent_logical);
  element_counter=element_counter+1;

  CreateGlueTriangle(elements_twistedtraps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],elements_logical[element_counter-1],iron_triangle_pDz,iron_triangle_pTheta,iron_triangle_pPhi,iron_triangle_pDy1,iron_triangle_pDy2); // EOij

}

void DetectorConstruction::CreateGlueTriangle(G4TwistedTrap*& glue_triangle_solid,G4LogicalVolume*& glue_triangle_logical,G4ThreeVector glue_triangle_translation,G4RotationMatrix* glue_triangle_rotation,G4LogicalVolume* parent_logical,G4double pDz,G4double pTheta,G4double pPhi,G4double pDy1,G4double pDy2)
{
 //Parameters
  std::stringstream number;
  number << element_counter;
  G4String glue_triangle_namestub="glue_triangle-"+number.str();
  G4double glue_triangle_twistedangle=0.1*deg; //twis
  G4double glue_triangle_pDz=pDz; //Dz
  G4double glue_triangle_pTheta=pTheta; //theta
  G4double glue_triangle_pPhi=pPhi; //phi
  G4double glue_triangle_pDy1=pDy1; //h1
  G4double glue_triangle_pDx1=(tck-stainlesssteel_thickness)/2; //bl1
  G4double glue_triangle_pDx2=(tck-stainlesssteel_thickness)/2; //tl1
  G4double glue_triangle_pDy2=pDy2; //h2 
  G4double glue_triangle_pDx3=(tck-stainlesssteel_thickness)/2; //bl2
  G4double glue_triangle_pDx4=(tck-stainlesssteel_thickness)/2; //tl2
  G4double glue_triangle_pAlph=0*deg; //One alpha

  //Solid
  CreateSolid(glue_triangle_namestub,glue_triangle_twistedangle,glue_triangle_pDz,glue_triangle_pTheta,glue_triangle_pPhi,glue_triangle_pDy1,glue_triangle_pDx1,glue_triangle_pDx2,glue_triangle_pDy2,glue_triangle_pDx3,glue_triangle_pDx4,glue_triangle_pAlph);

  //Logical Volume
  CreateLogicalVolume(glue_triangle_solid,FibreGlass,glue_triangle_namestub);

  //Physical volume
  CreatePhysicalVolume(glue_triangle_rotation,glue_triangle_translation,glue_triangle_logical,glue_triangle_namestub,parent_logical);
  element_counter=element_counter+1;

  CreateLeadTriangle(elements_twistedtraps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],elements_logical[element_counter-1],glue_triangle_pDz,glue_triangle_pTheta,glue_triangle_pPhi,glue_triangle_pDy1,glue_triangle_pDy2); // EMij
}

void DetectorConstruction::CreateLeadTriangle(G4TwistedTrap*& lead_triangle_solid,G4LogicalVolume*& lead_triangle_logical,G4ThreeVector lead_triangle_translation,G4RotationMatrix* lead_triangle_rotation,G4LogicalVolume* parent_logical,G4double pDz,G4double pTheta,G4double pPhi,G4double pDy1,G4double pDy2)
{
  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String lead_triangle_namestub="lead_triangle-"+number.str();
  G4double lead_triangle_twistedangle=0.1*deg; //twis
  G4double lead_triangle_pDz=pDz; //Dz
  G4double lead_triangle_pTheta=pTheta; //theta
  G4double lead_triangle_pPhi=pPhi; //phi
  G4double lead_triangle_pDy1=pDy1; //h1
  G4double lead_triangle_pDx1=(tck-stainlesssteel_thickness-glue_thickness)/2; //bl1
  G4double lead_triangle_pDx2=(tck-stainlesssteel_thickness-glue_thickness)/2; //tl1
  G4double lead_triangle_pDy2=pDy2; //h2 
  G4double lead_triangle_pDx3=(tck-stainlesssteel_thickness-glue_thickness)/2; //bl2
  G4double lead_triangle_pDx4=(tck-stainlesssteel_thickness-glue_thickness)/2; //tl2
  G4double lead_triangle_pAlph=0*deg; //One alpha

  //Solid
  CreateSolid(lead_triangle_namestub,lead_triangle_twistedangle,lead_triangle_pDz,lead_triangle_pTheta,lead_triangle_pPhi,lead_triangle_pDy1,lead_triangle_pDx1,lead_triangle_pDx2,lead_triangle_pDy2,lead_triangle_pDx3,lead_triangle_pDx4,lead_triangle_pAlph);

  //Logical Volume
  CreateLogicalVolume(lead_triangle_solid,Pb,lead_triangle_namestub);

  //Physical volume
  CreatePhysicalVolume(lead_triangle_rotation,lead_triangle_translation,lead_triangle_logical,lead_triangle_namestub,parent_logical);
  element_counter=element_counter+1;

}

void DetectorConstruction::CreateKaptonAnotherPart(G4Trap*& kapton_another_part_solid,G4LogicalVolume*& kapton_another_part_logical,G4ThreeVector kapton_another_part_translation,G4RotationMatrix* kapton_another_part_rotation,G4LogicalVolume* parent_logical)
{

  G4double  thfcix = (kapton_thickness+copper_thickness)/sin(alfi/2.);
  G4double  thfcox = (kapton_thickness+copper_thickness)/sin(alfo/2.);

  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String kapton_another_part_namestub="kapton_another_part-"+number.str();

  G4int kapton_another_part_trap_paremeter=1;
  G4double kapton_another_part_pDz=(rado-radi)/2; //Dz
  G4double kapton_another_part_pTheta=0*deg; //theta
  G4double kapton_another_part_pPhi=0*deg; //phi
  G4double kapton_another_part_pDy1=sroue/2; //h1
  G4double kapton_another_part_pDx1=thfcix/2; //bl1
  G4double kapton_another_part_pDx2=thfcix/2; //tl1
  G4double kapton_another_part_pAlp1=90*deg-alfi/2; //Alp1
  G4double kapton_another_part_pDy2=sroue/2; //h2 
  G4double kapton_another_part_pDx3=thfcox/2; //bl2
  G4double kapton_another_part_pDx4=thfcox/2; //tl2
  G4double kapton_another_part_pAlp2=kapton_another_part_pAlp1;//90*deg-alfo/2; //Alp2 //There is a requirement for both surfaces to be planar, hence pAlp1=pAlp2.

  //Solid
  CreateSolid(kapton_another_part_namestub,kapton_another_part_pDz,kapton_another_part_pTheta,kapton_another_part_pPhi,kapton_another_part_pDy1,kapton_another_part_pDx1,kapton_another_part_pDx2,kapton_another_part_pAlp1,kapton_another_part_pDy2,kapton_another_part_pDx3,kapton_another_part_pDx4,kapton_another_part_pAlp2,kapton_another_part_trap_paremeter);  

  //Logical Volume
  CreateLogicalVolume(kapton_another_part_solid,FibreGlass,kapton_another_part_namestub);

  //Physical volume
  CreatePhysicalVolume(kapton_another_part_rotation,kapton_another_part_translation,kapton_another_part_logical,kapton_another_part_namestub,parent_logical);
  element_counter=element_counter+1;

}


void DetectorConstruction::CreateKaptonTriangle(G4Trap*& kapton_triangle_solid,G4LogicalVolume*& kapton_triangle_logical,G4ThreeVector kapton_triangle_translation,G4RotationMatrix* kapton_triangle_rotation,G4LogicalVolume* parent_logical,G4bool li,G4bool lo)
{

  if (li==true)
    {
      alf=alfi;
      tck=tcki;
      rt1=rti1;
      rt2=rti2;
      phiio=-90*deg;
    };
  if (lo==true)
    {
      alf=alfo;
      tck=tcko;
      rt1=rte1;
      rt2=rte2;
      phiio=90*deg;
    };
  ht=stac/4/sin(alf/2)-kapton_curvature_radius/tan(alf/2);

  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String kapton_triangle_namestub="kapton_triangle-"+number.str();

  G4int kapton_triangle_trap_paremeter=1;
  G4double kapton_triangle_pDz=(rt2-rt1)/2; //Dz
  G4double kapton_triangle_pTheta=atan(ht/(rt2-rt1)); //theta
  G4double kapton_triangle_pPhi=phiio; //phi
  G4double kapton_triangle_pDy1=ht; //h1
  G4double kapton_triangle_pDx1=thfc/2; //bl1
  G4double kapton_triangle_pDx2=thfc/2; //tl1
  G4double kapton_triangle_pAlp1=0*deg; //Alp1
  G4double kapton_triangle_pDy2=0.001*cm; //h2 
  G4double kapton_triangle_pDx3=thfc/2; //bl2
  G4double kapton_triangle_pDx4=thfc/2; //tl2
  G4double kapton_triangle_pAlp2=0*deg;

  //Solid
  CreateSolid(kapton_triangle_namestub,kapton_triangle_pDz,kapton_triangle_pTheta,kapton_triangle_pPhi,kapton_triangle_pDy1,kapton_triangle_pDx1,kapton_triangle_pDx2,kapton_triangle_pAlp1,kapton_triangle_pDy2,kapton_triangle_pDx3,kapton_triangle_pDx4,kapton_triangle_pAlp2,kapton_triangle_trap_paremeter);  

  //Logical Volume
  CreateLogicalVolume(kapton_triangle_solid,FibreGlass,kapton_triangle_namestub);

  //Physical volume
  CreatePhysicalVolume(kapton_triangle_rotation,kapton_triangle_translation,kapton_triangle_logical,kapton_triangle_namestub,parent_logical);
  element_counter=element_counter+1;
  CreateCopperTriangle(elements_traps[element_counter],elements_logical[element_counter],elements_translation[element_counter],elements_rotation[element_counter],elements_logical[element_counter-1],kapton_triangle_pDz,kapton_triangle_pTheta,kapton_triangle_pPhi,kapton_triangle_pDy1,kapton_triangle_pDy2); // EQij
}

void DetectorConstruction::CreateCopperTriangle(G4Trap*& copper_triangle_solid,G4LogicalVolume*& copper_triangle_logical,G4ThreeVector copper_triangle_translation,G4RotationMatrix* copper_triangle_rotation,G4LogicalVolume* parent_logical, G4double pDz,G4double pTheta,G4double pPhi,G4double pDy1,G4double pDy2)
{


  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String copper_triangle_namestub="copper_triangle-"+number.str();

  G4int copper_triangle_trap_paremeter=1;
  G4double copper_triangle_pDz=pDz; //Dz
  G4double copper_triangle_pTheta=pTheta; //theta
  G4double copper_triangle_pPhi=pPhi; //phi
  G4double copper_triangle_pDy1=pDy1; //h1
  G4double copper_triangle_pDx1=copper_thickness/2; //bl1
  G4double copper_triangle_pDx2=copper_thickness/2; //tl1
  G4double copper_triangle_pAlp1=0*deg; //Alp1
  G4double copper_triangle_pDy2=pDy2; //h2 
  G4double copper_triangle_pDx3=copper_thickness/2; //bl2
  G4double copper_triangle_pDx4=copper_thickness/2; //tl2
  G4double copper_triangle_pAlp2=0*deg; //Alp2 //There is a requirement for both surfaces to be planar, hence pAlp1=pAlp2.
  //Solid
  CreateSolid(copper_triangle_namestub,copper_triangle_pDz,copper_triangle_pTheta,copper_triangle_pPhi,copper_triangle_pDy1,copper_triangle_pDx1,copper_triangle_pDx2,copper_triangle_pAlp1,copper_triangle_pDy2,copper_triangle_pDx3,copper_triangle_pDx4,copper_triangle_pAlp2,copper_triangle_trap_paremeter);  

  //Logical Volume
  CreateLogicalVolume(copper_triangle_solid,Cu,copper_triangle_namestub);

  //Physical volume
  CreatePhysicalVolume(copper_triangle_rotation,copper_triangle_translation,copper_triangle_logical,copper_triangle_namestub,parent_logical);
  element_counter=element_counter+1;

}

void DetectorConstruction::CreateIronLayer(G4Trap*& iron_layer_solid,G4LogicalVolume*& iron_layer_logical,G4ThreeVector iron_layer_translation,G4RotationMatrix* iron_layer_rotation,G4LogicalVolume* parent_logical,G4double pDz,G4double phi,G4double theta,G4double twist,G4double h1,G4double bl1,G4double tl1,G4double h2,G4double bl2,G4double tl2,G4double pAlp1,G4double pAlp2)
{

  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String iron_layer_namestub="iron_layer-"+number.str();

  G4int iron_layer_trap_paremeter=1;

  //Set the values based on what is passed
  G4double iron_layer_pDz=pDz; //Dz
  G4double iron_layer_twist=twist; //twis
  G4double iron_layer_pTheta=theta; //theta
  G4double iron_layer_pPhi=phi; //phi
  G4double iron_layer_pDy1=h1; //h1
  G4double iron_layer_pDx1=bl1; //bl1
  G4double iron_layer_pDx2=tl1; //tl1
  G4double iron_layer_pAlp1=pAlp1; //Alp1
  G4double iron_layer_pDy2=h2; //h2 
  G4double iron_layer_pDx3=bl2; //bl2
  G4double iron_layer_pDx4=tl2; //tl2
  G4double iron_layer_pAlp2=pAlp2; //Alp2

  //Overwrite the values based on some defaults (MORTRAN)
  iron_layer_pDz=0.01*cm; //Dz
  iron_layer_twist=0*deg; //twis
  iron_layer_pTheta=0*deg; //theta
  iron_layer_pPhi=0*deg; //phi
  iron_layer_pDy1=0.01*cm; //h1
  iron_layer_pDx1=0.01*cm; //bl1
  iron_layer_pDx2=0.01*cm; //tl1
  iron_layer_pAlp1=0*deg; //Alp1
  iron_layer_pDy2=0.01*cm; //h2 
  iron_layer_pDx3=0.01*cm; //bl2
  iron_layer_pDx4=0.01*cm; //tl2
  iron_layer_pAlp2=0*deg; //Alp2

  //Solid
  CreateSolid(iron_layer_namestub,iron_layer_pDz,iron_layer_pTheta,iron_layer_pPhi,iron_layer_pDy1,iron_layer_pDx1,iron_layer_pDx2,iron_layer_pAlp1,iron_layer_pDy2,iron_layer_pDx3,iron_layer_pDx4,iron_layer_pAlp2,iron_layer_trap_paremeter);  

  //Logical Volume
  CreateLogicalVolume(iron_layer_solid,Fe,iron_layer_namestub);

  //Physical volume
  CreatePhysicalVolume(iron_layer_rotation,iron_layer_translation,iron_layer_logical,iron_layer_namestub,parent_logical);
  element_counter=element_counter+1;

}

void DetectorConstruction::CreateGlueLayer3(G4Trap*& glue_layer_3_solid,G4LogicalVolume*& glue_layer_3_logical,G4ThreeVector glue_layer_3_translation,G4RotationMatrix* glue_layer_3_rotation,G4LogicalVolume* parent_logical,G4double pDz,G4double phi,G4double theta,G4double twist,G4double h1,G4double bl1,G4double tl1,G4double h2,G4double bl2,G4double tl2,G4double pAlp1,G4double pAlp2)
{

  //Parameters
  std::stringstream number;
  number << element_counter;
  G4String glue_layer_3_namestub="glue_layer_3-"+number.str();

  G4int glue_layer_3_trap_paremeter=1;

  //Set the values based on what is passed
  G4double glue_layer_3_pDz=pDz; //Dz
  G4double glue_layer_3_twist=twist; //twis
  G4double glue_layer_3_pTheta=theta; //theta
  G4double glue_layer_3_pPhi=phi; //phi
  G4double glue_layer_3_pDy1=h1; //h1
  G4double glue_layer_3_pDx1=bl1; //bl1
  G4double glue_layer_3_pDx2=tl1; //tl1
  G4double glue_layer_3_pAlp1=pAlp1; //Alp1
  G4double glue_layer_3_pDy2=h2; //h2 
  G4double glue_layer_3_pDx3=bl2; //bl2
  G4double glue_layer_3_pDx4=tl2; //tl2
  G4double glue_layer_3_pAlp2=pAlp2; //Alp2

  //Overwrite the values based on some defaults (from the MORTRAN)
  glue_layer_3_pDz=0.01*cm; //Dz
  glue_layer_3_twist=0*deg; //twis
  glue_layer_3_pTheta=0*deg; //theta
  glue_layer_3_pPhi=0*deg; //phi
  glue_layer_3_pDy1=0.01*cm; //h1
  glue_layer_3_pDx1=0.01*cm; //bl1
  glue_layer_3_pDx2=0.01*cm; //tl1
  glue_layer_3_pAlp1=0*deg; //Alp1
  glue_layer_3_pDy2=0.01*cm; //h2 
  glue_layer_3_pDx3=0.01*cm; //bl2
  glue_layer_3_pDx4=0.01*cm; //tl2
  glue_layer_3_pAlp2=0*deg; //Alp2

  //Solid
  CreateSolid(glue_layer_3_namestub,glue_layer_3_pDz,glue_layer_3_pTheta,glue_layer_3_pPhi,glue_layer_3_pDy1,glue_layer_3_pDx1,glue_layer_3_pDx2,glue_layer_3_pAlp1,glue_layer_3_pDy2,glue_layer_3_pDx3,glue_layer_3_pDx4,glue_layer_3_pAlp2,glue_layer_3_trap_paremeter);  

  //Logical Volume
  CreateLogicalVolume(glue_layer_3_solid,FibreGlass,glue_layer_3_namestub);

  //Physical volume
  CreatePhysicalVolume(glue_layer_3_rotation,glue_layer_3_translation,glue_layer_3_logical,glue_layer_3_namestub,parent_logical);
  element_counter=element_counter+1;

}

G4int DetectorConstruction::CheckAxes(G4RotationMatrix* rotation)
{
  double del = 0.001;
  if (rotation!=0)
    {
      CLHEP::Hep3Vector newX=rotation->colX();
      CLHEP::Hep3Vector newY=rotation->colY();
      CLHEP::Hep3Vector newZ=rotation->colZ();
      CLHEP::Hep3Vector w = newX.cross(newY);
      //G4cout<<"newX: "<<G4endl;
      PrintTransformMatrices(newX,0);
      //G4cout<<"newY: "<<G4endl;
      PrintTransformMatrices(newY,0);
      //G4cout<<"newZ: "<<G4endl;
      PrintTransformMatrices(newZ,0);
      //G4cout<<"w: "<<G4endl;
      PrintTransformMatrices(w,0);

      if (fabs(newZ.x()-w.x()) > del ||
	  fabs(newZ.y()-w.y()) > del ||
	  fabs(newZ.z()-w.z()) > del ||
	  fabs(newX.mag2()-1.) > del ||
	  fabs(newY.mag2()-1.) > del || 
	  fabs(newZ.mag2()-1.) > del ||
	  fabs(newX.dot(newY)) > del ||
	  fabs(newY.dot(newZ)) > del ||
	  fabs(newZ.dot(newX)) > del)
	{ //If the matrix is not "standard" to within the tolerance del, then:
	  WriteToLog("Non standard rotation matrix used",where_to_write);
	  //Check whether a multiplication by -1 would make it standard
	  newX=-1*newX;
	  newY=-1*newY;
	  newZ=-1*newZ;
	  w=newX.cross(newY);

	  if (fabs(newZ.x()-w.x()) > del ||
	      fabs(newZ.y()-w.y()) > del ||
	      fabs(newZ.z()-w.z()) > del ||
	      fabs(newX.mag2()-1.) > del ||
	      fabs(newY.mag2()-1.) > del || 
	      fabs(newZ.mag2()-1.) > del ||
	      fabs(newX.dot(newY)) > del ||
	      fabs(newY.dot(newZ)) > del ||
	      fabs(newZ.dot(newX)) > del)
	    {
	      //A multiplication by -1 of the matrix would not result in a "standard" rotation.
	      return 0;
	    }
	  else
	    {
	      //A multiplication by -1 of the matrix would result in a "standard" rotation.
	      WriteToLog("A multiplication by -1 of the matrix would result in a standard rotation",where_to_write);
	      return 2;
	    };
	}
      else
	{
	  //If the matrix is "standard" to within the tolerance del, then:
	  return 1;
	};
    }
  else
    { 
      //If no rotation set then
      return 1;
    };
}

void DetectorConstruction::PrintTransformMatrices(G4ThreeVector translation,G4RotationMatrix rotation)
{
  std::stringstream translationstream;
  std::stringstream rotationstream;
  std::stringstream matrixelementsstream;

  translationstream<<"\t\t ( "<<CleanUp(translation[0])<<" , "<<CleanUp(translation[1])<<" , "<<CleanUp(translation[2])<<" )";
  WriteToLog(translationstream.str(),where_to_write);
  translationstream.str("");

  rotationstream<<"\t\tphiX: "<<CleanUp(rotation.phiX())/deg<<" phiY: "<<CleanUp(rotation.phiY())/deg<<" phiZ: "<<CleanUp(rotation.phiZ())/deg;
  WriteToLog(rotationstream.str(),where_to_write);
  rotationstream.str("");
  rotationstream<<"\t\tthetaX: "<<CleanUp(rotation.thetaX())/deg<<" thetaY: "<<CleanUp(rotation.thetaY())/deg<<" thetaZ: "<<CleanUp(rotation.thetaZ())/deg;
  WriteToLog(rotationstream.str(),where_to_write);
  rotationstream.str("");
  matrixelementsstream<<"\t\t ( "<<CleanUp(rotation.xx())<<" , "<<CleanUp(rotation.xy())<<" , "<<CleanUp(rotation.xz())<<" )";
  WriteToLog(matrixelementsstream.str(),where_to_write);
  matrixelementsstream.str("");
  matrixelementsstream<<"\t\t ( "<<CleanUp(rotation.yx())<<" , "<<CleanUp(rotation.yy())<<" , "<<CleanUp(rotation.yz())<<" )";
  WriteToLog(matrixelementsstream.str(),where_to_write);
  matrixelementsstream.str("");
  matrixelementsstream<<"\t\t ( "<<CleanUp(rotation.zx())<<" , "<<CleanUp(rotation.zy())<<" , "<<CleanUp(rotation.zz())<<" )";
  WriteToLog(matrixelementsstream.str(),where_to_write);
  matrixelementsstream.str("");
  
  
}

void DetectorConstruction::PrintTransformMatrices(G4ThreeVector translation,const G4RotationMatrix* rotation)
{
  std::stringstream translationstream;
  translationstream<<"\t\t ( "<<CleanUp(translation[0])<<" , "<<CleanUp(translation[1])<<" , "<<CleanUp(translation[2])<<" )";
  WriteToLog(translationstream.str(),where_to_write);
  translationstream.str("");


  if (rotation==0)
    {
    }

  else
    {
      std::stringstream rotationstream;
      std::stringstream matrixelementsstream;
      rotationstream<<"\t\tphiX: "<<CleanUp(rotation->phiX())/deg<<" phiY: "<<CleanUp(rotation->phiY())/deg<<" phiZ: "<<CleanUp(rotation->phiZ())/deg;
      WriteToLog(rotationstream.str(),where_to_write);
      rotationstream.str("");
      rotationstream<<"\t\tthetaX: "<<CleanUp(rotation->thetaX())/deg<<" thetaY: "<<CleanUp(rotation->thetaY())/deg<<" thetaZ: "<<CleanUp(rotation->thetaZ())/deg;
      WriteToLog(rotationstream.str(),where_to_write);
      rotationstream.str("");
      matrixelementsstream<<"\t\t ( "<<CleanUp(rotation->xx())<<" , "<<CleanUp(rotation->xy())<<" , "<<CleanUp(rotation->xz())<<" )";
      WriteToLog(matrixelementsstream.str(),where_to_write);
      matrixelementsstream.str("");
      matrixelementsstream<<"\t\t ( "<<CleanUp(rotation->yx())<<" , "<<CleanUp(rotation->yy())<<" , "<<CleanUp(rotation->yz())<<" )";
      WriteToLog(matrixelementsstream.str(),where_to_write);
      matrixelementsstream.str("");
      matrixelementsstream<<"\t\t ( "<<CleanUp(rotation->zx())<<" , "<<CleanUp(rotation->zy())<<" , "<<CleanUp(rotation->zz())<<" )";
      WriteToLog(matrixelementsstream.str(),where_to_write);
      matrixelementsstream.str("");
    }
  
  
}

G4int DetectorConstruction::StandardizeMatrix(G4RotationMatrix* rotation)
{
  G4RotationMatrix* temprot = new G4RotationMatrix();
  temprot->rotateAxes(-1*(rotation->colX()),-1*(rotation->colY()),-1*(rotation->colZ()));
  *rotation=*temprot;
  WriteToLog("\t\tThis matrix has been converted to standard form",where_to_write);
  return 1;
}

G4int DetectorConstruction::StandardizeMatrix(G4RotationMatrix* rotation,G4String axis)
{
  G4RotationMatrix* temprot = new G4RotationMatrix();
  if (axis=="x" || axis=="X")
    {
      temprot->rotateAxes(-1*(rotation->colX()),1*(rotation->colY()),1*(rotation->colZ()));
      WriteToLog("\t\tx axis has been inverted",where_to_write);
    }
  else if (axis=="y" || axis=="Y")
    {
      temprot->rotateAxes(1*(rotation->colX()),-1*(rotation->colY()),1*(rotation->colZ()));
      WriteToLog("\t\ty axis has been inverted",where_to_write);
    }
  else if (axis=="z" || axis=="Z")
    {
      temprot->rotateAxes(1*(rotation->colX()),1*(rotation->colY()),-1*(rotation->colZ()));
      WriteToLog("\t\tz axis has been inverted",where_to_write);
    }
  else
    {
      WriteToLog("\t\tAxis not recognized",where_to_write);
    };

  *rotation=*temprot;
  return 1;
}

G4int DetectorConstruction::CheckAxes2(G4RotationMatrix* rotation)
{
  double del = 0.001;
  if (rotation!=0)
    {
      CLHEP::Hep3Vector newX=rotation->colX();
      CLHEP::Hep3Vector newY=rotation->colY();
      CLHEP::Hep3Vector newZ=rotation->colZ();
      CLHEP::Hep3Vector w = newX.cross(newY);
      //G4cout<<"newX: "<<G4endl;
      PrintTransformMatrices(newX,0);
      //G4cout<<"newY: "<<G4endl;
      PrintTransformMatrices(newY,0);
      //G4cout<<"newZ: "<<G4endl;
      PrintTransformMatrices(newZ,0);
      //G4cout<<"w: "<<G4endl;
      PrintTransformMatrices(w,0);

      if (fabs(newZ.x()-w.x()) > del ||
	  fabs(newZ.y()-w.y()) > del ||
	  fabs(newZ.z()-w.z()) > del ||
	  fabs(newX.mag2()-1.) > del ||
	  fabs(newY.mag2()-1.) > del || 
	  fabs(newZ.mag2()-1.) > del ||
	  fabs(newX.dot(newY)) > del ||
	  fabs(newY.dot(newZ)) > del ||
	  fabs(newZ.dot(newX)) > del)
	{ //If the matrix is not "standard" to within the tolerance del, then:
	  WriteToLog("\t\tbadaxisvectors2: ",where_to_write);
	};
    };
  return 0;
}

G4bool DetectorConstruction::IsReflection(const G4Scale3D& scale) const
{
  // Returns true if the scale is negative, false otherwise.                                                                                                                                                      
  // ---                                                                                                                                                                                                          

  if (scale(0,0)*scale(1,1)*scale(2,2) < 0.)
    return true;
  else
    return false;
}


void DetectorConstruction::PrintTransformMatrices(G4Transform3D transform)
{
  std::stringstream matrixelementsstream;
  matrixelementsstream<<"\t\t ( "<<CleanUp(transform.xx())<<" , "<<CleanUp(transform.xy())<<" , "<<CleanUp(transform.xz())<<" , "<<CleanUp(transform.dx())<<" )";
  WriteToLog(matrixelementsstream.str(),where_to_write);
  matrixelementsstream.str("");
  matrixelementsstream<<"\t\t ( "<<CleanUp(transform.yx())<<" , "<<CleanUp(transform.yy())<<" , "<<CleanUp(transform.yz())<<" , "<<CleanUp(transform.dy())<<" )";
  WriteToLog(matrixelementsstream.str(),where_to_write);
  matrixelementsstream.str("");
  matrixelementsstream<<"\t\t ( "<<CleanUp(transform.zx())<<" , "<<CleanUp(transform.zy())<<" , "<<CleanUp(transform .zz())<<" , "<<CleanUp(transform.dz())<<" )";
  WriteToLog(matrixelementsstream.str(),where_to_write);
  matrixelementsstream.str("");
  
}

G4double DetectorConstruction::CleanUp(G4double value)
{
  G4double tolerance=0.00001;
  if (fabs(value) < tolerance)
    {
      value=0;
    };

  return value;
}

void DetectorConstruction::CreateReflectedVolumes()
{
  G4ReflectX3D reflectx;
  G4ReflectY3D reflecty;
  G4ReflectZ3D reflectz;
  G4Transform3D transform;
  WriteToLog("Creating reflected volumes",where_to_write);
  for (unsigned int i=0;i<reflection_volumes.size();i++)
    {
      G4Translate3D translation=elements_translation[reflection_volumes[i]];
      G4Transform3D rotation = G4Rotate3D();
   if (elements_logical_names[reflection_volumes[i]].find("zdivision1")!=G4String::npos)
     {       
       transform = translation*rotation*reflecty;
     }
   else if (elements_logical_names[reflection_volumes[i]].find("zdivision2")!=G4String::npos)
     {
       transform = translation*rotation*reflectz;
     }
   else if (elements_logical_names[reflection_volumes[i]].find("zdivision4")!=G4String::npos)
     {
       transform = translation*rotation*reflecty;
     }
   else if (elements_logical_names[reflection_volumes[i]].find("zdivision4")!=G4String::npos)
     {
       transform = translation*rotation*reflecty;
     }
   
   std::stringstream volumedetails;
   volumedetails.str("");
   volumedetails<<"\nCreating a reflected volume from: "+elements_logical_names[reflection_volumes[i]];
   WriteToLog(volumedetails.str(),where_to_write);

   int found;
   found=elements_logical_names[reflection_volumes[i]].find("logical");

   G4PhysicalVolumesPair PvPair;
   PvPair = G4ReflectionFactory::Instance()->Place(transform,elements_logical_names[reflection_volumes[i]].replace(found,7,"physical_refl"),elements_logical[reflection_volumes[i]],reflection_volumes_parents_logical[i],false,15,false);

   volumedetails.str("");
   volumedetails<<"The physical volume being replaced has the pointer: ";
   volumedetails<<elements_physical[reflection_volumes[i]];
   WriteToLog(volumedetails.str(),where_to_write);

   elements_physical[reflection_volumes[i]]=PvPair.first;
   elements_logical[reflection_volumes[i]]=PvPair.first->GetLogicalVolume();

   volumedetails.str("");
   volumedetails<<"The physical volume created has pointer: ";
   volumedetails<<elements_physical[reflection_volumes[i]];
   WriteToLog(volumedetails.str(),where_to_write);

   volumedetails.str("");
   volumedetails<<"The name of the reflected logical volume is: "+elements_physical[reflection_volumes[i]]->GetLogicalVolume()->GetName();
   WriteToLog(volumedetails.str(),where_to_write);

   volumedetails.str("");
   volumedetails<<"The name of the reflected physical volume is: "+elements_physical[reflection_volumes[i]]->GetName();
   WriteToLog(volumedetails.str(),where_to_write);

   volumedetails.str("");
   volumedetails<<"Just to double check, the name of the mother logical is: ";
   volumedetails<<elements_physical[reflection_volumes[i]]->GetMotherLogical()->GetName();
   WriteToLog(volumedetails.str(),where_to_write);
   
    };
  
}
