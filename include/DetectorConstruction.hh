// $Id: DetectorConstruction.hh 33 2010-01-14 17:08:18Z adotti $
#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
//Sensitive Detectors
//#include "G4SDManager.hh"
//#include "AbsorbersSensitive.hh"
//Materials
#include "G4NistManager.hh"
//Necessary for placements
#include "G4ThreeVector.hh"
//Others
#include <vector>
#include <cmath>
#include <algorithm>
#include "G4String.hh"
#include <iostream>
#include <fstream>
//Solid Types
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4TwistedTrap.hh"
#include "G4Trap.hh"
//Logical Volume
#include "G4LogicalVolume.hh"
//Physical Volume
#include "G4PVPlacement.hh"
#include "G4PVDivision.hh"
#include "G4PVReplica.hh"
//Reflection
#include "G4ReflectionFactory.hh"
#include "G4Transform3D.hh"
#include "G4ReflectedSolid.hh"
//GDML
#include "G4GDMLParser.hh"
//Additional rotation matrix features
#include "G4tgbRotationMatrix.hh"
//Visualization
#include "G4Colour.hh"
#include "G4VisAttributes.hh"


class DetectorConstruction : public G4VUserDetectorConstruction
{
public:
  //! Constructor
  DetectorConstruction();
  //! Destructor
  ~DetectorConstruction();
public:
  //! Construct geometry of the setup
  G4VPhysicalVolume* Construct();
public:
  //Sensitive detectors
  //  G4SDManager* sensitive_detector_manager;
  //AbsorbersSensitive* absorbers_sensitive;
  //Overloaded methods to create solids by passing parameters and returning the pointers to the solids.
  G4Tubs* CreateSolid(G4String,G4double,G4double,G4double,G4double,G4double);
  G4Cons* CreateSolid(G4String,G4double,G4double,G4double,G4double,G4double,G4double,G4double);
  G4TwistedTrap* CreateSolid(G4String,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double);
  G4Trap* CreateSolid(G4String,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4int); 

  //Overloaded methods to create logical volumes by passing parameters and returning the pointers to the logical volumes.
  G4LogicalVolume* CreateLogicalVolume(G4Tubs*,G4Material*,G4String);
  G4LogicalVolume* CreateLogicalVolume(G4Cons*,G4Material*,G4String);
  G4LogicalVolume* CreateLogicalVolume(G4TwistedTrap*,G4Material*,G4String);
  G4LogicalVolume* CreateLogicalVolume(G4Trap*,G4Material*,G4String);

  //Methods to create physical volumes by passing parameters and returning the pointers to the physical volumes.
  G4VPhysicalVolume* CreatePhysicalVolume(G4RotationMatrix*,G4ThreeVector,G4LogicalVolume*,G4String,G4LogicalVolume*);
  G4VPhysicalVolume* CreatePhysicalVolumeDivision(G4String,G4LogicalVolume*,G4String,EAxis,G4int);
  //  G4int CheckAxes(const CLHEP::Hep3Vector&,const CLHEP::Hep3Vector&,const CLHEP::Hep3Vector&);
  G4int CheckAxes(G4RotationMatrix*);
  G4int StandardizeMatrix(G4RotationMatrix*);
  G4int StandardizeMatrix(G4RotationMatrix*,G4String);
  G4int CheckAxes2(G4RotationMatrix*);
  G4bool IsReflection(const G4Scale3D&) const;
  //Logging method
  void WriteToLog(G4String,G4int);
  void PrintTransformMatrices(G4ThreeVector,G4RotationMatrix);
  void PrintTransformMatrices(G4ThreeVector,const G4RotationMatrix*);
  void PrintTransformMatrices(G4Transform3D);
  //Specific methods to create particular components. This follows the basis of the MORTRAN code with a cascade of functions calls to create the solids.
  void CreateMother();
  void CreateEMEndcap(G4String);
  void CreateEMECEndcap(G4String);
  void CreateSupportingDisc(G4String);
  void CreateExternalSupportingDisc(G4String);
  void CreateMediumSupportingDisc(G4String);
  void CreateInternalSupportingDisc(G4String);
  void CreateWheels(G4String);
  void CreateWheelCrack(G4String);
  void CreateAzimuthalSector(G4String,G4int);
  void CreateZDivision(G4String,G4Material*,G4int,G4double,G4RotationMatrix*,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double);
  void CreateAbsorberFold(G4Cons*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4Material*,G4LogicalVolume*);
  void CreateIronFold(G4Cons*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*,G4int);
  void CreateGlueFold(G4Cons*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*,G4int);
  void CreateKaptonFold(G4Cons*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*);
  void CreateCopperFold(G4Cons*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*);
  void CreateNonsensitiveKapton(G4Trap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*);
  void CreateNonsensitiveCopper(G4Trap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*);
  void CreateNonsensitiveAbsorberWithoutLead(G4Trap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*,G4int);
  void CreateNonsensitiveGlue(G4Trap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*,G4int);
  void CreateAbsorberFlatPart(G4TwistedTrap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*,G4int);
  void CreateGlueLayer(G4TwistedTrap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*,G4double,G4double,G4int);
  void CreateLeadLayer(G4TwistedTrap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*,G4double,G4double,G4int);
  void CreateKaptonPlane(G4TwistedTrap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*);
  void CreateCopperElectrodeInKapton(G4TwistedTrap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*,G4double,G4double,G4double,G4double);
  void CreateAbsorberBeginning(G4Trap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*);
  void CreateGlueBeginning(G4Trap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*);
  void CreateKaptonBeginning(G4Trap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*);
  void CreateCopperBeginning(G4Trap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*);
  void CreateAbsorberFlatPart2(G4Trap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*);
  void CreateGlueLayer2(G4Trap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*,G4double,G4double,G4double,G4double,G4double,G4double);
  void CreateIronLayer2(G4Trap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*,G4double,G4double,G4double,G4double,G4double,G4double);
  void CreateIronLayer(G4Trap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double);
  void CreateGlueLayer3(G4Trap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double,G4double);
  void CreateIronTriangle(G4Trap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*,G4bool,G4bool);
  void CreateGlueTriangle(G4TwistedTrap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*,G4double,G4double,G4double,G4double,G4double);
  void CreateLeadTriangle(G4TwistedTrap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*,G4double,G4double,G4double,G4double,G4double);
  void CreateKaptonAnotherPart(G4Trap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*);
  void CreateKaptonTriangle(G4Trap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*,G4bool,G4bool);
  void CreateCopperTriangle(G4Trap*&,G4LogicalVolume*&,G4ThreeVector,G4RotationMatrix*,G4LogicalVolume*,G4double,G4double,G4double,G4double,G4double);
  void CreateReflectedVolumes();

  //write out a summary of the volumes created.
  void DumpSolidAndVolumeDetails();

  //Useful methods for returning logical and physical volumes based on their names
  G4LogicalVolume* GetLogicalVolume(G4String);
  G4VPhysicalVolume* GetPhysicalVolume(G4String);
  //Method to return the name of a logical volume given its pointer.
  G4String GetLogicalVolumeName(G4LogicalVolume*);
  G4double CleanUp(G4double);

  //Vectors to keep track of the solids created.
  std::vector<double> rotation_values;
  std::vector<G4Cons*> elements_cons;  
  std::vector<G4Tubs*> elements_tubs;  
  std::vector<G4TwistedTrap*> elements_twistedtraps;  
  std::vector<G4Trap*> elements_traps;  
  std::vector<G4LogicalVolume*> elements_logical;  
  std::vector<G4VPhysicalVolume*> elements_physical;  
  std::vector<G4ThreeVector> elements_translation;
  std::vector<G4RotationMatrix*> elements_rotation;
  std::vector<G4tgbRotationMatrix*> elements_tgbrotation;
  std::vector<G4String> elements_solid_names;
  std::vector<G4String> elements_logical_names;
  std::vector<G4String> elements_physical_names;
  std::vector<G4int> reflection_volumes;
  std::vector<G4LogicalVolume*> reflection_volumes_parents_logical;

  //global defining where to write the output to, useful for debugging.
  G4int where_to_write;
  G4int whichflatpart;
  //Some globals used in the code
  G4String current_logical;
  G4double convert_radians_degrees;
  G4double convert_degrees_radians;
  G4int element_counter; //Global counter for the shape elements in the absorber.

  //Materials
  G4Material* Air;
  G4Material* FibreGlass;
  G4Material* Al;
  G4Material* lAr;
  G4Material* Fe;
  G4Material* Pb;
  G4Material* Cu;
  G4Material* Kapton;
  //Also needed to define FibreGlass manually.
  G4Material* SiO2;
  G4Material* CaO;
  G4Material* Al2O3;
  G4Material* MgO;
  G4Material* Na2O;
  G4Material* SO3;

  //Global parameters used to define the solid shapes
  G4double granularity_packet_eta; //Eta granularity packet
  G4double granularity_packet_phi; //minimum phi granulatiry (why *3?)
  G4double calorimeter_internal_radius; //Internal radius of the calorimeter
  G4double calorimeter_external_radius; //External radius of the calorimeter
  G4double emec_z_origin; //EMEC z origin
  G4double calorimeter_length; //Length of calorimeter
  G4double lar_nominal_origin; //Nominal origin of lAr, used to calculate the folds
  G4double wheel_reference_point; //New position of wheel reference point in Atlas system
  G4double cell_focalpoint_distance; //Distance of cell focal point from the wheel reference point.
  G4double charge_collection_onoff; //Switch for charge collection: 0=off, 1=on THIS COULD BE A BOOL
  G4double massless_gap_onoff; //Switch for massless gap: 0=off, 1=on THIS COULD BE A BOOL
  G4double absorber_startingpart_length; //The length of the starting part of the absorber
  G4double supportingbar_thickness; //The thickness of the supporting bar
  G4double supportingbar_groove_depth; //The depth of the groove in the supporting bar
  G4double supportingbar_container_zsize; //The z size of the cylinder containg THE SUPPORTING BAR?
  G4double lar_totalthickness; //The total thickness of the lar part.
  G4double active_maximumradius; //The maximum radius of the active part.
  G4double precision_solving_nonlinear; //The presicion for solving the nonlinear EQUATION?
  G4double absorber_innercurvature_radius; //The inner curvature radius of the absorber
  G4double kapton_curvature_radius; //Curvature radius of the kapton
  G4double kapton_thickness; //Kapton thickness
  G4double copper_thickness; //Copper thickness
  G4double glue_thickness; //Glue thickness
  G4double stainlesssteel_thickness; //Stainless steel thickness
  G4double wheel_radiallayers_number; //Number of radial layers in each wheel
  G4double wheel_betweenwheels_semigap; //Semi gap between wheels
  G4double supportingdisc_thickness; //Thickness of disc supporting zig-zag structure;
  G4double externalsupportingdisc_thickness; //Thickness of external disc supporting zig-zag structure;
  G4double mediumsupportingdisc_thickness; //Thickness of medium disc supporting zig-zag structure;
  G4double internalsupportingdisc_thickness; //Thickness of internal disc supporting zig-zag structure;
  G4double uniformregions_number; //Number of uniform regions
  G4double granulatiry_regions_eta[11]; //Eta granulatiry in regions
  G4double granularity_regions_phi[11]; //Phi granularity in regions.
  G4double zones_differentstripwidth_eta[7]; //Eta zones with different strip widths
  G4double hit_maximumenergy; //Maximum energy of a hit
  G4double digit_maximumenergy; //Maximum energy of a digit
  G4double signalnormalzation_gapsize; //Average gap size for the signal normalization
  G4double barrel_endcap_gap; //Opening of the gap between the barral and endcap
  G4double inner_wheel_absorbers_number; //Number of absorbers in inner wheel
  G4double inner_wheel_absorbers_numwaves; //Number of waves on the absorber
  G4double inner_wheel_eta_int; //Maximum eta of the inner wheel
  G4double inner_wheel_eta_ext; //Minimum eta of the inner wheel
  G4double inner_wheel_firstsampling_numwaves; //Number of waves in the first sampling
  G4int inner_wheel_beta; //Beta parameter of the inner wheel
  G4double inner_wheel_alpha_inner; //Inner opening angle
  G4double inner_wheel_alpha_outer; //Outer opening angle
  G4double inner_wheel_lead_thickness; //Thickness of the lead layer.
  G4double outer_wheel_absorbers_number; //Number of absorbers in outer wheel
  G4double outer_wheel_absorbers_numwaves; //Number of waves on the absorber
  G4double outer_wheel_eta_int; //Maximum eta of the outer wheel
  G4double outer_wheel_eta_ext; //Minimum eta of the outer wheel
  G4double outer_wheel_firstsampling_numwaves; //Number of waves in the first sampling
  G4int outer_wheel_beta; //Beta parameter of the outer wheel
  G4double outer_wheel_alpha_inner; //Inner opening angle
  G4double outer_wheel_alpha_outer; //Outer opening angle
  G4double outer_wheel_lead_thickness; //Thickness of the lead layer.
  G4double wheel_parameters_absorbers_number[2]; //Number of absorbers in wheels
  G4double wheel_parameters_absorbers_numwaves[2]; //Number of waves on the absorbers
  G4double wheel_parameters_eta_int[2]; //Maximum eta of the wheels
  G4double wheel_parameters_eta_ext[2]; //Minimum eta of the wheels
  G4double wheel_parameters_firstsampling_numwaves[2]; //Number of waves in the first sampling
  G4int wheel_parameters_beta[2]; //Beta parameter of the wheels
  G4double wheel_parameters_alpha_inner[2]; //Inner opening angle
  G4double wheel_parameters_alpha_outer[2]; //Outer opening angle
  G4double wheel_parameters_lead_thickness[2]; //Thickness of the lead layer.
  G4double wheel_parameters_theta_int[2]; //Theta corresponding to maximum eta of the wheels.
  G4double wheel_parameters_theta_ext[2]; //Theta corresponding to minimum eta of the wheels.
  ////////////////////////////Temp - To be removed once development is complete
  G4double wheel_parameters_absorbers_number_temp[2]; //Number of absorbers in wheels_temp (This has been included for development only, it allows the calculations to be done based on the complete solid, but the visualization to be done on a subsection of the solid).
  ////////////////////////////temp - To be removed once development is complete
  G4double samplingfraction_fit_innerwheel[4]; //p3 fit for sampfrac/gap^1.3 vs eta for inner wheel.
  G4double samplingfraction_fit_outerwheel[4]; //p3 fit for sampfrac/gap^1.3 vs eta for outer wheel.
  G4double scale_correcttion; //Ratio calculation/simulation
  G4double padcounting_eta_extimum; //Low eta for pad counting
  G4double padcounting_width;
  G4double separation_inner_wheel_compartment[7]; //Inner wheel compartment separation
  G4double separation_z12[44];
  G4double separation_z23[22]; //z separation 23 at eta=1.525,deeta=0.050
  G4double z2;
  G4double scrb1; //I don't know what this does yet!
  G4double stac;
  G4double sroue1;
  G4double sroue;
  G4double wheel_internal_radius_1; //These are temporary variables used to set the solid parameter values.
  G4double wheel_internal_radius_2; //These are temporary variables used to set the solid parameter values.
  G4double wheel_external_radius_1; //These are temporary variables used to set the solid parameter values.
  G4double wheel_external_radius_2; //These are temporary variables used to set the solid parameter values.

  //Additional interim parameters
  G4double ifwd;
  G4double ifold;
  G4double zr1;
  G4double zr2;
  G4double zr1p;
  G4double zr2m;
  G4double zr;
  G4double ifront;
  G4double kr;
  G4double kc;
  G4double zcr1;
  G4double zcr2;
  G4double zcr;
  G4double drrmax;
  G4double drrmin;
  G4double iroue;
  G4double zcrk;
  G4double rrint1;
  G4double rrint1p;
  G4double rrint2;
  G4double rrint2m;
  G4double rrext1;
  G4double rrext1p;
  G4double rrext2;
  G4double rrext2m;
  G4double rti1;
  G4double rti2;
  G4double rte1;
  G4double rte2;
  G4double rrmax;
  G4double rrmin;
  G4double rrmax1;
  G4double rrmin1;
  G4double iwh;
  G4double rcrb;
  G4double zpk;
  G4double wheel_parameters_internal_radius[2];
  G4double wheel_parameters_external_radius[2];
  G4double wheel_parameters_d;
  G4double wheel_parameters_rho[2];
  G4double wheel_parameters_eabs[2];
  G4double wheel_parameters_alpha_inner_radians[2];
  G4double wheel_parameters_alpha_outer_radians[2];
  G4double wheel_parameters_eopt[2];
  G4double wheel_parameters_ropt[2];
  G4double rext;
  G4double wheel_parameters_zlopt[2];
  G4double stepr;
  G4double r;
  G4double rr[9];
  G4double e;
  G4double zigl;
  G4double wheel_parameters_salf2n;
  G4double alf2;
  G4double calf2;
  G4double alf;
  G4double talf2;
  G4double alfdeg;
  G4double epb;
  G4double alphar[9];
  G4double eabsr[9];
  G4double salf2;
  G4double radi;
  G4double rado;
  G4double alfidg;
  G4double alfodg;
  G4double tcki;
  G4double tcko;
  G4double alfo;
  G4double alfi;
  G4double tckix;
  G4double tckox;
  G4double zig;
  G4double ciltet;
  G4double cilox;
  G4double cilix;
  G4double cilx;
  G4double alfclo;
  G4double piby2;
  G4double alfcli;
  G4double alfcil;
  G4double cilalf;
  G4double clcphi;
  G4double rcil;
  G4double ciloxk;
  G4double cilixk;
  G4double cilxk;
  G4double ciltek;
  G4double thfc;
  //Parameters of a cylindrical layer
  G4double cylindrical_layer_parameters_radil[16]; //inner radius of the layer
  G4double cylindrical_layer_parameters_radol[16]; //outer radius of the layer
  G4double cylindrical_layer_parameters_alfil[16]; //opening angle of the inner radius
  G4double cylindrical_layer_parameters_alfol[16]; //opening angle of the outer radius
  G4double cylindrical_layer_parameters_tckil[16]; //absorber thickness at inner radius
  G4double cylindrical_layer_parameters_tckol[16]; //absorber thickness at outer radius
  G4double cylindrical_layer_parameters_ciltetl[16]; //inclination of a cone axis
  G4double cylindrical_layer_parameters_alfcill[16]; //angle of a cone sector
  G4double cylindrical_layer_parameters_xcncl[16]; //cone center position
  G4double cylindrical_layer_parameters_ciltekl[16]; //inclination of a cone axis of kapton
  G4double  cylindrical_layer_parameters_xcnckl[16]; //cone center position for kapton

  G4double xcnc;
  G4double xcnck;
  G4double drdi;
  G4double drdo;
  G4double alf1;
  G4double talf1;
  G4double esalf1;
  G4double esalf2;
  G4double talfi;
  G4double talfo;
  G4double esalfi;
  G4double esalfo;
  G4double cilxl;
  G4double phigc;
  G4double phigck;
  G4double dphi;
  G4double cdphi;
  G4double sdphi;
  G4double rho;
  G4double xtrfwi;
  G4double xtrfwo;
  G4double ytrfwi;
  G4double ytrfwo;
  G4double xtrfwd;
  G4double ytrfwd;
  G4double dxyfwd;
  G4double thetfw;
  G4double phifwd;
  G4double hfwdi;
  G4double hfwdo;
  G4double rt1;
  G4double rt2;
  G4double phiio;
  G4double sgn;
  G4double thetx;
  G4double phx;
  G4double phy;
  G4double rto1;
  G4double rto2;
  G4double tck;
  G4double aa;
  G4double ht;
  G4double sgnz;
  G4double thety;
  G4double dxiro;
  G4double dyiro;
  G4double driro;
  G4double dxglu;
  G4double dyglu;
  G4double drglu;
  //End set the parameters -------------------------------------------------------
private:
  //! define needed materials
  void DefineMaterials();
  //! initialize geometry parameters
  void ComputeParameters();

};

#endif
